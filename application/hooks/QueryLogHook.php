<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class QueryLogHook {

    function log_queries() {   
        $CI =& get_instance();
        $times = $CI->db->query_times;
        $dbs    = array();
        $output = NULL;    
        $queries = $CI->db->queries;

        if (count($queries) == 0)
        {
            $output .= "no queries\n";
        }
        else
        {
            $this->CI =& get_instance() ;
            $userkey = $this->CI->session->userdata("user_key");
            if(!isset($userkey)){
                $userkey = '';
            }
           foreach ($queries as $key => $query) {
                $output .= $query . "\n";
                $type = explode(' ', $query);
                
                if (isset($type[0])) {
                    $data['type'] = $type[0];
                    if (($type[0] == 'SELECT' || $type[0] == 'INSERT') && isset($type[2])) {
                        $data['table'] = $this->tablename($type[2]);
//                        var_dump($type[2]);
                    } else
                    if ($type[0] == 'UPDATE' && isset($type[1])) {
                        $data['table'] = $this->tablename($type[1]);
                    } else {
                        $data['table'] = "----------";
                    }
                } else {
                    $data['type'] = "----------";
                }

                $data['controller_key'] = $userkey;
                $data['query'] = $query;
                $data['created_at'] = date('Y-m-d H:i:s');
                $CI->db->insert('logs', $data);
            }
            $took = round(doubleval($times[$key]), 3);
            $output .= "===[took:{$took}]\n\n";
        }

        $CI->load->helper('file');  
        if ( ! write_file(APPPATH  . "/logs/queries.log.txt", $output, 'a+'))
        {
             log_message('debug','Unable to write query the file');
        }  
    }
    
    public function tablename($name){
//        echo $name;
        
        if (strpos($name, 'available_targets') !== FALSE)
            return 'available_targets';
        if (strpos($name, 'target') !== FALSE)
            return 'target';
        if (strpos($name, 'target_locatin') !== FALSE)
            return 'target_locatin';
        if (strpos($name, 'visualbits') !== FALSE)
            return 'visualbits';
        if (strpos($name, 'ism_trackdata') !== FALSE)
            return 'ism_trackdata';
        if (strpos($name, 'is_present_monitering') !== FALSE)
            return 'is_present_monitering';
        if (strpos($name, 'is_controller') !== FALSE)
            return 'is_controller';
        if (strpos($name, 'controller_login') !== FALSE)
            return 'controller_login';
        if (strpos($name, 'email_templates') !== FALSE)
            return 'email_templates';
        if (strpos($name, 'parent_childs') !== FALSE)
            return 'parent_childs';
        if (strpos($name, 'ism_presents') !== FALSE)
            return 'ism_presents';
        else
            return $name;
        
    }

}