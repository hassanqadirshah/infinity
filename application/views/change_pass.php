<?php
include_once 'header.php';
if (isset($msg) && $msg != "") {
    $msgshow = $msg;
}
?>
<div class="container">

    <form method="post" id="user_login" class="form-signin" action="<?php echo site_url('infinity/change_pass'); ?>" onsubmit="return cpasscheck()" >
        <h2 class="form-signin-heading text-center"><?php echo $msgshow; ?></h2>
        <input type="password" class="form-control" placeholder="Old Password" name="opassword" id="opassword" value="" required="">
        <input type="password" class="form-control" placeholder="New Password" name="npassword" id="npassword" value="" required="">
        <input type="password" class="form-control" placeholder="Confirm Password" name="cpassword" id="cpassword" value="" required="">
        <input type="hidden" name="userid" id="userid" value="<?php echo $userid; ?>" >
        <button class="btn btn-lg btn-primary btn-block" type="submit">Change It!</button>
    </form>

</div>
<script>
    function cpasscheck() {
        $('#npassword').val();
        console.log($('#npassword').val());
        console.log($('#cpassword').val());
        if ($('#npassword').val() == $('#cpassword').val())
        {
            return true;
        }
        else {
            alert("NEW PASSWORD AND CONFIRM PASSWORD NOT MATCH !!");
            $('#npassword').val('');
            $('#cpassword').val('');
            $('#npassword').focus();
            return false;
        }
    }
</script>



<?php
include_once 'footer.php';
?>