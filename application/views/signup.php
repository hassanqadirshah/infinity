<!DOCTYPE html>
<html>
    <head>
        <title>INFINITY DEVELOPERS</title>
        <link href="<?php echo site_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo site_url('assets/css/style.css'); ?>" rel="stylesheet">
        <link href="<?php echo site_url('assets/css/signup.css'); ?>" rel="stylesheet">
        <script src="<?php echo site_url('assets/js/minijs.js'); ?>"></script>
        <script src="<?php echo site_url('assets/js/bootstrap.min.js'); ?>"></script>
        <!--Validate Form -->
        <script src="<?php //echo site_url('assets/js/validation/lib.js');?>"></script>
        <script src="<?php //echo site_url('assets/js/validation/validation.js');?>"></script>
    </head>
    <style>
        .tandc{
            padding-top: 20px;
        }
        .tandc input{
            float: left;
        }
        .tandc a{
            float: left;
            width: 83%;
            font-size: 16px;
            MARGIN-LEFT: 5PX;
            COLOR: #FFF !IMPORTANT;
            TEXT-DECORATION: underline;
        }
        .tandc a:hover{
            color: #000 !important;
        }
    </style>
    <script>
$.validator.setDefaults({
	submitHandler: function() { alert("submitted!"); }
});

$().ready(function() {
	// validate the comment form when it is submitted
	$("#commentForm").validate();

	// validate signup form on keyup and submit
	$("#signupForm").validate({
		rules: {
			name: "required",
			fname: "required",
			name: {
				required: true,
				minlength: 2
			},
			fname: {
				required: true,
				minlength: 5
			},
			cpassword: {
				required: true,
				minlength: 5,
				equalTo: "#password"
			},
			email: {
				required: true,
				email: true
			},
			topic: {
				required: "#newsletter:checked",
				minlength: 2
			},
			agree: "required"
		},
		messages: {
			firstname: "Please enter your firstname",
			lastname: "Please enter your lastname",
			name: {
				required: "Please enter a username",
				minlength: "Your username must consist of at least 2 characters"
			},
			password: {
				required: "Please provide a password",
				minlength: "Your password must be at least 5 characters long"
			},
			cpassword: {
				required: "Please provide a password",
				minlength: "Your password must be at least 5 characters long",
				equalTo: "Please enter the same password as above"
			},
			email: "Please enter a valid email address",
			agree: "Please accept our policy"
		}
	});

	// propose username by combining first- and lastname
	$("#name").focus(function() {
		var firstname = $("#name").val();
		if(firstname && !this.value) {
			this.value = firstname + ".";
		}
	});

	//code to hide topic selection, disable for demo
	var newsletter = $("#newsletter");
	// newsletter topics are optional, hide at first
	var inital = newsletter.is(":checked");
	var topics = $("#newsletter_topics")[inital ? "removeClass" : "addClass"]("gray");
	var topicInputs = topics.find("input").attr("disabled", !inital);
	// show when newsletter is checked
	newsletter.click(function() {
		topics[this.checked ? "removeClass" : "addClass"]("gray");
		topicInputs.attr("disabled", !this.checked);
	});
});
</script>

<style type="text/css">
/*#commentForm { width: 500px; }
#commentForm label { width: 250px; }
#commentForm label.error, #commentForm input.submit { margin-left: 253px; }
#signupForm { width: 670px; }
#signupForm label.error {
	margin-left: 10px;
	width: auto;
	display: inline;
}
#newsletter_topics label.error {
	display: none;
	margin-left: 103px;
}*/
</style>
    <body>
        <div class="chapie"></div>
        <div class="main">
            <div class="heading" style="margin-bottom: 20px; color: red;"><?php
            if(isset($msg))echo $msg;
            
            ?></div>
            <div class="main-box shadow ">
                <!--<div class="heading">Admin Login</div>-->
                <div class="logo">
                    <p class="cp">
                        <!--<img src="<?php // echo site_url('assets/images/logo.png'); ?>"/>-->
                        <a href="<?php echo base_url();?>"><img src="<?php echo site_url('assets/images/logo2.png'); ?>"/></a>
                    </p>
                </div>
                <h2 class="form-signin-heading text-center">Sign up</h2>

                <form method="post" id="signupForm" enctype="multipart/form-data" class="transfer_con" action="<?php echo site_url('infinity/signup'); ?>" >

                    <ul>
                        <li><label>Name</label> <input type="text" name="name" id="name" class="form-control" value=""></li>
                        <li><label>Father / Husband Name</label> <input type="text" name="fname" class="form-control" id="fname"  value=""></li>
                        <li><label>Email Address</label> <input type="text" name="email" class="form-control" id="email"  value=""></li>
                        <li><label>CNIC#</label> <input type="text" name="nic" class="form-control" id="nic" value=""></li>
                        <li><label>Mobile Number</label> <input type="text" id="mno" name="mno" onkeypress="validate(event)" class="form-control" value=""></li>
                        <li><label>Gender</label> <select id="gender" class="form-control" name="gender"><option value="Male">Male</option><option value="Female">Female</option></select></li>
                        <li><label>Password</label> <input type="password" id="password" name="password" class="form-control" value=""></li>
                        <li><label>Confirm Password</label> <input type="password" id="cpassword" name="cpassword" class="form-control" value=""></li>
                        <li class='tandc'><input type="checkbox" class="tandcinp" id='tandcinp' name="tandcinp"><a href="<?php echo site_url('infinity/termsandconditions'); ?>">Accept Terms And Condition's</a></li>
                        <li><label>Upload Picture</label><input type="file" class="filetype"  name="user_pic"></li>                      
                        <li style='width: 95%;'><input type="submit" id="signup" class="btn btn-primary signupbtn" value="Sign Up Me !" onclick="return formvalidation();" ></li>
                        <li style='width: 95%;'><a href="<?php echo base_url();?>" style="color:maroon;font-weight: bold;"><span class="glyphicon glyphicon-arrow-left"></span> go back...</a></li>
                    </ul>
                    
                </form>
        <!--<div class="logo"><p class="cp"><img src="<?php //echo site_url('assets/images/logo.png');       ?>"/></p></div>-->
            </div>
            <script>

                            function formvalidation() {

                                var name = $('#name').val();
                                var fname = $('#fname').val();
                                var email = $('#email').val();
                                var nic = $('#nic').val();
                                var mno = $('#mno').val();
                                var pass = $('#password').val();
                                var cpass = $('#cpassword').val();
                                if (name != '' && fname != '' && email != '' && nic != '' && mno != '' && pass != '' && cpass != '') {
                                    if (pass != cpass) {
                                        alert("Password And Confirm Password Not Match !");
                                        $('#password').focus();
                                        return false;
                                    } else if (!validateEmail(email)) {
                                        alert("Enter Proper Email Id");
                                        $('#email').focus();
                                        return false;
                                    } else if (nic.length < 13 || nic.length > 13) {
                                        alert("Invalid NIC Number ");
                                        $('#nic').focus();
                                        return false;
                                    } else if (mno.length < 11 || mno.length > 11) {
                                        alert("Enter Mobile Number Like '03001234567' ");
                                        $('#mno').focus();
                                        return false;
                                    }else if($('#tandcinp').prop('checked') == false){
                                        alert("Please Accept Terms and Conditions");
                                        $('#tandcinp').focus();
                                        return false;
                                    } else {
                                        var status = confirm('Are You Sure All Fields You Fill Are Correct ?');
                                        if (status == true)
                                            $('.chapie').show();
                                        
//                                        $('.chapie').show();
                                        return status;
                                    }
                                } else {
                                    alert("Complete All fields First !");
                                    return false;
                                }
                            }
                            function validateEmail(email) {
                                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                                return re.test(email);
                            }

                            $(".chapie").click(function() {
                                //alert("Handler for .click() called.");
//                                location.reload();
                                window.location.href='login';
                            });

                            function validate(evt) {
                                var theEvent = evt || window.evt;
                                var charCode = (evt.which) ? evt.which : event.keyCode
//            console.log(charCode);
                                if ((charCode != 1 || charCode != 3) && charCode > 31 && (charCode < 48 || charCode > 57)) {
                                    theEvent.returnValue = false;
                                    if (theEvent.preventDefault)
                                        theEvent.preventDefault();
                                }
                                return true;
                            }
                            
                            
                            
                             $("#nic").keydown(function (e) {
                                
                             // Allow: backspace, delete, tab, escape, enter and .
                                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                                // Allow: Ctrl+A
                                (e.keyCode == 65 && e.ctrlKey === true) || 
                                // Allow: home, end, left, right
                                (e.keyCode >= 35 && e.keyCode <= 39)) {
                                // let it happen, don't do anything
                                return;
                                }
                            // Ensure that it is a number and stop the keypress
                            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                                e.preventDefault();
                            }
                            
                        });
                            
                            
            </script>
        </div>
    </body>
</html>
