<?php
include_once 'header.php';
//echo "<pre>";print_r($result);echo "</pre>";
?>

<div class="highlight myhighlight" onload="initialize()" sty>
    <style>
        .margin5{
            margin: 15px;
        }
    </style>
    <div class="col-md-12">
        <?php
        if (count($result) == 0) {
            echo "No Image Found";
        } else {
            ?>

            <?php
//            echo "<pre>";print_r($result);
            for ($m = 0; $m < count($result); $m++) {
                ?>
                <div class="col-md-2 margin5" id="<?php echo "box" . $m; ?>">
                    <?php
                    if ($result[$m]['recoverreqest'] == 1) {
                        ?>
                        <img class="picmarked" id="<?php echo "p" . $m . "t" ?>"  src="<?php echo site_url('assets/images/checked.png'); ?>">

                        <?php
                    } else {
                        ?>
                        <img class="markedpic" id="<?php echo "p" . $m . "t" ?>" onclick="unchecked(<?php echo $m; ?>);" src="<?php echo site_url('assets/images/checked2.png'); ?>">

                        <?php
                    }
                    
                    $src = '';
                    if($result[$m]['type'] == 0){
                        $src = site_url('upload/' . $result[$m]['child_key'] .'/'. $result[$m]['pic_name']);
                    }else if($result[$m]['type'] == 1){
                        $src = site_url('assets/images/movie.png');
                    }else if($result[$m]['type'] == 2){
                        $src = site_url('assets/images/keylog.jpg');
                    }
                    
                    
                    ?>

                    <!--<img class="markedpic" id="<?php //echo "p" . $m . "t" ?>" onclick="unchecked(<?php //echo $m; ?>);" src="<?php //echo site_url('assets/images/clicked2.gif'); ?>">-->
                    <img id="<?php echo $m ?>" onclick="clickedbtn(<?php echo $m; ?>);" src="<?php echo $src; ?>" style='height: 200px;width: 200px;'>
                    <input type="hidden" id="<?php echo "pname" . $m; ?>"  value="<?php echo $result[$m]['pic_name']; ?>" name="picname">
                    <input type="hidden" id="<?php echo "childkey" . $m; ?>"  value="<?php echo $result[$m]['child_key']; ?>" name="childkey">
                    <input type="hidden" id="<?php echo "picid" . $m; ?>"  value="<?php echo $result[$m]['id']; ?>" name="childkey">
                    <?php if ($result[$m]['recoverreqest'] == 0) { ?>
                    <span style="float: left;text-align: center;width: 170px;"> 
                        <input type="checkbox" class="mycheckbox" id="<?php echo "p" . $m ?>" name="v2">Recover This
                    </span>
                    <?php
                    }
                    ?>
                </div>
                <?php
            }
            ?>
            </div>
            <form method="post" name="recover" id ="recoverform" onsubmit="return validatealldata();" action="<?php echo site_url('infinity/recoverimagesrequest'); ?>">
                <div class="col-md-6 col-md-offset-3" style="margin-bottom: 20px;">
                    <button class="btn btn-lg btn-primary btn-block mybtn" type="submit">Request Admin To Recover Selected Images</button>
                </div>
            </form> 
            <?php
        }
        ?>
    
</div>

<script>
    var glob = parseInt(<?php echo count($result); ?>);
    function clickedbtn(getid) {
        $('#p' + getid + "t").show();
        $('#p' + getid).prop('checked', true);
    }
    function unchecked(getid) {
        $('#p' + getid + "t").hide();
        $('#p' + getid).prop('checked', false);
    }

    $(".mycheckbox").change(function() {
        var gotid = $(this).attr('id');
        if (this.checked) {

            $('#' + gotid + "t").show();
        } else {
            $('#' + gotid + "t").hide();
        }
    });

    function validatealldata() {
//        console.log(glob);
        var arrlenth = 0;
        for (var k = 0; k < glob; k++) {
            if ($('#p' + k).prop('checked') == true) {
                var row = '<input type="hidden" name="' + "request" + arrlenth + "picid" + '" value="' + $('#picid' + k).val() + '"/> <input type="hidden" name="' + "request" + arrlenth + "childid" + '" value="' + $('#childkey' + k).val() + '"/> <input type="hidden" name="' + "request" + arrlenth + "picname" + '" value="' + $('#pname' + k).val() + '"/>';
//                console.log(row);
                $('#recoverform').append(row);
                arrlenth++;
            }
        }
        if (arrlenth > 0) {
            return true;
        } else {
            return false;
        }
    }


</script>

<?php
include_once 'footer.php';
?>