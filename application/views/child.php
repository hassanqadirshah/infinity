<?php
include_once 'header.php';
//echo "<pre>";print_r($result);echo "</pre>";

$locationfind = getlastlocation($result[0]['parent_key'], $result[0]['child_key']);
//
//echo "<pre>";print_r($locationfind);echo "</pre>";
//
$mycount = count($locationfind);
//if ($mycount > 0) {
//    $location = $locationfind[$mycount - 1];
////    echo "<pre>";print_r($location);echo "</pre>";
//} else {
//    return 0;
//}
////        echo "<pre>";print_r($location);echo "</pre>";
?>

<script>
    var glob = <?php echo $mycount; ?>
</script>

<style type="text/css">
    div#map {
        position: relative;
    }
    div#crosshair {
        position: absolute;
        top: 192px;
        height: 19px;
        width: 19px;
        left: 50%;
        margin-left: -8px;
        display: block;
        background: url(crosshair.gif);
        background-position: center center;
        background-repeat: no-repeat;
    }
    .no{
        display: none;
    }
    .myhighlight{
        margin-bottom: 30px;
        float: left;
    }
    .mybtn{
        float: left;
        margin-left: 30px;
    }
    .table{
        margin-left: 0;
    }
</style>
<div class="highlight myhighlight" onload="initialize()">
    <div class="col-md-12" style="margin-bottom: 60px;">
        <div class="col-md-1"></div>
        <div class="col-md-10" style="height: 30px;">
            <h4 class="text-center">BASIC INFO OF <?php echo $result[0]['child_key'] . " ( " . $result[0]['user_name'] . " )"; ?> </h4>
        </div>
        <div class="col-md-1"></div>
        <div id="map" class="col-md-12 no" style="height: 300px;">
            <input type="hidden" id="currentindex" value="">
            <h2 style="text-align: center;">(His Last Location)</h2>
            <div class="col-md-1" >
                <a href="javascript:;" class="no" id="nextbtn">Next</a>
            </div>
            <div id="map_canvas" class="col-md-10" style="height: 300px;"></div>

            <div id="crosshair"></div>
            <div class="col-md-1">
                <a href="javascript:;" id="prevbtn">Previous</a>
            </div>
        </div>
        <div id="nomap" class="col-md-10 col-md-offset-1 no" style="height: 0px;color: red;">
            <h3 style="text-align: center;padding: 10px 0 0 0;">No Internet Connection Avaliable >_< </h3>
        </div>
    </div>
    <div class="col-md-12">
        <div class="col-md-6">
            <h4 class="text-center">System INFO</h4>
            <table class="table table-striped">
                <tr>
                    <td>Ethernet Mac Address</td>
                    <td><?php echo $result[0]['mac_e']; ?></td>
                </tr>
                <tr>
                    <td>WIFI Mac Address</td>
                    <td><?php echo $result[0]['mac_w']; ?></td>
                </tr>
                <tr>
                    <td>System Name</td>
                    <td><?php echo $result[0]['system_name']; ?></td>
                </tr>
                <tr>
                    <td>User Name</td>
                    <td><?php echo $result[0]['user_name']; ?></td>
                </tr>
                <tr>
                    <td>OS Name</td>
                    <td><?php echo $result[0]['os_name']; ?></td>
                </tr>
                <tr>
                    <td>OS Version</td>
                    <td><?php echo $result[0]['os_version']; ?></td>
                </tr>
                <tr>
                    <td>OS Type</td>
                    <td><?php echo $result[0]['os_type']; ?></td>
                </tr>
                <tr>
                    <td>Physical Memory</td>
                    <td><?php echo $result[0]['physical_memory']; ?></td>
                </tr>
                <tr>
                    <td>Processor Name</td>
                    <td><?php echo $result[0]['processor_name']; ?></td>
                </tr>
                <tr>
                    <td>No Of core's & Thread's</td>
                    <td><?php echo $result[0]['processor_counter']; ?></td>
                </tr>
                <tr>
                    <td>Machine Type</td>
                    <td><?php echo $result[0]['system_type']; ?></td>
                </tr>
                <tr>
                    <td>System Manufacture</td>
                    <td><?php echo $result[0]['system_manufacturer']; ?></td>
                </tr>
                <tr>
                    <td>System Model</td>
                    <td><?php echo $result[0]['system_model']; ?></td>
                </tr>
                <tr>
                    <td>BIOS Version</td>
                    <td><?php echo $result[0]['bios_version']; ?></td>
                </tr>
                <tr>
                    <td>Operating System Installation Date Time</td>
                    <td><?php echo $result[0]['os_install_datetime']; ?></td>
                </tr>
                <tr>
                    <td>Status</td>
                    <td><?php echo $result[0]['is_expired'] == 0 ? "Not Expired" : "Expired"; ?></td>
                </tr>
                <tr>
                    <td>Total Screen Shot Of This Person</td>
                    <td><?php echo gettotalimages($p_key, $result[0]['child_key'], $result[0]['mac_e'], $result[0]['mac_w']); ?></td>
                </tr>
                <tr>
                    <td>System Time Zone</td>
                    <td><?php echo $result[0]['time_zone']; ?></td>
                </tr>
                <tr>
                    <td>Introduced On </td>
                    <td><?php echo $result[0]['datetime']; ?></td>
                </tr>
                
            </table>
            <?php //echo site_url('assets' . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . $result[0]['child_key'] . DIRECTORY_SEPARATOR . 'infinity.zip');?>
            <a class="btn btn-success" href='<?php echo site_url('infinity/re_genrate_child').'/'.$result[0]['child_key']; ?>'>Download Program Again!</a>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <button type="button" id="uninstall" class="btn btn-danger <?php echo $result[0]['status'] == 1 ? 'disabled' : 'active'; ?>"   >Uninstall This User!</button>
        </div>
        <div class="col-md-6">
            <?php
            //$status_old = getoperationstatus($result[0]['child_key']);
//            $parent_type = getcontrollertype($result[0]['parent_key']);
//            $my_presents = getalloperations($result[0]['parent_key'], $result[0]['child_key']);
//            $totalpresents = totalpresents();
//
//            //echo "<pre>";print_r($my_presents);echo "</pre>";
//            //echo "<pre>";print_r($totalpresents);echo "</pre>";
//
//            levelpresents($my_presents, $totalpresents, $result[0]['parent_key'], $result[0]['child_key'], $parent_type);
//            $presents = getalloperations($result[0]['parent_key'], $result[0]['child_key']);
            ?>
            <div class="table-responsive" style="margin-top: 55px;">
                <!--<form method="post" id="update_child" action="<?php //echo site_url('infinity/child');                  ?>" >-->
                
                    
                     <?php
//                            echo "<pre>";print_r($presents);echo "</pre>";
                     
                     $ism = array();
                     $other = array();
                     $bandwidth = array();
                     $browsers = array();
                     $btns = array();
                     $indbtns = array();
                     
                     
                     for ($j = 0; $j < count($presents); $j++) {

                        if ($presents[$j]['category'] == 1 && $presents[$j]['status'] == 1) {
                            array_push($ism, $presents[$j]);
                        } else if ($presents[$j]['category'] == 0 && $presents[$j]['status'] == 1) {
                            array_push($other, $presents[$j]);
                        } else if ($presents[$j]['category'] == 2 && $presents[$j]['status'] == 1) {
                            array_push($bandwidth, $presents[$j]);
                        } else if ($presents[$j]['category'] == 3 && $presents[$j]['status'] == 1) {
                            array_push($browsers, $presents[$j]);
                        } else if ($presents[$j]['category'] == 4 && $presents[$j]['status'] == 1) {
                            array_push($btns, $presents[$j]);
                        }else if ($presents[$j]['category'] == 5 && $presents[$j]['status'] == 1) {
                            array_push($indbtns, $presents[$j]);
                        }
                    }
//                        secho "<pre>";print_r($btns);echo "</pre>";exit;
//                        echo "<pre>";print_r($presents);echo "</pre>";
//                        echo "<pre>";print_r($ismother);echo "</pre>";
//                        echo "<pre>";print_r($bandwidth);echo "</pre>";
//                        echo "<pre>";print_r($browsers);echo "</pre>";
                        $counter = 1;
                        ?>
                    
                    <?php
                    if(isset($btns) && count($btns) > 0 ){
                        ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="tclass" data-toggle="collapse" data-parent="#accordion" href="#collapsefour">
                                    
                                    <span>Quick Operations</span>
                                    <span class="glyphicon glyphicon-collapse-up upbtn" style="float: right;" ></span>
                                    <span class="glyphicon glyphicon-collapse-down downbtn no" style="float: right;" ></span>
                                </a>
                            </h4>
                        </div>
                        <div id="collapsefour" class="panel-collapse collapse">
                                <div class="panel-heading">
                                    <span class="col-md-1"><h4>#</h4></span>
                                    <span class="col-md-5"><h4>Operations</h4></span>
                                    <span class="col-md-6" style="text-align: center"><h4>Action</h4></span>
                                </div>
                                <?php
                                for ($c = 0; $c < count($btns); $c++) {
                                    $present_name = presentname($btns[$c]['present_id']);
                                    $present_detail = presentdetail($btns[$c]['present_id']);
                                    ?>
                                    <div class="panel-body">
                                        <span class="col-md-1"><?php echo $counter; ?></span>
                                        <span class="col-md-4" title="<?php echo $present_detail;?>"><?php echo $present_name; ?></span>
                                        <div class="col-md-7" style="text-align: center">
                                            <p class='col-md-3'></p>
                                            
                                            <?php
                                            
                                            $current = date('Y-m-d H:i:s');
                                            $updated = $btns[$c]['updated_at'];
                                            $interval = 0;
                                            if($current != '0000-00-00 00:00:00'){
                                                $interval = strtotime($current) - strtotime($updated);
                                            }else{
                                                $interval = 301;
                                            }
//                                            var_dump($interval);exit;
//                                            echo gmdate('Y-m-d H:i:s',$interval);
//                                            echo "<pre>";print_r($interval);exit;
                                            
                                            if($interval > 300 || $btns[$c]['present_status'] == 0){
                                            ?>
                                            <p id='container'>
                                                <button type="button" id="<?php echo $btns[$c]['present_id'];?>" data-popover="true" data-html=true  data-toggle="popover" data-placement="right" data-content="<?php echo $present_detail;?>" title="" data-original-title="<?php echo $present_name; ?>"  data-ckey="<?php echo $result[0]['child_key'];?>" class="quickope col-md-8 btn btn-primary <?php echo $result[0]['status'] == 1 ? 'disabled' : 'active'; ?>"   >Hit Me!</button>
                                            </p>
<!--                                            <form method="post" name="upqchild" action="<?php //echo site_url('infinity/updatequickchildope/'.$result[0]['child_key'].'/'.$btns[$c]['present_id']); ?>">
                                                <input type="submit" name="<?php //echo $present_name . '___' . $btns[$c]['present_id']; ?>" value="Hit Me!" class='col-md-8 btn btn-primary'>
                                            </form>-->
                                            <?php
                                                }else{
                                            ?>
                                            <input type="button" name="<?php echo $present_name . '___' . $btns[$c]['present_id']; ?>" value="Wating For Process" class='col-md-8 btn btn-primary disabled'>
                                            <?php
                                                }
                                            ?>
                                            
                                            
                                            
                                        </div>
                                    </div>
                                    <?php $counter++;
                                } ?>
                            </div>
                    </div>
                    <?php }?> 
                
                    <?php
                    if(isset($indbtns) && count($indbtns) > 0 ){
                        ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="tclass" data-toggle="collapse" data-parent="#accordion" href="#collapsesix">
                                    
                                    <span>Quick Feed</span>
                                    <span class="glyphicon glyphicon-collapse-up upbtn" style="float: right;" ></span>
                                    <span class="glyphicon glyphicon-collapse-down downbtn no" style="float: right;" ></span>
                                </a>
                            </h4>
                        </div>
                        <div id="collapsesix" class="panel-collapse collapse">
                                <div class="panel-heading">
                                    <span class="col-md-1"><h4>#</h4></span>
                                    <span class="col-md-5"><h4>Operations</h4></span>
                                    <span class="col-md-6" style="text-align: center"><h4>Action</h4></span>
                                </div>
                                <?php
                                for ($c = 0; $c < count($indbtns); $c++) {
                                    $present_name = presentname($indbtns[$c]['present_id']);
                                    $present_detail = presentdetail($indbtns[$c]['present_id']);
                                    ?>
                                    <div class="panel-body">
                                        <span class="col-md-1"><?php echo $counter; ?></span>
                                        <span class="col-md-4" title="<?php echo $present_detail;?>"><?php echo $present_name; ?></span>
                                        <div class="col-md-7" style="text-align: center">
                                            <p class='col-md-3'></p>
                                            
                                            <?php
                                            
                                            $current = date('Y-m-d H:i:s');
                                            $updated = $indbtns[$c]['updated_at'];
                                            $interval = 0;
                                            if($current != '0000-00-00 00:00:00'){
                                                $interval = strtotime($current) - strtotime($updated);
                                            }else{
                                                $interval = 301;
                                            }
//                                            var_dump($interval);exit;
//                                            echo gmdate('Y-m-d H:i:s',$interval);
//                                            echo "<pre>";print_r($interval);exit;
                                            
                                            if($interval > 300 || $indbtns[$c]['present_status'] == 0){
                                            ?>
                                            <p id='container'>
                                                <button type="button" id="<?php echo $indbtns[$c]['present_id'];?>" data-popover="true" data-html=true  data-toggle="popover" data-placement="right" data-content="<?php echo $present_detail;?>" title="" data-original-title="<?php echo $present_name; ?>"  data-ckey="<?php echo $result[0]['child_key'];?>" class="quickope col-md-8 btn btn-primary <?php echo $result[0]['status'] == 1 ? 'disabled' : 'active'; ?>"   >Hit Me!</button>
                                            </p>
<!--                                            <form method="post" name="upqchild" action="<?php //echo site_url('infinity/updatequickchildope/'.$result[0]['child_key'].'/'.$btns[$c]['present_id']); ?>">
                                                <input type="submit" name="<?php //echo $present_name . '___' . $btns[$c]['present_id']; ?>" value="Hit Me!" class='col-md-8 btn btn-primary'>
                                            </form>-->
                                            <?php
                                                }else{
                                            ?>
                                            <input type="button" name="<?php echo $present_name . '___' . $indbtns[$c]['present_id']; ?>" value="Wating For Process" class='col-md-8 btn btn-primary disabled'>
                                            <?php
                                                }
                                            ?>
                                            
                                            
                                            
                                        </div>
                                    </div>
                                    <?php $counter++;
                                } ?>
                            </div>
                    </div>
                    <?php }?> 
                    
                    <form method="post" name="upchild" action="<?php echo site_url('infinity/updatechildope'); ?>">
                    <?php
                        if(isset($ism ) && count($ism) > 0 ){
                        ?>
                    
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="tclass" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                    <span>ISM Presents</span>
                                    <span class="glyphicon glyphicon-collapse-up upbtn" style="float: right;" ></span>
                                    <span class="glyphicon glyphicon-collapse-down downbtn no" style="float: right;" ></span>
                                </a>
                                <!--<span class="glyphicon glyphicon-collapse-up" ></span>-->
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse">
                            <div class="panel-heading">
                                <span class="col-md-1"><h4>#</h4></span>
                                <span class="col-md-5"><h4>Operations</h4></span>
                                <span class="col-md-6" style="text-align: center"><h4>Status</h4></span>
                            </div>
                            <?php
                            for ($a = 0; $a < count($ism); $a++) {
                                $present_name = presentname($ism[$a]['present_id']);
                                $present_detail = presentdetail($ism[$a]['present_id']);
                                ?>
                           <div class="panel-body">
                                    <span class="col-md-1"><?php echo $counter; ?></span>
                                    <span class="col-md-4" title="<?php echo $present_detail; ?>"><?php echo $present_name; ?></span>
                                    <div class="col-md-7" style="text-align: center">
                                        <span> <input type="radio" data-popover="true" data-html=true  data-toggle="popover" data-placement="right" data-content="<?php echo $present_detail;?>" title="" data-original-title="Start <?php echo $present_name; ?>" name="<?php echo $present_name . '___' . $ism[$a]['present_id']; ?>" value="on" <?php echo $result[0]['status'] == 1 ? 'disabled' : ''; ?> <?php
                                            if ($ism[$a]['present_status'] == 1) {
                                                echo 'checked';
                                            }
                                            ?>>&nbsp;&nbsp;Start &nbsp;
                                        </span>
                                        <span><input type="radio" data-popover="true" data-html=true  data-toggle="popover" data-placement="right" data-content="<?php echo $present_detail;?>" title="" data-original-title="Stop <?php echo $present_name; ?>" name="<?php echo $present_name . '___' . $ism[$a]['present_id']; ?>" value="off" <?php echo $result[0]['status'] == 1 ? 'disabled' : ''; ?> <?php
                                            if ($ism[$a]['present_status'] == 0) {
                                                echo 'checked';
                                            }
                                            ?>>&nbsp;&nbsp;Stop
                                        </span>
                                    </div>
                                </div>
                        <?php $counter++;} ?>
                        </div>
                    </div>
                        <?php }?> 
                    <!--// 2nd band width--> 
                    <?php
                    if(isset($bandwidth) && count($bandwidth) > 0 ){
                        ?>
                    
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="tclass" data-toggle="collapse" data-parent="#accordion" href="#collapsetwo">
                                    <span>BandWidth</span>
                                    <span class="glyphicon glyphicon-collapse-up upbtn" style="float: right;" ></span>
                                    <span class="glyphicon glyphicon-collapse-down downbtn no" style="float: right;" ></span>
                                </a>
                            </h4>
                        </div>
                        <div id="collapsetwo" class="panel-collapse collapse">
                            <div class="panel-heading">
                                <span class="col-md-1"><h4>#</h4></span>
                                <span class="col-md-5"><h4>Operations</h4></span>
                                <span class="col-md-6" style="text-align: center"><h4>Status</h4></span>
                            </div>
                            <?php
                            for ($b = 0; $b < count($bandwidth); $b++) {
                                $present_name = presentname($bandwidth[$b]['present_id']);
                                $present_detail = presentdetail($bandwidth[$b]['present_id']);
                                ?>
                           <div class="panel-body">
                                    <span class="col-md-1"><?php echo $counter; ?></span>
                                    <span class="col-md-4" title="<?php echo $present_detail; ?>"><?php echo $present_name; ?></span>
                                    <div class="col-md-7" style="text-align: center">
                                        <span> <input type="radio" data-popover="true" data-html=true  data-toggle="popover" data-placement="right" data-content="<?php echo $present_detail;?>" title="" data-original-title="Start <?php echo $present_name; ?>"  name="<?php echo $present_name . '___' . $bandwidth[$b]['present_id']; ?>" value="on" <?php echo $result[0]['status'] == 1 ? 'disabled' : ''; ?> <?php
                                                if ($bandwidth[$b]['present_status'] == 1) {
                                                    echo 'checked';
                                                }
                                                ?>>&nbsp;&nbsp;Start &nbsp;&nbsp;
                                        </span>
                                        <span><input type="radio"  data-popover="true" data-html=true  data-toggle="popover" data-placement="right" data-content="<?php echo $present_detail;?>" title="" data-original-title="Stop <?php echo $present_name; ?>"  name="<?php echo $present_name . '___' . $bandwidth[$b]['present_id']; ?>" value="off" <?php echo $result[0]['status'] == 1 ? 'disabled' : ''; ?> <?php
                                                if ($bandwidth[$b]['present_status'] == 0) {
                                                    echo 'checked';
                                                }
                                                ?>>&nbsp;&nbsp;Stop &nbsp;&nbsp;
                                        </span>

                                        <span><input type="radio"  data-popover="true" data-html=true  data-toggle="popover" data-placement="right" data-content="<?php echo $present_detail;?>" title="" data-original-title="<?php echo $present_name; ?>" name="<?php echo $present_name . '___' . $bandwidth[$b]['present_id']; ?>" value="none" <?php echo $result[0]['status'] == 1 ? 'disabled' : ''; ?> <?php
                                            if ($bandwidth[$b]['present_status'] == -1) {
                                                echo 'checked';
                                            }
                                            ?>>&nbsp;&nbsp;Not Monitoring
                                        </span>
                                    </div>
                                </div>
                        <?php $counter++;} ?>
                        </div>
                    </div>
                    <?php }?> 
                    
                    <!--// 3rd  Browsers--> 
                    
                    <?php
                    if(isset($browsers) && count($browsers) > 0 ){
                        ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="tclass" data-toggle="collapse" data-parent="#accordion" href="#collapsethree">
                                    
                                    <span>Browsers</span>
                                    <span class="glyphicon glyphicon-collapse-up upbtn" style="float: right;" ></span>
                                    <span class="glyphicon glyphicon-collapse-down downbtn no" style="float: right;" ></span>
                                </a>
                            </h4>
                        </div>
                        <div id="collapsethree" class="panel-collapse collapse">
                            <div class="panel-heading">
                                <span class="col-md-1"><h4>#</h4></span>
                                <span class="col-md-5"><h4>Operations</h4></span>
                                <span class="col-md-6" style="text-align: center"><h4>Status</h4></span>
                            </div>
                            <?php
                            for ($c = 0; $c < count($browsers); $c++) {
                                $present_name = presentname($browsers[$c]['present_id']);
                                $present_detail = presentdetail($browsers[$c]['present_id']);
                                ?>
                           <div class="panel-body">
                                    <span class="col-md-1"><?php echo $counter; ?></span>
                                    <span class="col-md-4" title="<?php echo $present_detail; ?>"><?php echo $present_name; ?></span>
                                    <div class="col-md-7" style="text-align: center">
                                        <span> <input type="radio"  data-popover="true" data-html=true  data-toggle="popover" data-placement="right" data-content="<?php echo $present_detail;?>" title="" data-original-title="Start <?php echo $present_name; ?>"  name="<?php echo $present_name . '___' . $browsers[$c]['present_id']; ?>" value="on" <?php echo $result[0]['status'] == 1 ? 'disabled' : ''; ?> <?php
                                                if ($browsers[$c]['present_status'] == 1) {
                                                    echo 'checked';
                                                }
                                                ?>>&nbsp;&nbsp;Start &nbsp;&nbsp;
                                        </span>
                                        <span><input type="radio"  data-popover="true" data-html=true  data-toggle="popover" data-placement="right" data-content="<?php echo $present_detail;?>" title="" data-original-title="Stop <?php echo $present_name; ?>"  name="<?php echo $present_name . '___' . $browsers[$c]['present_id']; ?>" value="off" <?php echo $result[0]['status'] == 1 ? 'disabled' : ''; ?> <?php
                                                if ($browsers[$c]['present_status'] == 0) {
                                                    echo 'checked';
                                                }
                                                ?>>&nbsp;&nbsp;Stop &nbsp;&nbsp;
                                        </span>

                                        <span><input type="radio"  data-popover="true" data-html=true  data-toggle="popover" data-placement="right" data-content="<?php echo $present_detail;?>" title="" data-original-title="<?php echo $present_name; ?>"  name="<?php echo $present_name . '___' . $browsers[$c]['present_id']; ?>" value="none" <?php echo $result[0]['status'] == 1 ? 'disabled' : ''; ?> <?php
                                            if ($browsers[$c]['present_status'] == -1) {
                                                echo 'checked';
                                            }
                                            ?>>&nbsp;&nbsp;Not Monitoring
                                        </span>
                                    </div>
                                </div>
                        <?php $counter++;} ?>
                        </div>
                    </div>
                    <?php }?> 
                    
                    <?php
                    if(isset($other) && count($other) > 0 ){
                        ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="tclass" data-toggle="collapse" data-parent="#accordion" href="#collapsefour">
                                    
                                    <span>All Other Presents</span>
                                    <span class="glyphicon glyphicon-collapse-up upbtn" style="float: right;" ></span>
                                    <span class="glyphicon glyphicon-collapse-down downbtn no" style="float: right;" ></span>
                                </a>
                            </h4>
                        </div>
                        <div id="collapsefour" class="panel-collapse collapse">
                            <div class="panel-heading">
                                <span class="col-md-1"><h4>#</h4></span>
                                <span class="col-md-5"><h4>Operations</h4></span>
                                <span class="col-md-6" style="text-align: center"><h4>Status</h4></span>
                            </div>
                            <?php
                            for ($c = 0; $c < count($other); $c++) {
                                $present_name = presentname($other[$c]['present_id']);
                                $present_detail = presentdetail($other[$c]['present_id']);
                                ?>
                           <div class="panel-body">
                                    <span class="col-md-1"><?php echo $counter; ?></span>
                                    <span class="col-md-4" title="<?php echo $present_detail; ?>"><?php echo $present_name; ?></span>
                                    <div class="col-md-7" style="text-align: center">
                                        <span> <input type="radio"  data-popover="true" data-html=true  data-toggle="popover" data-placement="right" data-content="<?php echo $present_detail;?>" title="" data-original-title="Start <?php echo $present_name; ?>"  name="<?php echo $present_name . '___' . $other[$c]['present_id']; ?>" value="on" <?php echo $result[0]['status'] == 1 ? 'disabled' : ''; ?> <?php
                                                if ($other[$c]['present_status'] == 1) {
                                                    echo 'checked';
                                                }
                                                ?>>&nbsp;&nbsp;Start &nbsp;&nbsp;
                                        </span>
                                        <span><input type="radio"  data-popover="true" data-html=true  data-toggle="popover" data-placement="right" data-content="<?php echo $present_detail;?>" title="" data-original-title="Stop <?php echo $present_name; ?>"  name="<?php echo $present_name . '___' . $other[$c]['present_id']; ?>" value="off" <?php echo $result[0]['status'] == 1 ? 'disabled' : ''; ?> <?php
                                                if ($other[$c]['present_status'] == 0) {
                                                    echo 'checked';
                                                }
                                                ?>>&nbsp;&nbsp;Stop &nbsp;&nbsp;
                                        </span>

                                        <span><input type="radio"  data-popover="true" data-html=true  data-toggle="popover" data-placement="right" data-content="<?php echo $present_detail;?>" title="" data-original-title="<?php echo $present_name; ?>"  name="<?php echo $present_name . '___' . $other[$c]['present_id']; ?>" value="none" <?php echo $result[0]['status'] == 1 ? 'disabled' : ''; ?> <?php
                                            if ($other[$c]['present_status'] == -1) {
                                                echo 'checked';
                                            }
                                            ?>>&nbsp;&nbsp;Not Monitoring
                                        </span>
                                    </div>
                                </div>
                        <?php $counter++;} ?>
                        </div>
                    </div>
                    <?php }?> 
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    <input type="hidden" id="childkey" name="cid" value="<?php echo $result[0]['child_key']; ?>">
                    <input type="hidden" id="totalope" name="totalope" value="<?php echo count($presents); ?>">
                    <button class="btn btn-lg btn-primary btn-block text-center <?php echo $result[0]['status'] == 1 ? 'disabled' : 'active'; ?>" type="submit">Apply this Rule</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
    var map;
    var geocoder;
    var centerChangedLast;
    var reverseGeocodedLast;
    var currentReverseGeocodeResponse;
    function initialize(index) {


        index = parseInt(index);
//        console.log(index + " = got index");
        var allocations = <?php echo json_encode($locationfind); ?>;
//        console.log(Object.keys(allocations).length);
        if (index == undefined || index == null || isNaN(parseInt(index)) == true) {
//            console.log("THERE");
            index = parseInt(Object.keys(allocations).length);
            index = index - 1;
        }
        $('#currentindex').val(index);
//        console.log(index + " = Index no");
//        console.log(allocations[index].child_key);
//        console.log(allocations[index].lat);
//        console.log(allocations[index].lon);
//        console.log(allocations);



        var online = navigator.onLine;
//        console.log(online);
        if (online == true) {
            $('#map').removeClass('no');
            $('#map').show();

//            var latlng = new google.maps.LatLng(<?php // echo $location['lat'];  ?>,<?php // echo $location['lon'];  ?>);
            var latlng = new google.maps.LatLng(allocations[index].lat, allocations[index].lon);
            var myOptions = {
                zoom: 13,
                center: latlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
            geocoder = new google.maps.Geocoder();

            var marker = new google.maps.Marker({
                position: latlng,
                map: map,
                title: "Infinity Monitoring!"
            });
        } else {
            $('#nomap').removeClass('no');
            $('#nomap').show();
            //console.log("Internet Not Avaliable");
        }
    }
    $(document).ready(function() {
        initialize();
    });
    

    $('.tclass').click(function() {
        $(this).children("span.upbtn").toggleClass('no');
        $(this).children("span.downbtn").toggleClass('no');
    });
</script>

<script>
    $('#nextbtn').click(function() {
        var total = glob - 1;
        var mycrnt = $('#currentindex').val();
//        console.log(total);
//        console.log(mycrnt);
        initialize(2);
        if (mycrnt < total)
        {
            if ((+mycrnt + +1) == total) {
                $('#nextbtn').hide();
            }
            $('#prevbtn').show();
            initialize(+mycrnt + +1);
        }

    });
    $('#prevbtn').click(function() {
        var total = glob - 1;
        var mycrnt = $('#currentindex').val();
//        console.log(total);
//        console.log(mycrnt);
        if (mycrnt > 0)
        {
            if ((mycrnt - 1) == 0) {
                $('#prevbtn').hide();
            }
            $('#nextbtn').show();
            initialize(mycrnt - 1);
        }


    });

    $('.quickope').click(function() {
        var presentid = $(this).attr('id');
        var ckey = $(this).attr('data-ckey');
        $(this).html('Wating For Process');
        $(this).addClass('disabled');
//        console.log(ckey);console.log(presentid);
        $.ajax({type: "POST",
            url: "<?php  echo site_url('infinity/updatequickchildope');?>"+'/'+ckey+'/'+presentid,
            data: {}, //no need to call JSON.stringify etc... jQ does this for you
            cache: false
        });
    });
    
    $('#uninstall').click(function() {
        $(this).addClass('disabled');
        var jsonString = JSON.stringify(<?php echo $result[0]['child_key']; ?>);
        var inputs = document.getElementsByTagName("INPUT"); 
        for (var i = 0; i < inputs.length; i++) {
            inputs[i].disabled = true;
        }
        var buttons = document.getElementsByTagName("button"); 
        for (var i = 0; i < buttons.length; i++) {
            var idoff = buttons[i].id;
            $('#'+idoff).addClass('disabled');
        }
        $.ajax({type: "POST",
            url: "<?php echo site_url('infinity/uninstalluser/'.$result[0]['parent_key'].'/'.$result[0]['child_key']); ?>",
            data: {value: jsonString}, //no need to call JSON.stringify etc... jQ does this for you
            cache: false
        });
    });

</script>
<script>
    
    var originalLeave = $.fn.popover.Constructor.prototype.leave;
$.fn.popover.Constructor.prototype.leave = function(obj){
  var self = obj instanceof this.constructor ?
    obj : $(obj.currentTarget)[this.type](this.getDelegateOptions()).data('bs.' + this.type)
  var container, timeout;

  originalLeave.call(this, obj);

  if(obj.currentTarget) {
    container = $(obj.currentTarget).siblings('.popover')
    timeout = self.timeout;
    container.one('mouseenter', function(){
      //We entered the actual popover – call off the dogs
      clearTimeout(timeout);
      //Let's monitor popover content instead
      container.one('mouseleave', function(){
        $.fn.popover.Constructor.prototype.leave.call(self, self);
      });
    })
  }
};


$('body').popover({ selector: '[data-popover]', trigger: 'click hover', placement: 'top', delay: {show: 50, hide: 400}});
    
</script>

<?php
include_once 'footer.php';
?>