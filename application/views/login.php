<?php
if (!isset($msg)) {
    $msg = "log in";
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>INFINITY DEVELOPERS</title>
        
        <link href="<?php echo site_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo site_url('assets/css/style.css'); ?>" rel="stylesheet">
        <link href="<?php echo site_url('assets/css/signin.css'); ?>" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo site_url('assets/css/login.css'); ?>" type="text/css"/>
        <script src="<?php echo site_url('assets/js/minijs.js'); ?>"></script>
        <script src="<?php echo site_url('assets/js/bootstrap.min.js'); ?>"></script>
    </head>
    <body>
        <div class="main">
            <div class="dummy-box"></div>
            <div class="main-box shadow">
                <!--<div class="heading">Admin Login</div>-->
                <h3 style="margin-top: 0px; color: #830505;font-family: initial;" class="form-signin-heading text-center">
                    <?php 
//                    echo  $msg ="log in" ? '':$msg; 
                    echo  $msg; 
                    
                    ?></h3>
                <div class="logo">
                    <p class="cp">
                        <!--<img src="<?php echo site_url('assets/images/logo.png'); ?>"/>-->
                        <a href="<?php echo base_url();?>"><img src="<?php echo site_url('assets/images/logo2.png'); ?>"/></a>
                    </p>
                </div>
                <form method="post" id="user_login" class="form-signin" action="<?php echo site_url('infinity/login'); ?>" >
                    <h2 style="margin-top: 0px;" class="form-signin-heading text-center"><?php echo ''; ?></h2>
                    <input type="email" class="form-control" name="emailid" id="emailid" placeholder="Email Id" required="" autofocus="" value="<?php //if (isset($username)) {echo $username;}?>" />
                    <input type="password" class="form-control" placeholder="Password" name="password" id="password" value="" required="">
                    
                    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
                    <div><a href="<?php echo base_url();?>" style="color:maroon;font-weight: bold;"><span class="glyphicon glyphicon-arrow-left"></span> go back...</a></div>
                    <a class="r-password" href="<?php echo site_url('infinity/forgot_password'); ?>">Forgot Password ?</a>
                    <?php if(!isset($username)){ ?>
                    <!--<h4 style="text-align: center;width: 100%;margin-bottom: 0;">-->
                       <span class="orspan"> OR</span> <a class="r-password-2" href="<?php echo site_url('infinity/signup'); ?>">Sign-UP</a>
                    <!--</h4>-->
                    <?php } ?>
                </form>
                <!--<div class="logo"><p class="cp"><img src="<?php //echo site_url('assets/images/logo.png'); ?>"/></p></div>-->
            </div>
            
        </div>
    </body>
</html>
