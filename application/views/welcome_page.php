<!DOCTYPE HTML>
<html>   
    <head>
	<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv="X-UA-Compatible" content="IE=9" />
        <meta content="infinity, spy, monitoring, uol, hassanqadirshah, INFINITY, child, controll" name="keywords">
        <meta name="description" content="infinity spy & monitoring Now you can keep yourself safe, monitor your colleague's, family and friends. Our services helps you in your course of action, Gives you the only thing you always wanted Control">
        <meta property="og:title" content="Infinity Spy And Monitoring" />
        <meta property="og:description" content="infinity spy & monitoring Now you can keep yourself safe, monitor your colleague's, family and friends. Our services helps you in your course of action, Gives you the only thing you always wanted Control" />
        <meta property="og:url" content="http://www.theidcloud.com/">
        <meta property="og:image" content="http://www.theidcloud.com/assets/images/head-logo.png">
        <meta property="og:site_name" content="Infinity Spy And Monitoring" />
        <!--        <meta property="fb:app_id" content="886785177998352"/>-->
        <title>Infinity Spy And Monitoring</title>
		<!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
		<script src="<?php echo site_url('assets/mainpage/js/jquery.min.js'); ?>"></script>
		<script src="<?php echo site_url('assets/mainpage/js/jquery.poptrox.min.js'); ?>"></script>
		<script src="<?php echo site_url('assets/mainpage/js/skel.min.js'); ?>"></script>
		<script src="<?php echo site_url('assets/mainpage/js/init.js'); ?>"></script>
		
<!--                <link rel="stylesheet" href="<?php echo site_url('assets/mainpage/css/style.css'); ?>" />
                <link rel="stylesheet" href="<?php echo site_url('assets/mainpage/css/style-wide.css'); ?>" />
                <link rel="stylesheet" href="<?php echo site_url('assets/mainpage/css/style-normal.css'); ?>" />-->
                
                <noscript>
			<link rel="stylesheet" href="<?php echo site_url('assets/mainpage/css/skel-noscript.css'); ?>" />
			<link rel="stylesheet" href="<?php echo site_url('assets/mainpage/css/style.css'); ?>" />
		</noscript>
		<!--[if lte IE 8]><link rel="stylesheet" href="<?php echo site_url('assets/mainpage/css/ie/v8.css'); ?>css/ie/v8.css" /><![endif]-->
	</head>
        <body>

		<!-- Header -->
			<header id="header">

				<!-- Logo -->
					<h1 id="logo">
                                           <a href="<?php echo base_url();?>"><img class="image" src="<?php echo '/assets/images/logomp.png';?>"/></a>
                                            <!--<a href="#">Big Picture</a>-->
                                            <a class="loginbtn" href="<?php echo site_url('infinity/login'); ?>"><img class="image" style="display: block;" src="<?php echo '/assets/images/logomp.png';?>"/></a>
                                        </h1>
				
				<!-- Nav -->
					<nav id="nav">
                                            <ul>
                                                <li><a href="#intro">Portal</a></li>
                                                <li><a href="#one">How It Works</a></li>
                                                <li><a href="#two">About ISM</a></li>
                                                <li><a href="#work">About Team</a></li>
                                                <li><a href="#contact">Contact Us</a></li>
                                                <li><a href="<?php echo site_url('infinity/login'); ?>">Login</a></li>
                                                <li><a href="<?php echo site_url('infinity/signup'); ?>">Sign-Up</a></li>
                                            </ul>
					</nav>

			</header>
			
		<!-- Intro -->
			<section id="intro" class="main style1 dark fullscreen">
				<div class="content container small">
					<header>
						<h2>Hey.</h2>
					</header>
					<p>Welcome to <strong>infinity spy & monitoring</strong> Now you can keep yourself safe, monitor your colleague's, family and friends. Our services helps you in your course of action, Gives you the only thing you always wanted <strong><i>"Control"</i></strong></p>
					<footer>
						<a href="#one" class="button style2 down">More</a>
					</footer>
				</div>
			</section>
		
		<!-- One -->
			<section id="one" class="main style2 right dark fullscreen">
				<div class="content box style2">
					<header>
						<h2>What It Works</h2>
					</header>
					<p>
                                            <strong>Step 1:</strong> Join us <br/>
                                            <strong>Step 2:</strong> Verify Your Identity By Email.<br/>
                                            <strong>Step 3:</strong> login your account in web portal<br/>
                                            <strong>Step 4:</strong> Create New Target.<br/>
                                            <strong>Step 5:</strong> Install software on Your target System<br/>
                                            <strong>Step 6:</strong> Ready to go.<br/>
                                        </p>
				</div>
				<a href="#two" class="button style2 down anchored">Next</a>
			</section>
		
		<!-- Two -->
			<section id="two" class="main style2 left dark fullscreen">
				<div class="content box style2">
					<header>
						<h2>About ISM</h2>
					</header>
					<p>
                                            <strong>Infinity spy & Monitoring</strong> is software devoted to protect you and your love ones. 
                                                    We're working hard to create tools to give you control to monitor while 
                                                    <i><b>protecting your identity and retaining your privacy</b></i>.
                                        </p>
				</div>
				<a href="#work" class="button style2 down anchored">Next</a>
			</section>
			
		<!-- Work -->
			<section id="work" class="main style3 primary">
				<div class="content container">
					<header>
						<h2>About Team</h2>
                                                <p>
                                                    <strong>In Our Team, </strong>
                                                    Syed Ahsan Shah Play's a Role Of <strong>DATA ANALYST</strong> 
                                                    Muhammad Hammad Hassan As a <strong>LEAD DEVELOPER</strong>
                                                    <strong> And </strong>
                                                    Saeed-Ul-Hassan As a <strong>WEB DEVELOPER AND DESIGNER</strong>.
                                                </p>
					</header>
					
					<!--
						 Lightbox Gallery
						 
						 Powered by poptrox. Full docs here: https://github.com/n33/jquery.poptrox
					-->
                                        <div class="container small gallery">
                                            <div class="row flush images">
                                                <div class="4u"><a href="<?php echo '/assets/images/admin/ahsan_b.png'; ?>" class="image full l"><img src="<?php echo '/assets/images/admin/ahsan_s.png'; ?>" title="SYED AHSAN( DATA ANALYST )" alt="" /></a></div>
                                                <div class="4u"><a href="<?php echo '/assets/images/admin/hammad_b.png'; ?>" class="image full r"><img src="<?php echo '/assets/images/admin/hammad_s.png'; ?>" title="Muhammad Hammad ( LEAD DEVELOPER )" alt="" /></a></div>
                                                <div class="4u"><a href="<?php echo '/assets/images/admin/hassan_b.png'; ?>" class="image full l"><img src="<?php echo '/assets/images/admin/hassan_s.png'; ?>" title="Saeed Ul Hassan ( WEB DEVELOPER AND DESIGNER )" alt="" /></a></div>
                                            </div>
                                        </div>

				</div>
			</section>
			
		<!-- Contact -->
			<section id="contact" class="main style3 secondary">
				<div class="content container">
					<header>
						<h2>Say Hello.</h2>
						<p>provide Your precious Advice And Feedback Here..... </p>
					</header>
					<div class="box container small">
					
						<!--
							 Contact Form
							 
							 To get this working, place a script on your server to receive the form data, then
							 point the "action" attribute to it (eg. action="http://mydomain.tld/mail.php").
							 More on how it all works here: http://www.1stwebdesigner.com/tutorials/custom-php-contact-forms/
						-->
                                                <form method="post" action="<?php echo base_url(); ?>">
                                                    <div class="row half">
                                                        <div class="6u"><input type="text" name="name" placeholder="Name" /></div>
                                                        <div class="6u"><input type="text" name="email" placeholder="Email" /></div>
                                                    </div>
                                                    <div class="row half">
                                                        <div class="12u"><textarea name="message" placeholder="Message" rows="6"></textarea></div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="12u">
                                                            <ul class="actions">
                                                                <li><input type="submit" class="button" value="Send Message" /></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </form>

					</div>
				</div>
			</section>
			
		<!-- Footer -->
			<footer id="footer">
			
				<!--
				     Social Icons
				     
				     Use anything from here: http://fortawesome.github.io/Font-Awesome/cheatsheet/)
				-->
					<ul class="actions">
						<li><a href="#" class="fa solo fa-twitter"><span>Twitter</span></a></li>
						<li><a href="#" class="fa solo fa-facebook"><span>Facebook</span></a></li>
						<li><a href="#" class="fa solo fa-google-plus"><span>Google+</span></a></li>
<!--						<li><a href="#" class="fa solo fa-dribbble"><span>Dribbble</span></a></li>
						<li><a href="#" class="fa solo fa-pinterest"><span>Pinterest</span></a></li>
						<li><a href="#" class="fa solo fa-instagram"><span>Instagram</span></a></li>-->
					</ul>
                                
                                <p style="text-align: center;">&copy; infinitydevz.com 2016</p>
                                
				<!-- Menu -->
<!--					<ul class="menu">
						<li>&copy; Untitled</li>
						<li>Design: <a href="http://html5up.net/">HTML5 UP</a></li>
					</ul>-->
                                    <ul class="menu">
						<li><span id="siteseal"><script type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=fsFWc4UqoSTra7iJcEXFk6s9INbSEY4OkjPKQW1b725gZiRDgLnFuNYlAnzK"></script></span></li>
					</ul>
			</footer>

	</body>
</html>