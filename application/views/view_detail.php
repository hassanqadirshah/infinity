<?php
include_once 'header.php';

//echo "<pre>";
//print_r($result);
//echo "</pre>";
?>
<div class="highlight">
    <div class="col-md-12">
        <div class="col-md-7">
            <h4 class="text-center">YOUR BASIC INFO</h4>
            <table class="table table-striped">
                <tr>
                    <td>Name</td>
                    <td><?php echo $result[0]['name']; ?></td>
                </tr>
                <tr>
                    <td>Father Name</td>
                    <td><?php echo $result[0]['father_name']; ?></td>
                </tr>
                <tr>
                    <td>CNIC #</td>
                    <td><?php echo $result[0]['cnic_no']; ?></td>
                </tr>
                <tr>
                    <td>Gender</td>
                    <td><?php echo $result[0]['gender']==1?"Male":'Female'; ?></td>
                </tr>
                <tr>
                    <td>Mobile #</td>
                    <td><?php echo $result[0]['mobile_no']; ?></td>
                </tr>
                <tr>
                    <td>Email ID</td>
                    <td><?php echo $result[0]['email']; ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>

<?php
include_once 'footer.php';
?>