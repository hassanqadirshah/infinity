<?php
include_once 'header.php';
?>

<?php
?>

<div class="container" id="content">
    <div class="row">
        <div class="col-md-4"></div>
        <div class="col-md-4">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Virus</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Capture Screen</td>

                            <td id="callback-toggle-button">
                                <label class="switch-light well" onclick="">
                                    <input type="checkbox"><span><span>Off</span><span>On</span></span><a class="btn btn-primary"></a>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>History</td>
                            <td>
                                <label class="switch-light well" onclick="">
                                    <input type="checkbox"><span><span>Off</span><span>On</span></span><a class="btn btn-primary"></a>
                                </label>
                            </td>

                        </tr>
                        <tr>
                            <td>3</td>
                            <td>hover mouse</td>
                            <td>
                                <label class="switch-light well" onclick="">
                                    <input type="checkbox"><span><span>Off</span><span>On</span></span><a class="btn btn-primary"></a>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>Hang PC</td>
                            <td>
                                <label class="switch-light well" onclick="">
                                    <input type="checkbox"><span><span>Off</span><span>On</span></span><a class="btn btn-primary"></a>
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>Shut Down PC</td>
                            <td>
                                <label class="switch-light well" onclick="">
                                    <input type="checkbox"><span><span>Off</span><span>On</span></span><a class="btn btn-primary"></a>
                                </label>
                            </td>
                        </tr>

                    </tbody>
                </table>
            </div>

        </div>


    </div>

</div>
<script>
    (function(i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function() {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-19824700-1', 'ghinda.net');
    ga('send', 'pageview');
</script>


<?php
include_once 'footer.php';
?>