<!DOCTYPE html>
<html>
    <head>
        <title>INFINITY DEVELOPERS</title>
        <link href="<?php echo site_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo site_url('assets/css/style.css'); ?>" rel="stylesheet">
        <link href="<?php echo site_url('assets/css/signin.css'); ?>" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo site_url('assets/css/login.css'); ?>" type="text/css"/>
        <script src="<?php echo site_url('assets/js/minijs.js'); ?>"></script>
        <script src="<?php echo site_url('assets/js/bootstrap.min.js'); ?>"></script>
    </head>
    <body>
        <div class="main">
            <div class="dummy-box"></div>
            
            <div class="main-box shadow" style="height: 295px;">
                <h2 style="margin-top: 0px;" class="form-signin-heading text-center"><?php echo $msg;?></h2>
                <div class="logo">
                    <p class="cp">
                        <a href="<?php echo base_url();?>"><img src="<?php echo site_url('assets/images/logo2.png'); ?>"/></a>
                    </p>
                </div>
                <?php 
                if($success == 0){
                ?>
                <form method="post" id="forgot-password" class="form-signin" action="<?php echo site_url('infinity/forgot_password'); ?>" >
                    
                    <input type="email" class="form-control" name="emailid" id="emailid" placeholder="Email Id" required="" autofocus="" value="<?php //if (isset($username)) {echo $username;}?>" />
                    <button class="btn btn-lg btn-primary btn-block" style="margin-top: 5px;" type="submit">Reset Password</button>
                    <a href="<?php echo base_url();?>infinity/login" style="color:maroon;font-weight: bold;"><span class="glyphicon glyphicon-arrow-left"></span> go back...</a>
                </form>
                <?php } ?>
            </div>
            
        </div>
    </body>
</html>
