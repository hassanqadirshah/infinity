<?php
include_once 'header.php';
?>
<style>
    #options-content{
        width: 850px;
    }
    #ajax_list .span12 table{
        width: 73% !important;
    }

</style>
<div class="clear"></div>
<!-- CONTENT START -->
<div class="grid_16" id="content">
    <!--  TITLE START  --> 
    <div class="grid_9">
        <h1 class="dashboard">Operations</h1>
    </div>

    <div class="clear">
    </div>
    <!--  TITLE END  -->    
    <!-- #PORTLETS START -->
    <div id="portlets">

        <!--Last 30 Days Registered Users List-->
        <div class="portlet">
            <div class="portlet-header fixed"><img src="<?php echo site_url('assets/images/icons/user.gif'); ?>" width="16" height="16" alt="Latest Registered Users" /> Email Inbox</div>
            <div class="portlet-content nopadding">
                <div class="span9" style="margin-left: 0;">
                    <?php echo $output; ?>
                </div>
            </div>
        </div>
        <div class="column" id="left" style="width: 100%;"></div>
    </div>
    <div class="clear"> </div>
</div>
<div class="clear">
</div>
</div>
<div class="clear"></div>
<div align=center>Developed And Designed By <a href='http://infinity-developers.iserver.purelogics.info/infinity/infinity/login'>Infinity Developers</a></div>

</body>
</html>
