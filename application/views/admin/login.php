<?php
if (!isset($msg)) {
    $msg = "Hay Admin";
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>INFINITY</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="<?php echo site_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo site_url('assets/css/style.css'); ?>" rel="stylesheet">
        <link href="<?php echo site_url('assets/css/signin.css'); ?>" rel="stylesheet">
        <!--<script src="https://code.jquery.com/jquery.js"></script>-->
        <script src="<?php echo site_url('assets/js/minijs.js'); ?>"></script>
        <script src="<?php echo site_url('assets/js/bootstrap.min.js'); ?>"></script>
    </head>
    <body>
        <div class="container">

            <form method="post" id="user_login" class="form-signin" action="<?php echo site_url('admin/login'); ?>" >
                <h2 class="form-signin-heading text-center"><?php echo $msg;?></h2>
                <div class="logo">
                    <p class="cp">
                        <img src="<?php echo site_url('assets/images/logo2.png'); ?>"/>
                    </p>
                </div>
                <input type="text" class="form-control" name="name" id="name" placeholder="User Name" required="" autofocus="">
                <input type="password" class="form-control" placeholder="Password" name="password" id="password" value="" required="">
                <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
            </form>

        </div>
    </body>
</html>