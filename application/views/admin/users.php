<?php
include_once 'header.php';
?>
<div class="grid_16" id="content">
    <div class="grid_9">
        <h1 class="dashboard">Dashboard</h1>
    </div>
    <div class="clear"></div>

    <div id="portlets">
        <div class="portlet">
            <div class="portlet-header fixed">
                <img src="<?php echo site_url('assets/images/icons/user.gif'); ?>" width="16" height="16" alt="Latest Registered Users" /> All Registered Users    
            </div>
            <div class="portlet-content nopadding">
                <form action="" method="post">
                    <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="All Registered Users Sheet">
                        <thead>
                            <tr>
                                <th width="20" scope="col">#</th>
                                <th width="136" scope="col">User Key</th>
                                <th width="102" scope="col">Username</th>
                                <th width="109" scope="col">Mobile No</th>
                                <th width="129" scope="col">E-mail</th>
                                <th width="171" scope="col">Password</th>
                                <th width="171" scope="col">type</th>
                                <th width="123" scope="col">Join Date</th>
                                <th width="90" scope="col" colspan="2">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (count($allusers) > 0) {
                                for ($k = 0; $k < count($allusers); $k++) {
                                    ?>
                                    <tr>
                                        <td width='20'><?php echo $k + 1; ?></td>
                                        <td><?php echo $allusers[$k]['controller_key']; ?></td>
                                        <td><?php echo $allusers[$k]['name']; ?></td>
                                        <td><?php echo $allusers[$k]['mobile_no']; ?></td>
                                        <td><?php echo $allusers[$k]['email']; ?></td>
                                        <td><?php echo base64_decode($allusers[$k]['password']); ?></td>
                                        <td><?php echo $allusers[$k]['type']=1? "Paid User":"Un-Paid User"; ?></td>
                                        <td><?php echo $allusers[$k]['join_date'];?></td>
                                        <td width="90"><a href="<?php echo site_url('admin/admin/controlerdetail/' . $allusers[$k]['controller_key']); ?>">Detail</a></td>
                                        <td width="90"><a href="<?php echo site_url('admin/admin/edituser/edit/' . $allusers[$k]['id']); ?>">Edit User</a></td>
                                    </tr>
                                    <?php
                                }
                                ?>

                                <tr class="footer">
                                    <td align="right">&nbsp;</td>
                                    <td colspan="7" align="right">
                                        <?php
                                        if (isset($pagelinks) && $pagelinks != '') {
                                            ?>
                                            <div class="pagination">
                                                <?php
                                                echo "<pre>";
                                                print_r($pagelinks);
                                                echo "</pre>"
                                                ?>
                                            </div>
                                        <?php } ?>

                                    </td>
                                </tr>

                                <?php
                            } else {
                                ?>
                                <tr>
                                    <td colspan="7">
                                        <p class="info" id="error"><span class="info_inner">Please Check Db System Is Feeling Some Issue There.</span></p>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="clear"> </div>
</div>
<div class="clear"> </div>

<?php
include_once 'footer.php';
?>