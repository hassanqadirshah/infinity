<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" class=" js  js flexbox canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <title>INFINITY</title>
        <!--<link href="<?php //echo site_url('assets/css/admin-style.css');         ?>" rel="stylesheet"/>-->

        <?php $theme = getadmintheme($this->session->userdata('admin_id')); ?>

        <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/admin/960.css'); ?>" />
        <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/admin/reset.css'); ?>" />
        <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/css/admin/text.css'); ?>" />
        <link rel="stylesheet" type="text/css" href="<?php
        if ($theme == 1) {
            echo site_url('assets/css/admin/blue.css');
        } else if ($theme == 2) {
            echo site_url('assets/css/admin/green.css');
        } else if ($theme == 3) {
            echo site_url('assets/css/admin/red.css');
        } else {
            echo site_url('assets/css/admin/blue.css');
        }
        ?>" />
        <link type="text/css" href="<?php echo site_url('assets/css/admin/smoothness/ui.css'); ?>" rel="stylesheet" /> 

        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Karla:400,700">
        <!--<link href="<?php //echo site_url('assets/css/lightbox/screen.css');         ?>" rel="stylesheet">-->
            <link href="<?php echo site_url('assets/css/lightbox/lightbox.css'); ?>" rel="stylesheet">
                <script src="<?php echo site_url('assets/css/lightbox/modernizr.custom.js'); ?>"></script>


                <?php
                if (isset($css_files) && isset($js_files)) {
                    foreach ($css_files as $file):
                        ?>
                        <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
                    <?php endforeach; ?>
                    <?php foreach ($js_files as $file): ?>
                        <script src="<?php echo $file; ?>"></script>
                        <?php
                    endforeach;
                }
                ?> 

                <?php
                if (!isset($css_files) && !isset($js_files)) {
                    ?>
                    <script type="text/javascript" src="<?php echo site_url('assets/css/admin/js/jquery.min.js'); ?>"></script>
                    <script type="text/javascript" src="<?php echo site_url('assets/css/admin/js/ui.datepicker.js'); ?>"></script>

                    <?php
                }
                ?>



                <script type="text/javascript" src="<?php echo site_url('assets/css/admin/js/blend/jquery.blend.js'); ?>"></script>
                <script type="text/javascript" src="<?php echo site_url('assets/css/admin/js/ui.core.js'); ?>"></script>
                <script type="text/javascript" src="<?php echo site_url('assets/css/admin/js/ui.sortable.js'); ?>"></script>    
                <script type="text/javascript" src="<?php echo site_url('assets/css/admin/js/ui.dialog.js'); ?>"></script>

                <script type="text/javascript" src="<?php echo site_url('assets/css/admin/js/effects.js'); ?>"></script>              
                <script type="text/javascript" src="<?php echo site_url('assets/css/admin/js/flot/jquery.flot.pack.js'); ?>"></script>
                <script id="source" language="javascript" type="text/javascript" src="<?php echo site_url('assets/css/admin/js/graphs.js'); ?>"></script>

<!--<script type="text/javascript" src="../../ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>-->

                <!--                    
                                <script type="text/javascript" src="<?php echo site_url('assets/css/admin/js/jquery.min.js'); ?>"></script>
                                <script type="text/javascript" src="<?php echo site_url('assets/css/admin/js/blend/jquery.blend.js'); ?>"></script>
                                <script type="text/javascript" src="<?php echo site_url('assets/css/admin/js/ui.core.js'); ?>"></script>
                                <script type="text/javascript" src="<?php echo site_url('assets/css/admin/js/ui.sortable.js'); ?>"></script>    
                                <script type="text/javascript" src="<?php echo site_url('assets/css/admin/js/ui.dialog.js'); ?>"></script>
                                <script type="text/javascript" src="<?php echo site_url('assets/css/admin/js/ui.datepicker.js'); ?>"></script>
                                <script type="text/javascript" src="<?php echo site_url('assets/css/admin/js/effects.js'); ?>"></script>
                                <script type="text/javascript" src="<?php echo site_url('assets/css/admin/js/flot/jquery.flot.pack.js'); ?>"></script>
                                <script id="source" language="javascript" type="text/javascript" src="<?php echo site_url('assets/css/admin/js/graphs.js'); ?>"></script>
                -->


                </head>
                <body>
                    <div class="container_16" id="wrapper">	

                        <!--LOGO-->
                        <div class="grid_8" id="logo">Infinity Developers - Administration Panel</div>
                        <div class="grid_8">
                            <!-- USER TOOLS START -->
                            <div id="user_tools"><span><a href="<?php echo site_url('admin/admin/emailinbox'); ?>" class="mail"></a> Welcome Admin  |  <a href="<?php echo site_url('admin/admin/signout'); ?>">Logout</a></span></div>
                        </div>
                        <!-- USER TOOLS END -->    
                        <div class="grid_16" id="header">
                            <!-- MENU START -->
                            <div id="menu">
                                <?php $pname = $this->router->method; ?>
                                <ul class="group" id="menu_group_main">
                                    <li class="item first" id="one"><a href="<?php echo site_url('admin/admin/index'); ?>" class="main <?php
                                        if ($pname == 'index') {
                                            echo "current";
                                        }
                                        ?>"><span class="outer"><span class="inner dashboard">Infinity Dev</span></span></a></li>
                                    <li class="item middle" id="two"><a href="<?php echo site_url('admin/admin/emailinbox'); ?>" class="main <?php
                                        if ($pname == 'emailinbox') {
                                            echo "current";
                                        }
                                        ?>"><span class="outer"><span class="inner content">E-mails</span></span></a></li>
                                    <li class="item middle" id="three"><a href="<?php echo site_url('admin/admin/operations'); ?>" class="main <?php
                                        if ($pname == 'operations') {
                                            echo "current";
                                        }
                                        ?>"><span class="outer"><span class="inner reports png">Operations</span></span></a></li>
                                    <li class="item middle" id="four"><a href="<?php echo site_url('admin/admin/users'); ?>" class="main <?php
                                        if ($pname == 'users') {
                                            echo "current";
                                        }
                                        ?>"><span class="outer"><span class="inner users">Users</span></span></a></li>
                                    <li class="item middle" id="five"><a href="<?php echo site_url('admin/admin/allvisualbits'); ?>" class="main <?php
                                        if ($pname == 'allvisualbits') {
                                            echo "current";
                                        }
                                        ?>"><span class="outer"><span class="inner media_library">All Visual Bits</span></span></a></li>        
                                    <li class="item middle" id="six"><a href="<?php echo site_url('admin/admin/logs'); ?>" class="main <?php
                                        if ($pname == 'logs') {
                                            echo "current";
                                        }
                                        ?>"><span class="outer"><span class="inner event_manager">User Logs</span></span></a></li>        
<!--                                    <li class="item middle" id="six"><a href="#" class="main"><span class="outer"><span class="inner event_manager">User Logs</span></span></a></li>        -->
                                    <!--<li class="item middle" id="seven"><a href="<?php echo site_url('admin/admin/user_setting'); ?>" class="main <?php if ($pname == 'user_setting') {echo "current";} ?>"><span class="outer"><span class="inner newsletter">User Settings</span></span></a></li>-->        
                                    <li class="item middle" id="seven"><a href="<?php echo site_url('admin/admin/ism_updates'); ?>" class="main <?php if ($pname == 'ism_updates') {echo "current";} ?>"><span class="outer"><span class="inner newsletter">Version Updates</span></span></a></li>        
                                    <li class="item last" id="eight"><a href="<?php echo site_url('admin/admin/admin_setting'); ?>" class="main <?php if ($pname == 'admin_setting') {echo "current";} ?>"><span class="outer"><span class="inner settings">Admin Settings</span></span></a></li>        
                                </ul>
                            </div>
                            <!-- MENU END -->
                        </div>
                        <div class="grid_16">
                            <!-- TABS START -->
                            <div id="tabs">
                                <div class="container">
                                    <h2 align='center'><?php
                                        if (isset($this->pageHeading)) {
                                            echo $this->pageHeading;
                                        } else {
                                            echo '';
                                        };
                                        ?></h2>
                                    <!--                        <ul>
                                                                <li><a href="#" class="current"><span>Dashboard elements</span></a></li>
                                                                <li><a href="forms.html"><span>Content Editing</span></a></li>
                                                                <li><a href="#"><span>Submenu Link 3</span></a></li>
                                                                <li><a href="#"><span>Submenu Link 4</span></a></li>
                                                                <li><a href="#"><span>Submenu Link 5</span></a></li>
                                                                <li><a href="#"><span>Submenu Link 6</span></a></li>
                                                                <li><a href="#" class="more"><span>More Submenus</span></a></li>            
                                                            </ul>-->
                                </div>
                            </div>
                            <!-- TABS END -->    
                        </div>
                        <!-- HIDDEN SUBMENU START -->
                        <!--            <div class="grid_16" id="hidden_submenu">
                                        <ul class="more_menu">
                                            <li><a href="#">More link 1</a></li>
                                            <li><a href="#">More link 2</a></li>  
                                            <li><a href="#">More link 3</a></li>    
                                            <li><a href="#">More link 4</a></li>                               
                                        </ul>
                                        <ul class="more_menu">
                                            <li><a href="#">More link 5</a></li>
                                            <li><a href="#">More link 6</a></li>  
                                            <li><a href="#">More link 7</a></li> 
                                            <li><a href="#">More link 8</a></li>                                  
                                        </ul>
                                        <ul class="more_menu">
                                            <li><a href="#">More link 9</a></li>
                                            <li><a href="#">More link 10</a></li>  
                                            <li><a href="#">More link 11</a></li>  
                                            <li><a href="#">More link 12</a></li>                                 
                                        </ul>            
                                    </div>-->
                        <!-- HIDDEN SUBMENU END -->
                        <style>
                            .no{
                                display: none;
                            }
                            .updateme{
                                background-color: #79b5e3;

                            }
                            /*                            .updateme:hover{
                                                            background-color: #639ECB;
                                                        }*/
                            .imgthumbnail {
                                height: 50px;
                            }
                            .redme{
                                background-color: antiquewhite;
                            }
                            .greenme{
                                background-color: aquamarine;
                            }
                            .mytarea{
                                margin-left: 2px;
                                margin-right: 0px;
                                width: 270px;
                                margin-top: 10px;
                            }
                            #crud_page{
                                left: 27px;
                                top: -22px;
                                margin-right: 23px;
                            }
                        </style>