<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
<link href="<?php echo site_url('assets/grocery_crud/themes/twitter-bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet"/>       
             <link href="<?php echo site_url('assets/grocery_crud/themes/twitter-bootstrap/css/bootstrap-responsive.min.css'); ?>" rel="stylesheet"/>       
             <link href="<?php echo site_url('assets/grocery_crud/themes/twitter-bootstrap/css/style.css'); ?>" rel="stylesheet"/>       
             <link href="<?php echo site_url('assets/grocery_crud/themes/twitter-bootstrap/css/jquery-ui/flick/jquery-ui-1.9.2.custom.css'); ?>" rel="stylesheet"/>       
                    
                    
             <script type="text/javascript" src="<?php echo site_url('assets/grocery_crud/js/jquery-1.10.2.min.js'); ?>"></script>
             <script type="text/javascript" src="<?php echo site_url('assets/grocery_crud/themes/twitter-bootstrap/js/jquery-ui/jquery-ui-1.9.2.custom.js'); ?>"></script>
             <script type="text/javascript" src="<?php echo site_url('assets/grocery_crud/js/common/lazyload-min.js'); ?>"></script>
             <script type="text/javascript" src="<?php echo site_url('assets/grocery_crud/js/common/list.js'); ?>"></script>
             <script type="text/javascript" src="<?php echo site_url('assets/grocery_crud/themes/twitter-bootstrap/js/libs/bootstrap/bootstrap.min.js'); ?>"></script>
             <script type="text/javascript" src="<?php echo site_url('assets/grocery_crud/themes/twitter-bootstrap/js/libs/bootstrap/application.js'); ?>"></script>
             <script type="text/javascript" src="<?php echo site_url('assets/grocery_crud/themes/twitter-bootstrap/js/libs/modernizr/modernizr-2.6.1.custom.js'); ?>"></script>
             <script type="text/javascript" src="<?php echo site_url('assets/grocery_crud/themes/twitter-bootstrap/js/libs/tablesorter/jquery.tablesorter.min.js'); ?>"></script>
             <script type="text/javascript" src="<?php echo site_url('assets/grocery_crud/themes/twitter-bootstrap/js/cookies.js'); ?>"></script>
             <script type="text/javascript" src="<?php echo site_url('assets/grocery_crud/themes/twitter-bootstrap/js/jquery.form.js'); ?>"></script>
             <script type="text/javascript" src="<?php echo site_url('assets/grocery_crud/js/jquery_plugins/jquery.numeric.min.js'); ?>"></script>
             <script type="text/javascript" src="<?php echo site_url('assets/grocery_crud/themes/twitter-bootstrap/js/libs/print-element/jquery.printElement.min.js'); ?>"></script>
             <script type="text/javascript" src="<?php echo site_url('assets/grocery_crud/js/jquery_plugins/jquery.fancybox-1.3.4.js'); ?>"></script>
             <script type="text/javascript" src="<?php echo site_url('assets/grocery_crud/js/jquery_plugins/jquery.easing-1.3.pack.js'); ?>"></script>
             <script type="text/javascript" src="<?php echo site_url('assets/grocery_crud/themes/twitter-bootstrap/js/app/twitter-bootstrap.js'); ?>"></script>
             <script type="text/javascript" src="<?php echo site_url('assets/grocery_crud/themes/twitter-bootstrap/js/jquery.functions.js'); ?>"></script>
<style type='text/css'>
body
{
	font-family: Arial;
	font-size: 14px;
}
a {
    color: blue;
    text-decoration: none;
    font-size: 14px;
}
a:hover
{
	text-decoration: underline;
}
</style>
</head>
<body>
	<div>
		<a href='<?php echo site_url('examples/customers_management')?>'>Customers</a> |
		<a href='<?php echo site_url('examples/orders_management')?>'>Orders</a> |
		<a href='<?php echo site_url('examples/products_management')?>'>Products</a> |
		<a href='<?php echo site_url('examples/offices_management')?>'>Offices</a> | 
		<a href='<?php echo site_url('examples/employees_management')?>'>Employees</a> |		 
		<a href='<?php echo site_url('examples/film_management')?>'>Films</a> | 
		<a href='<?php echo site_url('examples/film_management_twitter_bootstrap')?>'>Twitter Bootstrap Theme [BETA]</a> | 
		<a href='<?php echo site_url('examples/multigrids')?>'>Multigrid [BETA]</a>
		
	</div>
	<div style='height:20px;'></div>  
    <div>
		<?php echo $output; ?>
    </div>
</body>
</html>
