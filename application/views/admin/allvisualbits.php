<?php include_once 'header.php'; ?>
<div class="grid_16" id="content">
    <div class="grid_9">
        <h1 class="dashboard">Dashboard</h1>
    </div>
    <div class="clear"></div>

    <div id="portlets">
        <div class="portlet">
            <div class="portlet-header fixed">
                <img src="<?php echo site_url('assets/images/icons/user.gif'); ?>" width="16" height="16" alt="Latest Registered Users" /> All Visual Bits    
            </div>
            <div class="portlet-content nopadding">
                <form action="" method="post">
                    <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="All Registered Users Sheet">
                        <thead>
                            <tr style="border-bottom: 1px solid;">
                                <th width="20" scope="col">#</th>
                                <th width="136" scope="col">View Controller</th>
                                <th width="102" scope="col">View Target</th>
                                <th width="109" scope="col">Captured On</th>
                                <th width="129" scope="col">Pic</th>
                                <th width="100" scope="col" style="text-align: right;">Actions</th>
                                <th width="70" scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (count($visualbits) > 0) {
                                for ($k = 0; $k < count($visualbits); $k++) {
                                    ?>
                                    <tr class="<?php
                                    if ($visualbits[$k]['status'] == 1) {
                                        echo "redme";
                                    } else if ($visualbits[$k]['recoverreqest'] == 1) {
                                        echo "greenme";
                                    }
                                    ?>">
                                        <td width='20'><?php echo $k + 1; ?></td>
                                        <td>
                                            <a href="<?php echo site_url('admin/admin/controlerdetail/' . $visualbits[$k]['parent_key']); ?>"><?php echo $visualbits[$k]['parent_key']; ?></a>
                                        </td>
                                        <td>
                                            <a href="<?php echo site_url('admin/admin/targetdetail/' . $visualbits[$k]['child_key']); ?>"><?php echo $visualbits[$k]['child_key']; ?></a>
                                        </td>
                                        <td><?php echo $visualbits[$k]['time-date']; ?></td>
                                        <td>
                                            <a class="example-image-link <?php echo $visualbits[$k]['child_key']; ?>" style="margin:0 10px 5px 10px" href="<?php echo site_url('upload/' . $visualbits[$k]['child_key'] . '/' . $visualbits[$k]['pic_name']); ?>" data-lightbox="example-set" title="Visual Bits">
                                                <img class='imgthumbnail' src='<?php echo site_url('upload/' . $visualbits[$k]['child_key'] . '/' . $visualbits[$k]['pic_name']); ?>'>
                                            </a>
                                        </td>

                                        <?php
                                        if ($visualbits[$k]['recoverreqest'] == 1) {
                                            ?>
                                            <td><a href='<?php echo site_url('admin/admin/recoverthis/' . $visualbits[$k]['child_key'] . '/' . $visualbits[$k]['id']); ?>'>Recover this</a></td>
                                            <?php
                                        } else {
                                            ?>
                                            <td></td>
                                            <?php
                                        }
                                        ?>
                                        <td><a href='<?php echo site_url('admin/admin/deletethis/' . $visualbits[$k]['child_key'] . '/' . $visualbits[$k]['id'] . '/' . $visualbits[$k]['pic_name']); ?>'>Delete</a></td>
                                    </tr>
                                    <?php
                                }
                                ?>

                                <tr class="footer">
                                    <td align="right">&nbsp;</td>
                                    <td colspan="7" align="right">
                                        <?php
                                        if (isset($pagelinks) && $pagelinks != '') {
                                            ?>
                                            <div class="pagination">
                                                <?php
                                                echo "<pre>";
                                                print_r($pagelinks);
                                                echo "</pre>"
                                                ?>
                                            </div>
                                        <?php } ?>

                                    </td>
                                </tr>

                                <?php
                            } else {
                                ?>
                                <tr>
                                    <td colspan="7">
                                        <p class="info" id="error"><span class="info_inner">Please Check Db System Is Feeling Some Issue There.</span></p>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="clear"> </div>
</div>
<div class="clear"> </div>

<?php
include_once 'footer.php';
?>