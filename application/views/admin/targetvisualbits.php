<?php
include_once 'header.php';
//echo "<pre>";
//print_r($user);
//exit;
?>
<style>
    #box-table-a thead tr th{
        font-weight: bold;
        /*text-align: center;*/
    }
    .imgthumbnail{
        height: 50px;
    }
</style>
<div class="clear">
</div>
<!-- CONTENT START -->
<div class="grid_16" id="content">
    <!--  TITLE START  --> 
    <div class="grid_9">
        <h1 class="dashboard"><?php echo $visualbits[0]['child_key']; ?> Profile</h1>
    </div>

    <div class="clear">
    </div>
    <!--  TITLE END  -->    
    <!-- #PORTLETS START -->
    <div id="portlets">

        <!--Last 30 Days Registered Users List-->
        <div class="portlet">
            <div class="portlet-header fixed"><img src="<?php echo site_url('assets/images/icons/user.gif'); ?>" width="16" height="16" alt="Latest Registered Users" /> User Request To Recover </div>
            <div class="portlet-content nopadding">
                
                    <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Registered Users Sheet">
                        <thead>
                            <?php $k = 0; ?>
                            <tr>
                                <th width="30" scope="col">#</th>
                                <th width="100" scope="col">Controller Key</th>
                                <th width="100" scope="col">This Target Key</th>
                                <th width="150" scope="col">Request To Recover</th>
                                <th width="150" scope="col">Taken On</th>
                                <th width="150" scope="col">Image</th>
                                <th width="150" scope="col" colspan="2">Action's</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $recoverreqest = 0;
                            for ($p = 0; $p < count($visualbits); $p++) {
                                if ($visualbits[$p]['recoverreqest'] == 1) {
                                    $recoverreqest++;
                                    ?>
                                    <tr>
                                        <td><?php echo $recoverreqest; ?></td>
                                        <td><?php echo $visualbits[$p]['parent_key']; ?></td>
                                        <td><?php echo $visualbits[$p]['child_key']; ?></td>
                                        <td><?php echo $visualbits[$p]['request_on']; ?></td>
                                        <td><?php echo $visualbits[$p]['time-date']; ?></td>
                                        <td>
                                            <a class="example-image-link <?php echo $visualbits[$p]['child_key']; ?>" style="margin:0 10px 5px 10px" href="<?php echo site_url('upload/' . $visualbits[$p]['child_key'] . '/' . $visualbits[$p]['pic_name']); ?>" data-lightbox="example-set" title="<?php echo "Admin View"; ?>">
                                                <img class='imgthumbnail' src='<?php echo site_url('upload/' . $visualbits[$p]['child_key'] . '/' . $visualbits[$p]['pic_name']); ?>'>
                                            </a>
                                        </td>
                                        <td><a href='<?php echo site_url('admin/admin/recoverthis/' . $visualbits[$p]['child_key'] . '/' . $visualbits[$p]['id']); ?>'>Recover this</a></td>
                                        <td><a href='<?php echo site_url('admin/admin/deletethis/' . $visualbits[$p]['child_key']. '/' . $visualbits[$p]['id']. '/' . $visualbits[$p]['pic_name']); ?>'>Delete</a></td>
                                    </tr>
                                    <?php
                                }
                            }
                            if ($recoverreqest == 0) {
                                ?>
                                <tr>
                                    <td colspan="7">
                                        <p class="info" id="info"><span class="info_inner">Have No Visual Bits With Recover Request</span></p>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                    </table>
                
            </div>
        </div>
        <!--  End Last 30 Days Registered Users List -->
        <div class="column" id="left" style="width: 100%;">
            <!--THIS IS A PORTLET-->
            <div class="portlet">
                <div class="portlet-header">
                    <img src="<?php echo site_url('assets/images/icons/user.gif'); ?>" width="16" height="16" alt="Target List" /> List of this User Target's</div>
                <div class="portlet-content">

                    <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="">
                        <thead>
                            <?php $k = 0; ?>
                            <tr>
                                <th width="30" scope="col">#</th>
                                <th width="100" scope="col">Controller Key</th>
                                <th width="100" scope="col">This Target Key</th>
                                <th width="150" scope="col">Request To Recover</th>
                                <th width="150" scope="col">Taken On</th>
                                <th width="150" scope="col">Image</th>
                                <th width="150" scope="col" colspan="2">Action's</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            for ($p = 0; $p < count($visualbits); $p++) {
                                ?>
                                <tr>
                                    <td><?php echo $p + 1; ?></td>
                                    <td><?php echo $visualbits[$p]['parent_key']; ?></td>
                                    <td><?php echo $visualbits[$p]['child_key']; ?></td>
                                    <td><?php echo $visualbits[$p]['request_on']; ?></td>
                                    <td><?php echo $visualbits[$p]['time-date']; ?></td>
                                    <td>
                                        <a class="example-image-link <?php echo $visualbits[$p]['child_key']; ?>" style="margin:0 10px 5px 10px" href="<?php echo site_url('upload/' . $visualbits[$p]['child_key'] . '/' . $visualbits[$p]['pic_name']); ?>" data-lightbox="example-set" title="<?php echo "Admin View"; ?>">
                                            <img class='imgthumbnail' src='<?php echo site_url('upload/' . $visualbits[$p]['child_key'] . '/' . $visualbits[$p]['pic_name']); ?>'>
                                        </a>
                                    </td>
                                    <?php if ($visualbits[$p]['delete_permanent'] == 0) { ?>
                                        <td><a href='<?php echo site_url('admin/admin/deletethis/' . $visualbits[$p]['child_key'] . '/' . $visualbits[$p]['id'] . '/' . $visualbits[$p]['pic_name']); ?>'>Delete</a></td>
                                    <?php } ?>
                                </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>      
        </div>
    </div>
    <div class="clear"> </div>
</div>
<div class="clear">
</div>
<?php
include_once 'footer.php';
//echo "<pre>";print_r($admin_detail);exit;
?>
