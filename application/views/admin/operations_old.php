<?php
include_once 'header.php';
?>
<style>
    #box-table-a tr td{cursor: pointer;}
    .boldall{font-weight: bolder;background-color: #79b5e3;}
    .boldall:hover{background-color: #639ECB !important;}
    #box-table-a tr:hover td {
        background: none; 
        color: #333;
    }
</style>
<div class="grid_16" id="content">
    <div class="grid_9">
        <h1 class="dashboard">Dashboard</h1>
    </div>
    <div class="clear"></div>

    <div id="portlets">
        <div class="portlet">
            <div class="portlet-header fixed">
                <img src="<?php echo site_url('assets/images/icons/user.gif'); ?>" width="16" height="16" alt="Latest Registered Users" /> List Of All Operations    
            </div>
            <div class="portlet-content nopadding">

                <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a">
                    <thead>
                        <tr>
                            <th width="20" scope="col">#</th>
                            <th width="200" scope="col">Operation Name</th>
                            <th width="200" scope="col">Process Name</th>
                            <th width="310" scope="col">Description</th>
                            <th width="100" scope="col">Avaliable For</th>
                            <th width="80" scope="col">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (count($operations) > 0) {
                            for ($k = 0; $k < count($operations); $k++) {
                                ?>
                                <tr class="clickme" id="clicked<?php echo $k; ?>" onclick="showbox(<?php echo $k; ?>);">
                                    <td width='20'><?php echo $k + 1; ?></td>
                                    <td width="200"><?php echo $operations[$k]['present_name']; ?></td>
                                    <td width="200"><?php echo $operations[$k]['present_process_name']; ?></td>
                                    <td width="330"><?php echo $operations[$k]['present_description']; ?></td>
                                    <td width="80"><?php
                                        if ($operations[$k]['avaliable_for'] == 0) {
                                            echo "All";
                                        } else if ($operations[$k]['avaliable_for'] == 1) {
                                            echo "Only Paids";
                                        } else if ($operations[$k]['avaliable_for'] == 2) {
                                            echo "None";
                                        }
                                        ?></td>
                                    <td width="80">
        <!--                                            <a class="example-image-link" style="margin:0 10px 5px 10px" href="<?php echo site_url('admin/admin/updateoperation/' . $operations[$k]['id']); ?>" data-lightbox="example-set" title="<?php echo "Admin View"; ?>">-->
                                        Update
                                        <!--</a>-->
                                    </td>
                                </tr>


                            <form method="post" name="upchild" action="<?php echo site_url('admin/admin/updateoperation'); ?>">
                                <tr class="no updateme" id="update<?php echo $k; ?>">
                                    <td width='20'><?php echo $k + 1; ?></td>
                                    <td width='200'><input type="text" id="present_name" name="present_name" value="<?php echo $operations[$k]['present_name']; ?>"/></td>
                                    <td width='200'><input type="text" id="process_name" name="process_name" value="<?php echo $operations[$k]['present_process_name']; ?>"/></td>
                                    <td width='330'><input type="text" id="process_description" name="process_description" value="<?php echo $operations[$k]['present_description']; ?>"/></td>

                                    <td width='80'>
                                        <select name="av_for" id="av_for">
                                            <option value="0"  <?php
                                            if ($operations[$k]['avaliable_for'] == 0) {
                                                echo "selected";
                                            }
                                            ?> >All</option>
                                            <option value="1" <?php
                                            if ($operations[$k]['avaliable_for'] == 1) {
                                                echo "selected";
                                            }
                                            ?> >Paid</option>
                                            <option value="2" <?php
                                            if ($operations[$k]['avaliable_for'] == 2) {
                                                echo "selected";
                                            }
                                            ?> >None</option>
                                        </select>
                                    </td>
                                    <td width="80">
                                        <input type="submit" value="Update This"/>
                                    </td>
                                <input type="hidden" id="processid" name="process_id" value="<?php echo $operations[$k]['id']; ?>"/>

                                </tr>
                            </form>
                            <?php
                        }
                    } else {
                        ?>
                        <tr>
                            <td colspan="7">
                                <p class="info" id="error"><span class="info_inner">Please Check Db System Is Feeling Some Issue There.</span></p>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>

            </div>
        </div>
        <div class="clear"></div>
    </div>
    <div id="portlets">
        <div class="portlet">
            <div class="portlet-header fixed">
                <img src="<?php echo site_url('assets/images/icons/user.gif'); ?>" width="16" height="16" alt="Latest Registered Users" /> List Of All Operations    
            </div>
            <div class="portlet-content nopadding">
                <form method="post" name="upchild" action="<?php echo site_url('admin/admin/addoperation'); ?>">
                    <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a">
                        <thead>
                            <tr>
                                <th width="20" scope="col">#</th>
                                <th width="100" scope="col">Operation Name</th>
                                <th width="100" scope="col">Process Name</th>
                                <th width="240" scope="col">Description</th>
                                <th width="70" scope="col">Avaliable For</th>
                                <th width="80" scope="col">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="clickme">
                                <td width='20'><?php echo 1; ?></td>
                                <td><input type="text" id="present_name" name="present_name"/></td>
                                <td><input type="text" id="process_name" name="process_name"/></td>
                                <td><textarea class="mytarea" id="process_description" name="process_description" placeholder="Add Operation Detail"></textarea></td>
                                <td>
                                    <select name="av_for" id="av_for">
                                        <option value="0">All</option>
                                        <option value="1" selected="selected">Paid</option>
                                        <option value="2">None</option>
                                    </select>
                                </td>
                                <td>
                                    <input type="submit" value="Add This"/>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </form>

            </div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="clear"> </div>
</div>
<div class="clear"> </div>
<script>
    function showbox(k) {
        $('#update' + k).toggleClass('no');
//        console.log($('#clicked' + k).addClass('boldall'));
        
        if($('#update' + k).hasClass('no') == false){
            $('#clicked' + k).addClass('boldall');
        }else{
            $('#clicked' + k).removeClass('boldall');
        }
        
        
    }

    $('.updateme').click(function() {
        //        $(this).toggleClass('no');
    });
</script>
<?php
include_once 'footer.php';
?>