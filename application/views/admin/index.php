<?php
include_once 'header.php';
//echo "<pre>";print_r($allusers);exit;
?>

<!-- CONTENT START -->
<div class="grid_16" id="content">
    <!--  TITLE START  --> 
    <div class="grid_9">
        <h1 class="dashboard">Infinity Dev</h1>
    </div>
    <!--RIGHT TEXT/CALENDAR-->
    <div class="grid_6" id="eventbox"><a href="#" class="inline_calendar">You don't have any events for today! Yay!</a>
        <div class="hidden_calendar"></div>
    </div>
    <!--RIGHT TEXT/CALENDAR END-->
    <div class="clear">
    </div>
    <!--  TITLE END  -->    
    <!-- #PORTLETS START -->
    <div id="portlets">

        <!--Last 30 Days Registered Users List-->
        <div class="portlet">
            <div class="portlet-header fixed"><img src="<?php echo site_url('assets/images/icons/user.gif'); ?>" width="16" height="16" alt="Latest Registered Users" /> Users Registered</div>
            <div class="portlet-content nopadding">
                <form action="" method="post">
                    <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Registered Users Sheet">
                        <thead>
                            <tr>
                                <th width="20" scope="col">#</th>
                                <th width="80" scope="col">User Key</th>
                                <th width="150" scope="col">Username</th>
                                <th width="100" scope="col">Mobile No</th>
                                <th width="130" scope="col">E-mail</th>
                                <th width="100" scope="col">Password</th>
                                <th width="130" scope="col">Join Date</th>
                                <th width="150" scope="col">Actions</th>
                                <!--<th width="90" scope="col">Actions</th>-->
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (count($lastmontregistration) > 0) {
                                for ($k = 0; $k < count($lastmontregistration); $k++) {
                                    ?>
                                    <tr>
                                        <td><?php echo $k + 1; ?></td>
                                        <td><?php echo $lastmontregistration[$k]['controller_key']; ?></td>
                                        <td><?php echo $lastmontregistration[$k]['name']; ?></td>
                                        <td><?php echo $lastmontregistration[$k]['mobile_no']; ?></td>
                                        <td><?php echo $lastmontregistration[$k]['email']; ?></td>
                                        <td><?php echo $lastmontregistration[$k]['password']; ?></td>
                                        <td><?php echo $lastmontregistration[$k]['join_date']; ?></td>
                                        <td width="90"><a href="<?php echo site_url('admin/admin/controlerdetail/' . $lastmontregistration[$k]['controller_key']); ?>">Detail</a></td>
                                        <!--<td width="90"><a href="#" class="approve_icon" title="Approve"></a> <a href="#" class="reject_icon" title="Reject"></a> <a href="#" class="edit_icon" title="Edit"></a> <a href="#" class="delete_icon" title="Delete"></a></td>-->
                                    </tr>

                                    <?php }
                                ?>
                                <tr class="footer">
                                    <td align="right">&nbsp;</td>
                                    <td colspan="7" align="right">
                                        <?php
                                        if (isset($pagelinks) && $pagelinks != '') {
                                            ?>
                                            <div class="pagination">
                                                <?php
                                                echo "<pre>";
                                                print_r($pagelinks);
                                                echo "</pre>"
                                                ?>
                                            </div>
                                        <?php } ?>
                                    </td>
                                </tr>
                                <?PHP
                            } else {
                                ?>
                                <tr>
                                    <td colspan="7">
                                        <p class="info" id="info"><span class="info_inner">No registration In Last Month</span></p>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
        <!--  End Last 30 Days Registered Users List -->
        
        
        
        
        
        <div class="portlet">
            <div class="portlet-header fixed"><img src="<?php echo site_url('assets/images/icons/user.gif'); ?>" width="16" height="16" alt="Latest Registered Users" /> Images Recovery Request</div>
            <div class="portlet-content nopadding">
                <form action="" method="post">
                    <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Registered Users Sheet">
                        <thead>
                            <tr style="border-bottom: 1px solid;">
                                <th width="20" scope="col">#</th>
                                <th width="136" scope="col">View Controller</th>
                                <th width="102" scope="col">View Target</th>
                                <th width="109" scope="col">Captured On</th>
                                <th width="129" scope="col">Pic</th>
                                <th width="100" scope="col" style="text-align: right;">Actions</th>
                                <th width="70" scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (count($visualbits) > 0) {
                                for ($k = 0; $k < count($visualbits); $k++) {
                                    ?>
                                    <tr class="<?php
                                    if ($visualbits[$k]['status'] == 1) {
                                        echo "redme";
                                    } else if ($visualbits[$k]['recoverreqest'] == 1) {
                                        echo "greenme";
                                    }
                                    ?>">
                                        <td width='20'><?php echo $k + 1; ?></td>
                                        <td>
                                            <a href="<?php echo site_url('admin/admin/controlerdetail/' . $visualbits[$k]['parent_key']); ?>"><?php echo $visualbits[$k]['parent_key']; ?></a>
                                        </td>
                                        <td>
                                            <a href="<?php echo site_url('admin/admin/targetdetail/' . $visualbits[$k]['child_key']); ?>"><?php echo $visualbits[$k]['child_key']; ?></a>
                                        </td>
                                        <td><?php echo $visualbits[$k]['time-date']; ?></td>
                                        <td>
                                            <a class="example-image-link <?php echo $visualbits[$k]['child_key']; ?>" style="margin:0 10px 5px 10px" href="<?php echo site_url('upload/' . $visualbits[$k]['child_key'] . '/' . $visualbits[$k]['pic_name']); ?>" data-lightbox="example-set" title="Visual Bits">
                                                <img class='imgthumbnail' src='<?php echo site_url('upload/' . $visualbits[$k]['child_key'] . '/' . $visualbits[$k]['pic_name']); ?>'>
                                            </a>
                                        </td>

                                        <?php
                                        if ($visualbits[$k]['recoverreqest'] == 1) {
                                            ?>
                                            <td><a href='<?php echo site_url('admin/admin/recoverthis/' . $visualbits[$k]['child_key'] . '/' . $visualbits[$k]['id']); ?>'>Recover this</a></td>
                                            <?php
                                        } else {
                                            ?>
                                            <td></td>
                                            <?php
                                        }
                                        ?>
                                        <td><a href='<?php echo site_url('admin/admin/deletethis/' . $visualbits[$k]['child_key'] . '/' . $visualbits[$k]['id'] . '/' . $visualbits[$k]['pic_name']); ?>'>Delete</a></td>
                                    </tr>
                                    <?php
                                }
                                ?>

                                <tr class="footer">
                                    <td align="right">&nbsp;</td>
                                    <td colspan="7" align="right">
                                        <?php
                                        if (isset($pagelinks) && $pagelinks != '') {
                                            ?>
                                            <div class="pagination">
                                                <?php
                                                echo "<pre>";
                                                print_r($pagelinks);
                                                echo "</pre>"
                                                ?>
                                            </div>
                                        <?php } ?>

                                    </td>
                                </tr>

                                <?php
                            } else {
                                ?>
                                <tr>
                                    <td colspan="7">
                                        <p class="info" id="error"><span class="info_inner">No Image With Recovery Request.</span></p>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        <!-- FIRST SORTABLE COLUMN START -->
        <div class="column" id="left" style="width: 100%;">
            <!--THIS IS A PORTLET-->
<!--            <div class="portlet">
                <div class="portlet-header"><img src="<?php //echo site_url('assets/images/icons/chart_bar.gif'); ?>" width="16" height="16" alt="Reports" /> Visitors - Last 30 days</div>
                <div class="portlet-content">
                    THIS IS A PLACEHOLDER FOR FLOT - Report & Graphs 
                    <div id="placeholder" style="width:auto; height:250px;"></div>
                </div>
            </div>      -->
            <!--THIS IS A PORTLET-->
<!--            <div class="portlet">
                <div class="portlet-header">Anything  (no icon too if you like it better)</div>

                <div class="portlet-content">
                    <p>This can be any content you want. I placed a basic form here with text editor so you can see the functionality of the forms too.</p>
                    <h3>This is a form</h3>
                    <form id="form1" name="form1" method="post" action="">
                        <label>Some title</label>
                        <input type="text" name="textfield" id="textfield" class="smallInput" />
                        <label>Large input box</label>
                        <input type="text" name="textfield2" id="textfield2" class="largeInput" />
                        <label>This is a textarea</label>
                        <textarea name="textarea" cols="45" rows="3" class="smallInput" id="textarea"></textarea>
                        <a class="button"><span>Submit in blue</span></a>
                        <a class="button_grey"><span>Submit this form</span></a>
                    </form>
                    <p>&nbsp;</p>
                </div>
            </div>-->
        </div>
        <!-- FIRST SORTABLE COLUMN END -->
        <!-- SECOND SORTABLE COLUMN START -->
<!--        <div class="column">
            THIS IS A PORTLET        
            <div class="portlet">
                <div class="portlet-header"><img src="<?php //echo site_url('assets/images/icons/comments.gif'); ?>" width="16" height="16" alt="Comments" />Latest Comments</div>

                <div class="portlet-content">
                    <p class="info" id="success"><span class="info_inner">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</span></p>
                    <p class="info" id="error"><span class="info_inner">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</span></p>
                    <p class="info" id="warning"><span class="info_inner">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</span></p>
                    <p class="info" id="info"><span class="info_inner">Lorem ipsum dolor sit amet, consectetuer adipiscing elit</span></p>
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit</div>
            </div>    
            THIS IS A PORTLET 
            <div class="portlet">
                <div class="portlet-header"><img src="<?php //echo site_url('assets/images/icons/feed.gif'); ?>" width="16" height="16" alt="Feeds" />Your selected News source					</div>
                <div class="portlet-content">
                    <ul class="news_items">
                        <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean  adipiscing massa quis arcu interdum scelerisque. Duis vitae nunc nisi.  Quisque eget leo a nibh gravida vulputate ut sed nulla. <a href="#">Donec quis  lectus turpis, sed mollis nibh</a>. Donec ut mi eu metus ultrices  porttitor. Phasellus nec elit in nisi</li>
                        <li>Nunc convallis, enim quis tincidunt dictum, ante ipsum  interdum massa, consequat sodales arcu magna nec eros.<a href="#"> Vivamus nec  placerat odio.</a> Sed nec mi sed orci mattis feugiat. Etiam est dui,  rutrum nec dictum vel, accumsan id sem. </li>
                        <li>Nunc convallis, enim quis tincidunt dictum, ante ipsum  interdum massa, consequat sodales arcu magna nec eros.<a href="#"> Vivamus nec  placerat odio.</a> Sed nec mi sed orci mattis feugiat. Etiam est dui,  rutrum nec dictum vel, accumsan id sem. </li>
                        <li>Nunc convallis, enim quis tincidunt dictum, ante ipsum  interdum massa, consequat sodales arcu magna nec eros.<a href="#"> Vivamus nec  placerat odio.</a> Sed nec mi sed orci mattis feugiat. Etiam est dui,  rutrum nec dictum vel, accumsan id sem. </li>
                        <li>Nunc convallis, enim quis tincidunt dictum, ante ipsum  interdum massa, consequat sodales arcu magna nec eros.<a href="#"> Vivamus nec  placerat odio.</a> Sed nec mi sed orci mattis feugiat. </li>
                    </ul>
                    <a href="#">&raquo; View all news items</a>
                </div>
            </div>                         
        </div>-->
        <!--  SECOND SORTABLE COLUMN END -->
        <div class="clear"></div>

    </div>
    <div class="clear"> </div>
    <!-- END CONTENT-->    
</div>
<div class="clear"> </div>

<!-- This contains the hidden content for modal box calls -->

































<?php
exit;
?>
<div id="bg3">
    <div id="bg4">
        <div id="bg5" style="margin-bottom: 30px;
             min-height: 500px;
             padding-bottom: 30px;">
            <div id="page">
                <table>
                    <tr>
                        <th>Key</th>
                        <th>Name</th>
                        <th>Mobile No</th>
                        <th>Email</th>
                        <th>Password</th>
                        <th>Join Date</th>
                        <th>Registered Childs</th>
                        <th>Un-Registered Childs</th>
                        <th>View</th>
                    </tr>
                    <?php
                    for ($k = 0; $k < count($allusers); $k++) {
                        ?>
                        <tr>
                            <td><?php echo $allusers[$k]['controller_key']; ?></td>
                            <td><?php echo $allusers[$k]['name']; ?></td>
                            <td><?php echo $allusers[$k]['mobile_no']; ?></td>
                            <td><?php echo $allusers[$k]['email']; ?></td>
                            <td><?php echo $allusers[$k]['password']; ?></td>
                            <td><?php echo $allusers[$k]['join_date']; ?></td>
                            <td><?php echo $allusers[$k]['category']; ?></td>
                            <td><?php echo $allusers[$k]['category']; ?></td>
                            <td><a href="<?php echo site_url('admin/admin/controlerdetail/' . $allusers[$k]['controller_key']); ?>">Detail</a></td>

                        </tr>
                        <?php
                    }
                    ?>
                </table>




                <?php
//                echo "<pre>";print_r($allusers);exit;
                ?>
            </div>
        </div>
    </div>
</div>

<?php
include_once 'footer.php';
//echo "<pre>";print_r($admin_detail);exit;
?>