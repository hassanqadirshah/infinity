<?php
include_once 'header.php';
//echo "<pre>";
//print_r($user);
//exit;
?>
<style>
    #box-table-a thead tr th{
        font-weight: bold;
        /*text-align: center;*/
    }
</style>
<div class="clear">
</div>
<!-- CONTENT START -->
<div class="grid_16" id="content">
    <!--  TITLE START  --> 
    <div class="grid_9">
        <h1 class="dashboard"><?php echo $user[0]['controller_key']; ?> Profile</h1>
    </div>

    <div class="clear">
    </div>
    <!--  TITLE END  -->    
    <!-- #PORTLETS START -->
    <div id="portlets">
        <?php $k = 0; ?>
        <!--Last 30 Days Registered Users List-->
        <div class="portlet">
            <div class="portlet-header fixed">
                <img src="<?php echo site_url('assets/images/icons/user.gif'); ?>" width="16" height="16" alt="Latest Registered Users" /> 
                <?php if($user[$k]['type']==1){
                    echo "User profile complete Detail (<span style='color:green;font-weight: bold;'> Paid User</span> )";
                }else{
                    echo "User profile complete Detail (<span style='color:Red;font-weight: bold;'> Un-Paid User</span>)";
                }
                ?>
            </div>
            <div class="portlet-content nopadding">
                <form action="javascript:;" method="post">
                    <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Registered Users Sheet">
                        <thead>
                            
                            <tr>
                                <th width="136" scope="col">User Key</th>
                                <th width="129" scope="col">E-mail</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><?php echo $user[$k]['controller_key']; ?></td>
                                <td><?php echo $user[$k]['email']; ?></td>

                            </tr>
                        </tbody>
                        <thead>
                            <tr>
                                <th width="102" scope="col">User Name</th>
                                <th width="102" scope="col">Father Name</th>

                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><?php echo $user[$k]['name']; ?></td>
                                <td><?php echo $user[$k]['father_name']; ?></td>
                            </tr>
                        </tbody>
                        <thead>
                            <tr>

                                <th width="109" scope="col">CNIC No</th>
                                <th width="109" scope="col">Mobile No</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><?php echo $user[$k]['cnic_no']; ?></td>
                                <td><?php echo $user[$k]['mobile_no']; ?></td>
                            </tr>
                        </tbody>
                        <thead>
                            <tr>
                                <th width="109" scope="col">Gender</th>
                                <th width="171" scope="col">Password</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>

                                <td><?php echo $user[$k]['gender'] == 1 ? "Male" : "Female"; ?></td>
                                <td><?php echo base64_decode($user[$k]['password']); ?></td>
                            </tr>
                        </tbody>
                        <thead>
                            <tr>
                                <th width="123" scope="col">Last Login Date</th>
                                <th width="90" scope="col">Join Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><?php echo $user[$k]['last_login']; ?></td>
                                <td><?php echo $user[$k]['join_date']; ?></td>                                        
                            </tr>
                        </tbody>
                        <thead>
                            <tr>
                                <th width="123" scope="col">Total Childs</th>
                                <th width="90" scope="col">Total Images from all Childs</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <?php $allchilds = getuserchilds($user[$k]['controller_key']); ?>
                                <td><?php echo count($allchilds); ?></td>
                                <td><?php echo count(gettotalpicsadmin($user[$k]['controller_key'])); ?></td>                                        
                            </tr>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
        <!--  End Last 30 Days Registered Users List -->
        <div class="column" id="left" style="width: 100%;">
            <!--THIS IS A PORTLET-->
            <div class="portlet">
                <div class="portlet-header">
                    <img src="<?php echo site_url('assets/images/icons/user.gif'); ?>" width="16" height="16" alt="Target List" /> List of this User Target's</div>
                <div class="portlet-content">

                    <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Registered Users Sheet">
                        <thead>
                            <tr>
                                <th width="30" scope="col">#</th>
                                <th width="200" scope="col">Target Code</th>
                                <th width="200" scope="col">Total Pictures</th>
                                <th width="500" colspan="2" style='text-align: center;' scope="col" >Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (count($allchilds) > 0) {
//                                print_r($allchilds);
                                for ($p = 0; $p < count($allchilds); $p++) {
                                    ?>
                                    <tr>
                                        <td><?php echo $p + 1; ?></td>
                                        <td><?php echo $allchilds[$p]['child_key']; ?></td>
                                        <td><?php echo count(gettotalpicsadmin($allchilds[$p]['parent_key']),$allchilds[$p]['child_key']); ?></td>
                                        <td width="300"><a href='<?php echo site_url('admin/admin/targetdetail/' . $allchilds[$p]['child_key']); ?>'>View Detail</a></td>
                                        <td><a href='<?php echo site_url('admin/admin/targetvisualbits/' . $allchilds[$p]['child_key']); ?>'>View Pictures</a></td>
                                    </tr>
                                    <?php
                                }
                            } else {
                                ?>
                                <tr>
                                    <td colspan="7">
                                        <p class="info" id="info"><span class="info_inner">Have No Targets</span></p>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                    </table>



                </div>
            </div>      
        </div>
    </div>
    <div class="clear"> </div>
</div>
<div class="clear">
</div>
<?php
include_once 'footer.php';
//echo "<pre>";print_r($admin_detail);exit;
?>
