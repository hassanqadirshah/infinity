<?php
include_once 'header.php';
//echo "<pre>";
//print_r($target);
?>
<style>
    #box-table-a thead tr th{
        font-weight: bold;
        /*text-align: center;*/
    }
</style>
<div class="clear">
</div>
<!-- CONTENT START -->
<div class="grid_16" id="content">
    <!--  TITLE START  --> 
    <div class="grid_9">
        <h1 class="dashboard"><?php echo "Target(" . $target[0]['child_key'] . ") Of Controler(" . $target[0]['parent_key'] . ")"; ?></h1>
    </div>

    <div class="clear">
    </div>
    <!--  TITLE END  -->    
    <!-- #PORTLETS START -->
    <div id="portlets">

        <!--Last 30 Days Registered Users List-->
        <div class="portlet">
            <div class="portlet-header fixed"><img src="<?php echo site_url('assets/images/icons/user.gif'); ?>" width="16" height="16" alt="Latest Registered Users" /> Target profile complete Detail</div>
            <div class="portlet-content nopadding">
                <form action="" method="post">
                    <table width="100%" cellpadding="0" cellspacing="0" id="box-table-a" summary="Registered Users Sheet">
                        <thead>
                            <?php $k = 0; ?>
                            <tr>
                                <th width="136" scope="col">Controller Key</th>
                                <th width="129" scope="col">This Target Key</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><?php echo $target[$k]['parent_key']; ?></td>
                                <td><?php echo $target[$k]['child_key']; ?></td>

                            </tr>
                        </tbody>
                        <thead>
                            <tr>
                                <th width="102" scope="col">System Name</th>
                                <th width="102" scope="col">User Name</th>

                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><?php echo $target[$k]['system_name']; ?></td>
                                <td><?php echo $target[$k]['user_name']; ?></td>
                            </tr>
                        </tbody>
                        <thead>
                            <tr>

                                <th width="109" scope="col">Ethernet Mac</th>
                                <th width="109" scope="col">Wifi Mac</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><?php echo $target[$k]['mac_e']; ?></td>
                                <td><?php echo $target[$k]['mac_w']; ?></td>
                            </tr>
                        </tbody>
                        <thead>
                            <tr>
                                <th width="109" scope="col">Os Version</th>
                                <th width="171" scope="col">Join on</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>

                                <td><?php echo $target[$k]['os_version']; ?></td>
                                <td><?php echo $target[$k]['install_datetime']; ?></td>
                            </tr>
                        </tbody>
                        <thead>
                            <tr>
                                <th width="700" scope="col" colspan="2" style='text-align: center;font-size: 20px;'>
                                    <a href='<?php echo site_url('admin/admin/controlerdetail/' . $target[$k]['parent_key']); ?>'>Back To Controler</a></th>
                                
                            </tr>
                        </thead>
                    </table>
                </form>
            </div>
        </div>
        <div class="column" id="left" style="width: 100%;"></div>
    </div>
    <div class="clear"> </div>
</div>
<div class="clear">
</div>
<?php
include_once 'footer.php';
//echo "<pre>";print_r($admin_detail);exit;
?>
