<!DOCTYPE html>
<html>
    <head>
        <title>INFINITY DEVELOPERS</title>
        <link href="<?php echo site_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo site_url('assets/css/style.css'); ?>" rel="stylesheet">
        <link href="<?php echo site_url('assets/css/signin.css'); ?>" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo site_url('assets/css/login.css'); ?>" type="text/css"/>
        <script src="<?php echo site_url('assets/js/minijs.js'); ?>"></script>
        <script src="<?php echo site_url('assets/js/bootstrap.min.js'); ?>"></script>
    </head>
    <body>
        <div class="main">
            <div class="dummy-box"></div>
            <div class="main-box-errorpage shadow">
                <div class="logo">
                    <p class="cp">
                        <a href="<?php echo base_url(); ?>"><img src="<?php echo site_url('assets/images/logo2.png'); ?>"/></a>
                    </p>
                </div>
                <div class="col-md-12">
                    <img src="<?php echo site_url('assets/images/logo/404page.png'); ?>" class="img-responsive center-block"/>
                </div>
            </div>
        </div>
    </body>
</html>

