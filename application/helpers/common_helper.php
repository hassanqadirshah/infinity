<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 4.3.2 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2009, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */
// ------------------------------------------------------------------------

/**
 * CodeIgniter Reprice Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/helpers/reprice_helper.html
 */
if (!function_exists('validateNumber')) {

    function validateNumber($cell_no) {
        $is_valid = false;
        if ($cell_no[0] == 0) {
            $new_number = substr($cell_no, 1, strlen($cell_no));
            $new_number = "92" . $new_number;
            $is_valid = true;
        } elseif ($cell_no[0] == 9 && $cell_no[1] == 2) {
            $new_number = $cell_no;
            $is_valid = true;
        } elseif ($cell_no[0] == 3) {
            $new_number = "92" . $cell_no;
            $is_valid = true;
        }
        if ($is_valid) {
            return $new_number;
        } else {
            return false;
        }
    }

}

if (!function_exists('generateRandomString')) {

    function generateRandomString($length = 6) {

        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }

}
if (!function_exists('getadmintheme')) {

    function getadmintheme($adminid) {
        $CI = get_instance();
        $CI->db->where('owner_id', $adminid);
        $kk = $CI->db->get('is_owner')->result_array();
//        echo $CI->db->last_query();print_r($kk);
        return $kk[0]['theme'];
    }

}

if (!function_exists('genraterand')) {

    function genraterand() {
        $CI = get_instance();
        $CI->load->model('customer_model', 'Customer');
        $num = date("YW") . rand(101, 999);
//     $num =201403376;   
        $data = $CI->Customer->getUserByCode($num);
        while (count($data) > 0) {
            $num = genraterand();
            $data = $CI->Customer->getUserByCode($num);
        }
        return $num;
    }

}
if (!function_exists('getuserchilds')) {

    function getuserchilds($parentid) {
        $CI = get_instance();
        //$CI->load->model('customer_model', 'Customer');
        $CI->db->where('parent_key', $parentid);
        return $CI->db->get('target')->result_array();
    }

}
if (!function_exists('getusertype')) {

    function getusertype($userkey) {
        $CI = get_instance();
        $CI->db->where('controller_key', $userkey);
        $result = $CI->db->get('is_controller')->result_array();
        if (count($result) > 0) {
            return $result[0]['type'];
        } else {
            return 0;
        }
    }

}
if (!function_exists('getoperationstatus')) {

    function getoperationstatus($childid) {
        $CI = get_instance();
        $CI->db->where('child_key', $childid);
        return $CI->db->get('is_present_monitering')->result_array();
    }

}
if (!function_exists('getalloperations')) {

    function getalloperations($parentid,$childid) {
        $CI = get_instance();
        $CI->db->where('parent_key', $parentid);
        $CI->db->where('child_key', $childid);
        $CI->db->order_by("category", "DESC");
        return $CI->db->get('is_present_monitering')->result_array();
    }

}
if (!function_exists('getavaliableoperations')) {

    function getavaliableoperations($parentid,$childid) {
        $CI = get_instance();
        $CI->db->where('parent_key', $parentid);
        $CI->db->where('child_key', $childid);
        $CI->db->where('status', 1);
        $CI->db->order_by("category", "DESC");
        return $CI->db->get('is_present_monitering')->result_array();
    }

}

if (!function_exists('checkemailid')) {

    function checkemailid($email) {
        $CI = get_instance();
        $CI->db->where('email', $email);
        $CI->db->from('is_controller');
        return $CI->db->count_all_results();
    }

}
if (!function_exists('checkavailablestatus')) {

    function checkavailablestatus($childid) {
        $CI = get_instance();
        $CI->db->where('child_key', $childid);
        $CI->db->where('child_status', 1);
        $kk = $CI->db->get('available_targets')->result_array();
        
        if(isset($kk[0]['lastupdate']))
            $lastupdate = $kk[0]['lastupdate'];
        else
            $lastupdate = date('Y-m-d H:i:s');
        
        $diff = round((strtotime(date('Y-m-d H:i:s')) - strtotime($lastupdate)) / 60);
        //echo $diff;

        if ($diff <= 5) {
            return 1;
        } else
        if ($diff <= 10) {
            return 2;
        } else
        if ($diff > 10) {
            $CI->load->model('user_model', 'User');
            $CI->User->updateavailablestatus($childid, 0);
            return 3;
        }
    }

}
if (!function_exists('getpicbyid')) {

    function getpicbyid($picid, $parentkey) {
        $CI = get_instance();
        $CI->db->where('id', $picid);
        $CI->db->where('parent_key', $parentkey);
        $CI->db->where('delete_permanent',0);
        return $CI->db->get('visualbits')->result_array();
    }

}
if (!function_exists('thumbnail')) {

    function thumbnail($image_path, $thumb_path, $image_name, $thumb_width) {
        $src_img = imagecreatefromjpeg("$image_path/$image_name");
        $origw = imagesx($src_img);
        $origh = imagesy($src_img);
        $new_w = $thumb_width;
        $diff = $origw / $new_w;
        $new_h = $new_w;
        $dst_img = imagecreate($new_w, $new_h);

        imagecopyresized($dst_img, $src_img, 0, 0, 0, 0, $new_w, $new_h, imagesx($src_img), imagesy($src_img));


        $sExtension = strtolower(end(explode('.', $image_name)));
        if ($sExtension == 'jpg' || $sExtension == 'jpeg') {

            imagejpeg($dst_img, "$thumb_path/$image_name");
        } else if ($sExtension == 'png') {

            imagepng($dst_img, "$thumb_path/$image_name");
        }



//    imagejpeg($dst_img, "$thumb_path/$image_name"); 
        RETURN TRUE;
    }

}
if (!function_exists('getlastlogindate')) {

    function getlastlogindate($controllerkey) {
        $CI = get_instance();
        $CI->db->where('controller_key', $controllerkey);
        return $CI->db->get('controller_login')->result_array();
    }

}
if (!function_exists('gettotalpics')) {

    function gettotalpics($parentkey = '', $childrkey = '') {
        if ($childrkey != '' && $parentkey != '') {
            $CI = get_instance();
            $CI->db->where('child_key', $childrkey);
            $CI->db->where('parent_key', $parentkey);
            $CI->db->where('status', 0);
            $CI->db->where('type', 0);
            $CI->db->where('delete_permanent',0);
            return $CI->db->get('visualbits')->result_array();
        } else {
            $CI = get_instance();
            $CI->db->where('status', 0);
            $CI->db->where('type', 0);
            $CI->db->where('parent_key', $parentkey);
            $CI->db->where('delete_permanent',0);
            return $CI->db->get('visualbits')->result_array();
        }
    }

}
if (!function_exists('gettotaletcfiles')) {

    function gettotaletcfiles($parentkey = '', $childrkey = '') {
        if ($childrkey != '' && $parentkey != '') {
            $CI = get_instance();
            $CI->db->where('child_key', $childrkey);
            $CI->db->where('parent_key', $parentkey);
            $CI->db->where('status', 0);
            $CI->db->where('type !=', 0);
            $CI->db->where('delete_permanent',0);
            return $CI->db->get('visualbits')->result_array();
        } else {
            $CI = get_instance();
            $CI->db->where('status', 0);
            $CI->db->where('type', 1);
            $CI->db->where('parent_key', $parentkey);
            $CI->db->where('delete_permanent',0);
            return $CI->db->get('visualbits')->result_array();
        }
    }

}
if (!function_exists('gettotalpicsadmin')) {

    function gettotalpicsadmin($parentkey = '', $childrkey = '') {
        if ($childrkey != '' && $parentkey != '') {
            $CI = get_instance();
            $CI->db->where('child_key', $childrkey);
            $CI->db->where('parent_key', $parentkey);
            $CI->db->where('delete_permanent',0);
            return $CI->db->get('visualbits')->result_array();
        } else {
            $CI = get_instance();
            $CI->db->where('parent_key', $parentkey);
            $CI->db->where('delete_permanent',0);
            return $CI->db->get('visualbits')->result_array();
        }
    }

}
if (!function_exists('getlastlocation')) {

    function getlastlocation($controllerkey, $targetkey) {
        $CI = get_instance();
        $CI->db->where('controller_key', $controllerkey);
        $CI->db->where('child_key', $targetkey);
        $CI->db->order_by("datetime", "decs");
        $CI->db->limit('10');
        return $CI->db->get('target_locatin')->result_array();
    }

}



if (!function_exists('getFilters')) {

    function getFilters($selectedMonth = 'all', $start_date = '', $end_date = '', $selected_radio = '', $method = 'POST', $action = '') {
        $CI = get_instance();
        $starting_month = '2014/01/01';
        $html = '<form method = "' . $method . '" class="helperc" action ="' . $action . '">';
        $html .= '<div class="mew"><input type="radio" id="monthlyReport" name="filter_radio" value="monthlyReport" ' . (($selected_radio == 'monthlyReport') ? 'checked' : '') . '/><label for="monthlyReport">Monthly Report</label></div>';
        $html .= '<div class="mew"><input type="radio" id="dailyReport" name="filter_radio" value="dailyReport" ' . (($selected_radio == 'dailyReport') ? 'checked' : '') . '/><label for="dailyReport">Daily Report</label></div>';
        //monthly report
        $html .= '<div id="monthly_report_container" class="sbox" style="display:' . (($selected_radio == 'monthlyReport') ? 'block;' : 'none;') . '">';
        $html .= '<select id="monthName" name="monthName">';
        $html .= '<option value="all">All</option>';
        $current_month = date("Y/m/d", strtotime("+1 month", strtotime(date("Y/m/01"))));
        $init_month = $starting_month;
        while ($init_month != $current_month) {
            $display_month = date("F Y", strtotime($init_month));
            $option_value = date("Y-m", strtotime($init_month));
            $html .= '<option value="' . $option_value . '" ' . ($selectedMonth == $option_value ? 'selected' : '') . '>' . $display_month . '</option>';
            $init_month = date("Y/m/d", strtotime("+1 month", strtotime($init_month)));
        }
        $html .= '</select>';
        $html .= '</div>';
        //daily report
        $html .= '<div id="daily_report_container" class="sbox2" style="display:' . (($selected_radio == 'dailyReport') ? 'block;' : 'none;') . '">';
        if ($start_date == '' && $end_date == '') {
            $start_date = date('Y/m/01');
            $end_date = date("Y/m/d", strtotime("-1 day", strtotime("+1 month", strtotime(date('Y/m/01')))));
        }
        $html .= '<label for="startDate">Start Date</label><input type="text" class="datePicker spinput" id="startDate" name="startDate" value="' . $start_date . '" />';
        $html .= '<label for="endDate">End Date</label><input type="text" class="datePicker spinput" id="endDate" name="endDate" value="' . $end_date . '" />';
        $html .= '</div>';
        if (($CI->uri->segment(2) !== 'dayMonthSumReport' ) && ($CI->uri->segment(2) != "logintracking" ) && ($CI->uri->segment(2) != "unauthorized" ) && ($CI->uri->segment(2) != "DirectFlyUpBenefitHistory" ) && ($CI->uri->segment(2) != "loginhistory" )) {
            $html .= '<div  class="childkey"><label for="ccode" id="ccode" >' . (($CI->uri->segment(2) == 'salesHistory') ? 'Registered By Code' : (($CI->uri->segment(2) == 'directHistory') ? 'Introducer Code' : (($CI->uri->segment(2) == 'flyUpHistory') ? 'Fly UP Benefit To Code' : (($CI->uri->segment(2) == 'auditLog') ? 'Distributor Code' : (( $CI->uri->segment(2) == 'transferHistory' ) ? 'Transfer BY Code' : (( $CI->uri->segment(2) == 'admintransferhistory' ) ? 'Transfer To Code' : (( $CI->uri->segment(2) == 'distributorReport' ) ? 'Distributor Code' : 'Code' ))))))) .
                    '</label><input type="text" name="code" id ="code" class="spinput" value="' . ($CI->input->get('code') ? $CI->input->get('code') : '') . '"></div>';
        }
        if ($CI->uri->segment(2) == 'distributorReport') {
            $html .= '<div  class="childkey"><label for="cname" id="cname" >Distributor Name</label><input type="text" name="name" id ="name" class="spinput" value="' . ($CI->input->get('name') ? $CI->input->get('name') : '') . '"></div>';
        }
        $html .= '<input type="submit" class="btn btn-primary" value ="Search">';
        $html .= '</form>';


        //script
        return $html;
    }

    if (!function_exists('convertTri')) {

        function convertTri($num, $tri) {
            $ones = array("", " one", " two", " three", " four", " five", " six", " seven", " eight", " nine", " ten", " eleven", " twelve", " thirteen", " fourteen", " fifteen", " sixteen", " seventeen", " eighteen", " nineteen");
            $tens = array("", "", " twenty", " thirty", " forty", " fifty", " sixty", " seventy", " eighty", " ninety");
            $triplets = array("", " thousand", " million", " billion", " trillion", " quadrillion", " quintillion", " sextillion", " septillion", " octillion", " nonillion");
            // chunk the number, ...rxyy
            $r = (int) ($num / 1000);
            $x = ($num / 100) % 10;
            $y = $num % 100;

            // init the output string
            $str = "";

            // do hundreds
            if ($x > 0)
                $str = $ones[$x] . " hundred";

            // do ones and tens
            if ($y < 20)
                $str .= $ones[$y];
            else
                $str .= $tens[(int) ($y / 10)] . $ones[$y % 10];

            // add triplet modifier only if there
            // is some output to be modified...
            if ($str != "")
                $str .= $triplets[$tri];

            // continue recursing?
            if ($r > 0)
                return convertTri($r, $tri + 1) . $str;
            else
                return $str;
        }

    }
    if (!function_exists('getinwords')) {

        function convertNum($num) {
            $num = (int) $num;    // make sure it's an integer

            if ($num < 0)
                return "negative" . convertTri(-$num, 0);

            if ($num == 0)
                return "zero";

            return convertTri($num, 0);
        }

    }
}




if (!function_exists('getpictursecount')) {

    function getpictursecount($controllerkey) {
        $CI = get_instance();
        $CI->db->where('parent_key', $controllerkey);
        $CI->db->where('delete_permanent',0);
        $totalpics = count($CI->db->get('visualbits')->result_array());
        if ($totalpics > 0) {
            return $totalpics;
        } else {
            return 0;
        }
    }

}
if (!function_exists('getpresentname')) {

    function getpresentname($nameid) {
        $CI = get_instance();
        $CI->db->where('id', $nameid);
        $presentdetail = $CI->db->get('ism_presents')->result_array();
        return $presentdetail;
    }

}
if (!function_exists('presentname')) {

    function presentname($present_id) {
        $CI = get_instance();
        $CI->db->where('id', $present_id);
        $kk = $CI->db->get('ism_presents')->result_array();
        if (count($kk) > 0) {
            return $kk[0]['present_name'];
        } else {
            return " ";
        }
    }

}
if (!function_exists('presentdetail')) {

    function presentdetail($present_id) {
        $CI = get_instance();
        $CI->db->where('id', $present_id);
        $kk = $CI->db->get('ism_presents')->result_array();
        if (count($kk) > 0) {
            return $kk[0]['present_description'];
        } else {
            return " ";
        }
    }

}
if (!function_exists('getcontrollertype')) {

    function getcontrollertype($controllerid) {
        $CI = get_instance();
        $CI->db->where('controller_key', $controllerid);
        $presentdetail = $CI->db->get('is_controller')->result_array();
        if(count($presentdetail) > 0)
            return $presentdetail[0]['type'];
        else
            return 0;
        
    }

}
if (!function_exists('totalpresents')) {

    function totalpresents() {
        $CI = get_instance();
        $kl = $CI->db->get('ism_presents')->result_array();
//        echo "<pre>";print_r($kl); //        echo $CI->db->last_query();
        return $kl;
    }

}
    // type 0 = unpaid, 1 = paid
    // avaliable_for 0 = ALL, 1 = paid, 2 = none
    // 0 = avaliable 1 not-avl
    if (!function_exists('levelpresents')) {

    function levelpresents($mypresents, $totalpresents, $pkey, $ckey, $ptype) {

        
        $data = array();
        if (count($mypresents) == 0 || $mypresents == '') {
            $CI = get_instance();
            for ($k = 0; $k < (count($totalpresents)); $k++) {
                $data[0]['parent_key'] = $pkey;
                $data[0]['child_key'] = $ckey;
                $data[0]['present_id'] = $totalpresents[$k]['id'];
                $data[0]['present_status'] = 0;
                $data[0]['time'] = 0;
                $data[0]['path'] = $totalpresents[$k]['path'];
                if ($totalpresents[$k]['avaliable_for'] == 1 && $ptype == 1) {
                    $data[0]['status'] = 1;
                } else if ($totalpresents[$k]['avaliable_for'] == 1 && $ptype == 0) {
                    $data[0]['status'] = 0;
                } else if ($totalpresents[$k]['avaliable_for'] == 2) {
                    $data[0]['status'] = 0;
                }
                $data[0]['created_at'] = date('Y-m-d H:i:s');
                
                $CI->db->insert('is_present_monitering', $data[0]);
                redirect(current_url());
            }
        } else {
            if (count($totalpresents) > count($mypresents)) {
                $missing_presents = array();
                foreach ($totalpresents as $data1) {
                    $duplicate = false;
                    foreach ($mypresents as $data2) {
                        if ($data1['id'] === $data2['present_id'])
                            $duplicate = true;
                    }
                    if ($duplicate === false)
                        $missing_presents[] = $data1;
                }
//                echo "<pre>";print_r($missing_presents);echo "</pre>";
                if (count($missing_presents) > 0) {
                    $CI = get_instance();
                    empty($data);
                    for ($u = 0; $u < (count($missing_presents)); $u++) {
                        $data[0]['parent_key'] = $pkey;
                        $data[0]['child_key'] = $ckey;
                        $data[0]['present_id'] = $missing_presents[$u]['id'];
                        $data[0]['present_status'] = 0;
                        $data[0]['time'] = 0;
                        $data[0]['path'] = $totalpresents[$u]['path'];
                        if ($missing_presents[$u]['avaliable_for'] == 1 && $ptype == 1) {
                            $data[0]['status'] = 1;
                        } else if ($missing_presents[$u]['avaliable_for'] == 1 && $ptype == 0) {
                            $data[0]['status'] = 0;
                        } else if ($missing_presents[$u]['avaliable_for'] == 2) {
                            $data[0]['status'] = 0;
                        }
                        $data[0]['created_at'] = date('Y-m-d H:i:s');
                        $CI->db->insert('is_present_monitering', $data[0]);                       
                    }
                    redirect(current_url());
                }
            }
            $my_update_presents = getalloperations($pkey, $ckey);
            $data2 = array();
            for ($k = 0; $k < (count($totalpresents)); $k++) {
                
                $data2[0]['path'] = $totalpresents[$k]['path'];
//                $data2[0]['present_id'] = $totalpresents[$k]['id'];
                $data2[0]['category'] = $totalpresents[$k]['category'];
                if ($totalpresents[$k]['avaliable_for'] == 1 && $ptype == 1) {
                    $data2[0]['status'] = 1;
                } else
                if ($totalpresents[$k]['avaliable_for'] == 1 && $ptype == 0) {
                    $data2[0]['status'] = 0;
                } else
                if ($totalpresents[$k]['avaliable_for'] == 2) {
                    $data2[0]['status'] = 0;
                }
                else
                if ($totalpresents[$k]['avaliable_for'] == 0) {
                    $data2[0]['status'] = 1;
                }
                //echo "<pre>";print_r($data2);
                $CI = get_instance();
                $CI->db->where('present_id', $totalpresents[$k]['id']);
                $CI->db->where('parent_key', $pkey);
                $CI->db->where('child_key', $ckey);
                $CI->db->update('is_present_monitering', $data2[0]);
                
//                echo $CI->db->last_query();
//                exit;
            }
           // redirect(current_url());
//            exit;
        }
    }

}

if (!function_exists('gettotalimages')) {

    function gettotalimages($parentid, $childid, $mace, $macw) {
        $CI = get_instance();
        //$CI->load->model('customer_model', 'Customer');
        $CI->db->where('parent_key', $parentid);
        $CI->db->where('child_key', $childid);
        $CI->db->where('status', 0);
        $CI->db->where('delete_permanent',0);
//        $CI->db->or_where('mac-w' ,$macw );

        $CI->db->from('visualbits');
        $kl = $CI->db->count_all_results();
        //$CI->db->get('images')->result_array();
//        echo $CI->db->last_query();exit;
        return $kl;
    }

}
if (!function_exists('getusername')) {

    function getusername($childid) {
        $CI = get_instance();
        $CI->db->where('child_key', $childid);
        $kk = $CI->db->get('target')->result_array();
        if (count($kk) > 0) {
            return $kk[0]['user_name'];
        } else {
            return " ";
        }
    }

}
if (!function_exists('getcontrollername')) {

    function getcontrollername($controler) {
        $CI = get_instance();
        $CI->db->where('controller_key', $controler);
        $kk = $CI->db->get('is_controller')->result_array();
        if (count($kk) > 0) {
            return $kk[0]['name'] . $kk[0]['father_name'];
        } else {
            return " ";
        }
    }

}
if (!function_exists('getaddress')) {

    function getaddress($lat,$lng) {
        $url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng=' . trim($lat) . ',' . trim($lng) . '&sensor=false';
        $json = @file_get_contents($url);
        $data = json_decode($json);
        $status = $data->status;
        if ($status == "OK")
            return $data->results[0]->formatted_address;
        else
            return false;
    }

}
if (!function_exists('getcontrolleremail')) {

    function getcontrolleremail($controler) {
        $CI = get_instance();
        $CI->db->where('controller_key', $controler);
        $kk = $CI->db->get('is_controller')->result_array();
        if (count($kk) > 0) {
            return $kk[0]['email'];
        } else {
            return " ";
        }
    }

}

if (!function_exists('update_child_presents')) {

    function update_child_presents($mypresents, $totalpresents, $pkey, $ckey,$ptype) {

        $data = array();
        if (count($mypresents) == 0 || $mypresents == '') {
            $CI = get_instance();
            for ($k = 0; $k < (count($totalpresents)); $k++) {
                $data[0]['parent_key'] = $pkey;
                $data[0]['child_key'] = $ckey;
                $data[0]['present_id'] = $totalpresents[$k]['id'];
                $data[0]['present_status'] = -1;
                $data[0]['time'] = 0;
                $data[0]['path'] = $totalpresents[$k]['path'];
                if ($totalpresents[$k]['avaliable_for'] == 1 && $ptype == 1) {
                    $data[0]['status'] = 1;
                } else if ($totalpresents[$k]['avaliable_for'] == 1 && $ptype == 0) {
                    $data[0]['status'] = 0;
                } else if ($totalpresents[$k]['avaliable_for'] == 2) {
                    $data[0]['status'] = 0;
                } else if ($totalpresents[$k]['avaliable_for'] == 0) {
                    $data[0]['status'] = 1;
                }
                $data[0]['created_at'] = date('Y-m-d H:i:s');                
                $CI->db->insert('is_present_monitering', $data[0]);
            }
        } else {
            if (count($totalpresents) > count($mypresents)) {
                $missing_presents = array();
                foreach ($totalpresents as $data1) {
                    $duplicate = false;
                    foreach ($mypresents as $data2) {
                        if ($data1['id'] === $data2['present_id'])
                            $duplicate = true;
                    }
                    if ($duplicate === false)
                        $missing_presents[] = $data1;
                }
//                echo "<pre>";print_r($missing_presents);echo "</pre>";
                if (count($missing_presents) > 0) {
                    $CI = get_instance();
                    empty($data);
                    for ($u = 0; $u < (count($missing_presents)); $u++) {
                        $data[0]['parent_key'] = $pkey;
                        $data[0]['child_key'] = $ckey;
                        $data[0]['present_id'] = $missing_presents[$u]['id'];
                        $data[0]['present_status'] = -1;
                        $data[0]['time'] = 0;
                        $data[0]['path'] = $totalpresents[$u]['path'];
                        if ($missing_presents[$u]['avaliable_for'] == 1 && $ptype == 1) {
                            $data[0]['status'] = 1;
                        } else if ($missing_presents[$u]['avaliable_for'] == 1 && $ptype == 0) {
                            $data[0]['status'] = 0;
                        } else if ($missing_presents[$u]['avaliable_for'] == 2) {
                            $data[0]['status'] = 0;
                        }else if ($missing_presents[$u]['avaliable_for'] == 0) {
                            $data[0]['status'] = 1;
                        }
                        $data[0]['created_at'] = date('Y-m-d H:i:s');
                        $CI->db->insert('is_present_monitering', $data[0]);                       
                    }
                }
            }
            $my_update_presents = getalloperations($pkey, $ckey);
            $data2 = array();
            for ($k = 0; $k < (count($totalpresents)); $k++) {
                
                $data2[0]['path'] = $totalpresents[$k]['path'];
//                $data2[0]['present_id'] = $totalpresents[$k]['id'];
                $data2[0]['category'] = $totalpresents[$k]['category'];
                if ($totalpresents[$k]['avaliable_for'] == 1 && $ptype == 1) {
                    $data2[0]['status'] = 1;
                } else
                if ($totalpresents[$k]['avaliable_for'] == 1 && $ptype == 0) {
                    $data2[0]['status'] = 0;
                } else
                if ($totalpresents[$k]['avaliable_for'] == 2) {
                    $data2[0]['status'] = 0;
                }else
                if ($totalpresents[$k]['avaliable_for'] == 0) {
                    $data2[0]['status'] = 1;
                }
                //echo "<pre>";print_r($data2);
                $CI = get_instance();
                $CI->db->where('present_id', $totalpresents[$k]['id']);
                $CI->db->where('parent_key', $pkey);
                $CI->db->where('child_key', $ckey);
                $CI->db->update('is_present_monitering', $data2[0]);
                
//                echo $CI->db->last_query();
//                exit;
            }
           // redirect(current_url());
//            exit;
        }
    }

}
