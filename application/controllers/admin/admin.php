<?php

class Admin extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('url', 'html', 'form', 'email'));
        $this->load->library('pagination');
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        $this->load->library('session'); //if it's not autoloaded in your CI setup
        $this->load->library('grocery_CRUD');
        $this->layout = 'admin_inner';
//        $this->require_admin_login();
        $this->load->model('admin_model', 'admin');
//        $this->output->enable_profiler(TRUE); 

//        $this->config->load('grocery_crud');
//        $this->config->set_item('grocery_crud_dialog_forms', true);
//        $this->config->set_item('grocery_crud_default_per_page', 10);
//        $js_files = js_files;
//        $css_files = css_files;
    }

   /* 
    function __destruct() {
        if (!empty($this->db->queries) && count($this->db->queries) > 0) {
            $times = $this->db->query_times;
            $query = $this->db->queries;
            for($log=0;$log<count($query);$log++){
                
                $data = explode(' ', $query[$log]);
                echo "<pre>";print_r($data);echo "</pre>";
//                log_message('debug', "Query Time: " . $times[$log] . " | Query: " . $query);
            }
            
            
            
            
//            foreach ($this->db->queries as $key => $query) {
//                echo "<pre>";
//                print_r($this->db->queries);
//                echo "</pre>";
////                echo "Query Time: " . $times[$key] . " | Query: " . $query;
//                log_message('debug', "Query Time: " . $times[$key] . " | Query: " . $query);
//            }
        }
    }

    */
    
    // INDEX PAGE
    public function index($page = 0) {
        if ($this->session->userdata("admin_id") != "") {
            $this->pageHeading = "HOME Page";
            $recordsperpage = 10;
            $admin_id = $this->session->userdata("admin_id");
            $result = $this->admin->getadminbyid($admin_id);
            $lastmonthusers = $this->admin->getmonthrigestered($recordsperpage, $page);

            $config['uri_segment'] = 3;
            $config['base_url'] = base_url() . 'admin/admin/index';
            $config['per_page'] = $recordsperpage;
            $config['total_rows'] = count($this->admin->totallmonthrusers());

            $this->pagination->initialize($config);
            $pagelinks = $this->pagination->create_links();
            $recoveryrequest = $this->admin->imagerecoveryrequest();
            $this->load->view('admin/index', array('admin_detail' => $result, 'pagelinks' => $pagelinks, 'lastmontregistration' => $lastmonthusers, 'visualbits' => $recoveryrequest));
        } else {
            redirect('admin/admin/login');
        }
    }
public function _example_output($output = null)
	{
		$this->load->view('admin/user',$output);
    
    //$this->load->view('example.php',$output);
	}
    
        public function user_setting() {
            if ($this->session->userdata("admin_id") == "") {
                redirect('admin/admin/login');
            }
        try {
            
            
            $crud = new grocery_CRUD();
            $crud->set_table('is_controller');
            $crud->set_subject('Controlleredit_user');
            $crud->required_fields('controller_key');
            $crud->columns('controller_key', 'name', 'email', 'cnic_no', 'mobile_no');
            
            $crud->field_type('gender','dropdown',
            array('0' => 'Male', '1' => 'Female'));
            $crud->field_type('type','dropdown',
            array('0' => 'Unpaid', '1' => 'Paid'));
            
            $crud->field_type('controller_key', 'readonly');
            
//            $crud->field_type('name', 'readonly');
//            $crud->field_type('father_name', 'readonly');
//            $crud->field_type('cnic_no', 'readonly');
//            $crud->field_type('email', 'readonly');
//            $crud->field_type('mobile_no', 'readonly');
            
            $crud->field_type('join_date', 'readonly');
            $crud->field_type('last_login', 'readonly');
            
            $crud->set_field_upload('image', 'assets/uploads/controllers');
            //$crud->set_relation('controller_key', 'target', 'parent_key');
            
             $output = $crud->render();
            
            $this->load->view('admin/user', $output);
        } catch (Exception $e) {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }

    }
    public function ism_updates() {
             if ($this->session->userdata("admin_id") == "") {
                redirect('admin/admin/login');
            }
        try {
            
// S,new_file_name as NFN,old_file_name as OFN,new_file_path as NFP,old_file_path as OFP');
            $crud = new grocery_CRUD();
            $crud->set_table('is_update');
            $crud->set_subject('Versions Updates');
            $crud->required_fields('name','modifecation_date','updatebefore_date','status','old_file_name', 'new_file_path', 'old_file_path');
            $crud->columns('name','modifecation_date','updatebefore_date','status','new_file_name', 'old_file_name', 'new_file_path', 'old_file_path');
            
            $crud->display_as('modifecation_date', 'Modifecation Date');
            $crud->display_as('updatebefore_date', 'Updatebefore Date');
            $crud->display_as('new_file_name', 'New File Name');
            $crud->display_as('old_file_name', 'Old File Name');
            $crud->display_as('new_file_path', 'New File Path');
            $crud->display_as('old_file_path', 'Old File Path');
            
            $output = $crud->render();
            
            $this->load->view('admin/user', $output);
        } catch (Exception $e) {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }

    }
        public function admin_setting() {
             if ($this->session->userdata("admin_id") == "") {
                redirect('admin/admin/login');
            }
        try {
            
            
            $crud = new grocery_CRUD();
            $crud->set_table('is_owner');
            $crud->set_subject('Admin');
            $crud->required_fields('controller_key');
            $crud->columns('owner_id', 'name', 'login_name', 'email','theme','mobile');
           
            $crud->field_type('theme','dropdown',
            array('1' => 'BLUE', '2' => 'Green', '3' => 'Red'));
            
            
            $crud->display_as('owner_id', 'Sceure-ID');
            $crud->display_as('name', 'Admin Name');
            $crud->display_as('login_name', 'Login Name');
            $crud->display_as('email', 'Email ID');
            $crud->display_as('mobile', 'Mobile #');
            
            $crud->field_type('time-date', 'hidden');
            
            
            if($this->session->userdata("admin_id") != 93040){
            
            $crud->where('owner_id',$this->session->userdata("admin_id"));
            $crud->unset_delete();
//            $crud->unset_edit();
            $crud->unset_read();
            $crud->unset_delete();
            }
            
            
            
            
            
            $crud->field_type('added_by', 'hidden', $this->session->userdata("admin_id"));
//             $crud->unset_add();
            
            
             $output = $crud->render();
            
            $this->load->view('admin/user', $output);
        } catch (Exception $e) {
            show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
        }

    }

    public function users($page = 0) {
        if ($this->session->userdata("admin_id") != "") {
            $this->pageHeading = "All Users Page";
            $recordsperpage = 10;
            $admin_id = $this->session->userdata("admin_id");
            $result = $this->admin->getadminbyid($admin_id);
            $allusers = $this->admin->getallusers($recordsperpage, $page);

            $config['uri_segment'] = 3;
            $config['base_url'] = base_url() . 'admin/admin/users';
            $config['per_page'] = $recordsperpage;
            $config['total_rows'] = count($this->admin->totalusers());

            $this->pagination->initialize($config);
            $pagelinks = $this->pagination->create_links();

            $this->load->view('admin/users', array('admin_detail' => $result, 'allusers' => $allusers, 'pagelinks' => $pagelinks));
        } else {
            redirect('admin/admin/login');
        }
    }

    
    public function edituser() {
        if ($this->session->userdata("admin_id") == "") {
            redirect('admin/admin/login');
        }
        try {

// S,new_file_name as NFN,old_file_name as OFN,new_file_path as NFP,old_file_path as OFP');
            $crud = new grocery_CRUD();
            $crud->set_table('is_controller');
            $crud->set_subject('Edit User');

            $crud->columns('controller_key', 'name', 'father_name', 'cnic_no', 'mobile_no', 'gender', 'email', 'email_varification_status', 'password', 'picture', 'type');

            $crud->field_type('type', 'dropdown', array('-1' => 'Disabled', '0' => 'Un-paid', '1' => 'Paid'));
            $crud->field_type('gender', 'dropdown', array('0' => 'Female', '1' => 'Male'));

            $crud->display_as('name', 'Controller Name');
            $crud->display_as('email', 'Email ID');
            $crud->display_as('mobile_no', 'Mobile #');

            $crud->field_type('controller_key', 'readonly');
            $crud->field_type('cnic_no', 'readonly');
            $crud->field_type('mobile_no', 'readonly');
            $crud->field_type('email', 'readonly');
            $crud->field_type('controller_key', 'readonly');

            $crud->field_type('varified_on', 'hidden');
            $crud->field_type('join_date', 'hidden');
            $crud->field_type('last_login', 'hidden');
            $crud->set_field_upload('picture', 'assets/images/user');
            $crud->callback_edit_field('password', array($this, 'decodepassword'));
            $crud->callback_before_update(array($this, 'encodepassword'));
            $crud->unset_add();
            $crud->unset_delete();
            $crud->unset_list();
//            $crud->unset_back_to_list();
//            $crud->set_crud_url_path(site_url('admin/admin/users'));
            $output = $crud->render();

            $this->load->view('admin/user', $output);
        } catch (Exception $e) {
            if ($e->getCode() == 14) { //The 14 is the code of the "You don't have permissions" error on grocery CRUD.
                redirect('admin/admin/users');//This is a custom view that you have to create
                //Or you can simply have an error message for this
                //For example: show_error('You don\'t have permissions for this operation');
            } else {
                show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
            }
        }
    }

    function encodepassword($post_array, $primary_key = null) {

        $post_array['password'] = base64_encode($post_array['password']);
        return $post_array;
    }

    function decodepassword($value, $primary_key) {
        return '<input type="text" maxlength="50" value="' . base64_decode($value) . '" name="password" style="width:462px">';
    }

    public function allvisualbits($page = 0) {
        $this->pageHeading = "All Visual Bits";
        $recordsperpage = 20;

        if ($this->session->userdata("admin_id") != "") {
            $allvisualbits = $this->admin->getallvisualbits($recordsperpage, $page);
            $config['uri_segment'] = 3;
            $config['base_url'] = base_url() . 'admin/admin/allvisualbits';
            $config['per_page'] = $recordsperpage;
            $config['total_rows'] = $this->admin->totalvisualbits();

            $this->pagination->initialize($config);
            $pagelinks = $this->pagination->create_links();

            $this->load->view('admin/allvisualbits', array('visualbits' => $allvisualbits, 'pagelinks' => $pagelinks));
        } else {
            redirect('admin/admin/login');
        }
    }

    // controlerdetail PAGE
    public function controlerdetail($controlerid) {
        $this->pageHeading = "Controler Detail Page";

        if ($this->session->userdata("admin_id") != "") {
            $user = $this->admin->getcontrolerbyid($controlerid);
            if (count($user) > 0) {
                $this->load->view('admin/controlerdetail', array('user' => $user));
            } else {
                redirect('admin/admin/index');
            }
        } else {
            redirect('admin/admin/login');
        }
    }

    public function operations() {
        $this->pageHeading = "Add, Edit And Update Operations";

        if ($this->session->userdata("admin_id") != "") {
            try {
                $crud = new grocery_CRUD();
                $crud->set_table('ism_presents');
                $crud->set_subject('Presents');
                $crud->required_fields('present_name','present_process_name', 'present_description', 'avaliable_for');
                $crud->columns('present_name', 'present_process_name', 'present_description', 'avaliable_for','category');
                
                $crud->display_as('present_name', 'Present Name');
                $crud->display_as('present_process_name', 'Process Name');
                $crud->display_as('present_description', 'Description');
                $crud->display_as('avaliable_for', 'Avaliable For');
                
                $crud->field_type('category','dropdown',
            array('0' => 'other', '1' => 'ISM','2' => 'bandwidth' , '3' => 'browsers', '4' => 'Quick Operations', '5' => 'Instant Operations'));
                $crud->field_type('timedate', 'hidden', date('Y-m-d H:i:s'));
                
                $crud->field_type('avaliable_for','dropdown',
            array('0' => 'All', '1' => 'Paid Members','2' => 'None'));
                
                
//                $output = $this->grocery_crud->render();
                $output = $crud->render();
               $this->load->view('admin/operations', $output);
            } catch (Exception $e) {
                show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
            }
            
        } else {
            redirect('admin/admin/login');
        }
    }
    public function logs() {
        $this->pageHeading = "User Logs";

        if ($this->session->userdata("admin_id") != "") {
            try {
                $crud = new grocery_CRUD();
                $crud->set_table('logs');
                $crud->set_subject('User Logs');
                
                $crud->columns('controller_key', 'type', 'table', 'query','created_at');
                
                $crud->display_as('controller_key', 'Controller');
                $crud->display_as('type', 'Event');
                $crud->display_as('Table Used', 'table');
                $crud->display_as('query', 'Detail in Query');
                $crud->display_as('created_at', 'Date & time');
                
//                $crud->set_relation('controller_key', 'is_controller', 'name');
                
                
                $crud->unset_add();
                $crud->unset_edit();
                $crud->unset_delete();


                $output = $crud->render();
               $this->load->view('admin/operations', $output);
            } catch (Exception $e) {
                show_error($e->getMessage() . ' --- ' . $e->getTraceAsString());
            }
            
        } else {
            redirect('admin/admin/login');
        }
    }

    public function operations_old() {
        $this->pageHeading = "Add, Edit And Update Operations";

        if ($this->session->userdata("admin_id") != "") {
            $operations = $this->admin->getalloperations();
            $this->load->view('admin/operations', array('operations' => $operations));
        } else {
            redirect('admin/admin/login');
        }
    }

    public function updateoperation() {
        $this->pageHeading = "Update Operations";
        if ($this->session->userdata("admin_id") != "") {
            if ($this->input->post()) {
                $data_td['present_name'] = $this->input->post('present_name');
                $data_td['present_process_name'] = $this->input->post('process_name');
                $data_td['present_description'] = $this->input->post('process_description');
                $data_td['avaliable_for'] = $this->input->post('av_for');
                $presentid = $this->input->post('process_id');
                $this->admin->updateopbyid($presentid, $data_td);
            }
            $operations = $this->admin->getalloperations();
            $this->load->view('admin/operations', array('operations' => $operations));
        } else {
            redirect('admin/admin/login');
        }
    }

    public function addoperation() {
        $this->pageHeading = "ADD Operations";
        if ($this->session->userdata("admin_id") != "") {
            if ($this->input->post()) {
                $data['present_name'] = $this->input->post('present_name');
                $data['present_process_name'] = $this->input->post('process_name');
                $data['present_description'] = $this->input->post('process_description');
                $data['avaliable_for'] = $this->input->post('av_for');
                $data['timedate'] = date('Y-m-d H:i:s');
                $this->admin->addnewoperation($data);
            }
            $operations = $this->admin->getalloperations();
            $this->load->view('admin/operations', array('operations' => $operations));
        } else {
            redirect('admin/admin/login');
        }
    }

    public function targetdetail($targetid) {
        $this->pageHeading = "Target Detail Page";

        if ($this->session->userdata("admin_id") != "") {
            $target = $this->admin->gettargetbyid($targetid);
            if (count($target) > 0) {
                $this->load->view('admin/targetdetail', array('target' => $target));
            } else {
                redirect('admin/admin/index');
            }
        } else {
            redirect('admin/admin/login');
        }
    }

    public function recoverthis($targetid, $imageid) {
        $this->pageHeading = "Target Detail Page";

        if ($this->session->userdata("admin_id") != "") {
            $target = $this->admin->gettargetbyid($targetid);
            if (count($target) > 0) {
                $controler_key = $this->admin->getcontrolerbytargetid($targetid);
                $request_on = $this->admin->requesttorecoverdate($targetid, $imageid);
                $controler_email = $this->admin->getcontroleremail($controler_key);
                $controler_name = $this->admin->getcontrolername($controler_key);
//                $controler_email = "saeed.hassan@purelogics.net";
                $this->admin->recoverimage($targetid, $controler_key, $imageid);

                template_email_send('revoverimage', array("email" => $controler_email, "controler_name" => $controler_name, "request_on" => $request_on));
                redirect('admin/admin/targetvisualbits/' . $targetid);
            } else {
                redirect('admin/admin/index');
            }
        } else {
            redirect('admin/admin/login');
        }
    }

    public function deletethis($targetid, $imageid, $picname) {
        $this->pageHeading = "Target Detail Page";

        if ($this->session->userdata("admin_id") != "") {
            $target = $this->admin->gettargetbyid($targetid);
            if (count($target) > 0) {
                $this->admin->deleteimage($targetid, $imageid);
                $path = site_url('upload/' . $targetid);
                unlink($path . "/" . $picname);
//                unlink($myFile);
//                rmdir($myFile);
                redirect('admin/admin/targetvisualbits/' . $targetid);
            } else {
                redirect('admin/admin/index');
            }
        } else {
            redirect('admin/admin/login');
        }
    }

    public function targetvisualbits($targetid) {
        $this->pageHeading = "Target All Visual Bits";

        if ($this->session->userdata("admin_id") != "") {
            $visualbits = $this->admin->gettargetvisualbits($targetid);
            if (count($visualbits) > 0) {
                $this->load->view('admin/targetvisualbits', array('visualbits' => $visualbits));
            } else {
                redirect('admin/admin/index');
            }
        } else {
            redirect('admin/admin/login');
        }
    }

    public function emailinbox() {
        $this->pageHeading = "Email Inbox";

        if ($this->session->userdata("admin_id") != "") {
            $kk =  $this->updateemails();
//            echo "<pre>";print_r($kk);echo "</pre>";
//            echo count($kk);
//            echo $kk->fullcount;
//            exit;
            $this->load->view('admin/emailinbox',array('emails' => $kk));
        } else {
            redirect('admin/admin/login');
        }
    }
    
    
    public function updateemails() {
        $username = 'infinity.developers.team@gmail.com';
        $password = '123mhassh';
        $kk = $this->check_email($username, $password);
//        print_r($kk);
//        exit;
        
        
        if (isset($kk) && $kk != '' && count($kk) > 0) {
            $x = new SimpleXmlElement($kk);
//            echo "<pre>";print_r($x);echo "</pre>";exit;
            $json = json_encode($x);
            $array = json_decode($json,TRUE);
            return $array;
        }else{
            return '';
        }
    }

    public function login($msg = "") {
        if ($this->session->userdata("admin_id") != "") {
            redirect('admin/admin/index');
        } else
        if ($msg != "" && $msg == 1) {
            $data['msg'] = "User Name and Password Not Match";
            $this->load->view('admin/login', $data);
        } else
        if ($msg != "" && $msg == 2) {
            $data['msg'] = "PLEASE LOGIN FIRST";
            $this->load->view('admin/login', $data);
        } else
        if ($this->input->post()) {
            $user_name = $this->input->post('name');
            $password = $this->input->post('password');
            $result = $this->admin->admininfo($user_name, $password);
            $this->session->set_userdata('admin_id', $result[0]['owner_id']);
//            echo "<pre>";print_r($result);exit;
            redirect('admin/admin/index');
        } else {
            $this->load->view('admin/login');
        }
    }
    
    public function check_email($username, $password) {
        //Connect Gmail feed atom
        $url = "https://mail.google.com/mail/feed/atom";

        // Send Request to read email 
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_USERPWD, $username . ":" . $password);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_ENCODING, "");
        $curlData = curl_exec($curl);
        curl_close($curl);

        //returning retrieved feed
        return $curlData;
    }

    public function signout() {
        $this->session->sess_destroy();
        redirect('admin/admin/login');
    }

}

/* End of file welcome.php */
/* Location: ./system/application/controllers/welcome.php */