<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {
    
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
    
    public function __construct() {
        parent::__construct();
        //echo "THERE";exit;
        $this->load->helper(array('url', 'html', 'form', 'email'));
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        date_default_timezone_set('Asia/Karachi');
        $this->load->model('user_model', 'user');
    }
    
    
	public function index()
	{   
                if ($this->input->post()) {
//                    echo "<pre>";print_r($_POST);echo "</pre>";
                    $data['name'] = $this->input->post('name');
                    $data['sender'] = $this->input->post('email');
                    $data['message'] = $this->input->post('message');
                    $data['email'] = "infinity.developers.team@gmail.com";
                    template_email_send('mailtous', $data);
//                    exit;
                }
            
		$this->load->view('welcome_page');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */