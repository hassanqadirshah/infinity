<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Admin_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        date_default_timezone_set('Asia/Karachi');
    }

    public function admininfo($admin_name, $admin_key) {
        $this->db->where(array('login_name' => $admin_name, 'key' => $admin_key));
        return $this->db->get('is_owner')->result_array();
//         return $this->db->count_all_results();
//        return $this->db->get('type_statment')->row()->statment;
    }

    public function getadminbyid($admin_name) {
        $this->db->where(array('owner_id' => $admin_name));
        return $this->db->get('is_owner')->result_array();
    }

    public function totalusers() {
        return $this->db->get('is_controller')->result_array();
    }

    public function totalvisualbits() {
        $this->db->where('delete_permanent', 0);
//         $this->db->get('visualbits')->count_all_results();
        return $this->db->count_all_results('visualbits');
    }

    public function getallusers($limit, $start) {
        $this->db->limit($limit, $start);
        $this->db->order_by("id", "asc");
        $jk = $this->db->get('is_controller')->result_array();
//        echo $this->db->last_query();
        return $jk;
    }

    public function getallvisualbits($limit, $start) {
        $this->db->where('delete_permanent', 0);
        $this->db->limit($limit, $start);
        $this->db->order_by("id", "asc");
        return $this->db->get('visualbits')->result_array();
    }
    
    public function imagerecoveryrequest() {
        $this->db->where('delete_permanent', 0);
        $this->db->where('recoverreqest', 1);
        $this->db->order_by("id", "asc");
        return $this->db->get('visualbits')->result_array();
    }

    public function totallmonthrusers() {
        $lastteoweeks = date('Y-m-d H:i:s', strtotime('today - 31 days'));
        $this->db->where('join_date >', $lastteoweeks);
        return $this->db->get('is_controller')->result_array();
    }

    public function getmonthrigestered($limit, $start) {
        $lastteoweeks = date('Y-m-d H:i:s', strtotime('today - 365 days'));
        $this->db->limit($limit, $start);
        $this->db->where('join_date >', $lastteoweeks);
        return $this->db->get('is_controller')->result_array();
        //echo $this->db->last_query();
    }

    public function getcontrolerbyid($controlerkey) {
        $this->db->where(array('controller_key' => $controlerkey));
        return $this->db->get('is_controller')->result_array();
    }

    public function getalloperations() {
        $this->db->order_by("timedate", "desc");
        return $this->db->get('ism_presents')->result_array();
    }

    public function updateopbyid($presentid, $data_td) {
        $this->db->where(array('id' => $presentid));
        $this->db->update('ism_presents', $data_td);
    }

    public function addnewoperation($data) {
        $this->db->insert('ism_presents', $data);
    }

    public function getoperationbyid($opid) {
        $this->db->where(array('id' => $opid));
        return $this->db->get('ism_presents')->result_array();
    }

    public function getcontrolerbytargetid($targetkey) {
        $this->db->where(array('child_key' => $targetkey));
        return $this->db->get('parent_childs')->row()->parent_key;
//        return $this->db->get('is_controller')->result_array();
    }

    public function getcontrolername($controlerkey) {
        $this->db->where(array('controller_key' => $controlerkey));
        return $this->db->get('is_controller')->row()->name;
//        return $this->db->get('is_controller')->result_array();
    }

    public function getcontroleremail($controlerkey) {
        $this->db->where(array('controller_key' => $controlerkey));
        return $this->db->get('is_controller')->row()->email;
//        return $this->db->get('is_controller')->result_array();
    }

    public function gettargetbyid($targetkey) {
        $this->db->where(array('child_key' => $targetkey));
        return $this->db->get('target')->result_array();
    }

    public function gettargetvisualbits($targetkey) {
        $this->db->where(array('child_key' => $targetkey));
        $this->db->where('delete_permanent', 0);
        return $this->db->get('visualbits')->result_array();
    }

    public function recoverimage($targetkey, $controler_key, $imgid) {
        $this->db->where('id', $imgid);
        $this->db->where('child_key', $targetkey);
        $this->db->where('parent_key', $controler_key);
        $data['status'] = 0;
        $data['recoverreqest'] = 0;
        $data['deleted_on'] = date('Y-m-d H:i:s');
        $data['request_on'] = date('Y-m-d H:i:s');
        $this->db->update('visualbits', $data);
//        echo $this->db->last_query();
//        exit;
    }

    public function requesttorecoverdate($targetkey, $imgid) {
        $this->db->where('id', $imgid);
        $this->db->where('child_key', $targetkey);
        return $this->db->get('visualbits')->row()->request_on;
//        return $this->db->get('visualbits')->result_array();
    }

    public function deleteimage($targetkey, $imgid) {
        $this->db->where('id', $imgid);
        $this->db->where('child_key', $targetkey);

        $data['delete_permanent'] = 1;
        $data['deleted_on'] = date('Y-m-d H:i:s');
        $this->db->update('visualbits', $data);
//        echo $this->db->last_query();
//        exit;
    }

}
