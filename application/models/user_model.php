<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class User_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
        date_default_timezone_set('Asia/Karachi');
    }

    public function createNewuser($table, $data) {
        return $this->db->insert($table, $data);
    }

    public function createnewchild($table, $data) {
        return $this->db->insert($table, $data);
    }

    public function addnewchildstatus($data) {
        return $this->db->insert('available_targets', $data);
    }

    public function addnewchildpresents($data) {
        return $this->db->insert('is_present_monitering', $data);
    }

    public function loginInfo($user_email, $password) {
//        $this->db->where(array('email' => $user_email, 'password' => $password));
        $this->db->where('email', $user_email);
        $this->db->where('password', $password);
//        $this->db->where('email_varification_status', 1);
        return $this->db->get('is_controller')->result_array();
    }

    public function getUserTable() {
//        $this->db->where('id',$user_id);
        return $this->db->get('is_controller')->result_array();
    }

    public function gethistory($user_id) {
        $this->db->where('controller_key', $user_id);
        $this->db->order_by("date_time", "desc");
        $this->db->limit(20);
        return $this->db->get('controller_login')->result_array();
    }

    public function getUserById($user_id) {
        $this->db->where('id', $user_id);
        return $this->db->get('is_controller')->result_array();
    }
    public function getUserByckey($ckey) {
        $this->db->where('controller_key', $ckey);
        return $this->db->get('is_controller')->result_array();
    }

    public function getchildById($child_id) {
        $this->db->where('child_key', $child_id);
        $result = $this->db->get('target')->result_array();
        if (isset($result) && count($result) > 0) {
            return $result;
        } else {
            return 'empty';
        }
    }
    public function getparentchild($pid,$child_id) {
        $this->db->where('parent_key', $pid);
        $this->db->where('child_key', $child_id);
        $result = $this->db->get('target')->result_array();
        if (isset($result) && count($result) > 0) {
            return $result;
        } else {
            return 'empty';
        }
    }
    public function getchildbymace($mace) {
        $this->db->where('mac_e', $mace);
        $result = $this->db->get('target')->result_array();
        if (isset($result) && count($result) > 0) {
            return $result;
        } else {
            return 'empty';
        }
    }
    public function getchildbymacw($macw) {
        $this->db->where('mac_w', $macw);
        $result = $this->db->get('target')->result_array();
        if (isset($result) && count($result) > 0) {
            return $result;
        } else {
            return 'empty';
        }
    }
    public function getchildbykeymacemacw($userkey,$mace,$macw) {
        $this->db->where('child_key', $userkey);
        $this->db->where('mac_e', $mace);
        $this->db->or_where('mac_w', $macw);
        $result = $this->db->get('target')->result_array();
//        echo $this->db->last_query();
        if (isset($result) && count($result) > 0) {
            return $result;
        } else {
            return 'empty';
        }
    }

    public function updatetarget($pkey,$ckey, $data) {
        $this->db->where('parent_key', $pkey);
        $this->db->where('child_key', $ckey);
        $this->db->update('target', $data);
    }
    
    public function findemilid($emial_id) {
        $this->db->where('email', $emial_id);
        return $this->db->get('is_controller')->result_array();
//        echo $this->db->last_query();exit;
    }
    public function findemail($emial_id) {
        $this->db->where('email', $emial_id);
        return $this->db->get('is_controller')->result_array();
//        echo $this->db->last_query();exit;
    }

    public function getchildBykey($key) {
        $this->db->where('parent_key', $key);
        return $this->db->get('target')->result_array();
    }

    public function getcchildBykey($key) {
        $this->db->where('parent_key', $key);
        return $this->db->get('parent_childs')->result_array();
    }

    public function getparentbychildkey($key) {
        $this->db->where('child_key', $key);
        $kk = $this->db->get('parent_childs')->result_array();
        if(isset($kk) && count($kk) > 0){
            return $kk[0]['parent_key'];
        }else{
            return 'empty';
        }
    }

    public function getimagenameskey_count($pkey, $tkey = '') {
        $this->db->where('parent_key', $pkey);
        $this->db->where('status', 0);
        $this->db->where('type', 0);
        $this->db->where('delete_permanent',0);
        if (isset($tkey) && $tkey != '') {
            $this->db->where('child_key', $tkey);
        }
        $this->db->order_by("time-date","desc");
        return count($this->db->get('visualbits')->result_array());
    }
    
    public function getimagenameskey($pkey, $tkey = '',$trecords,$rrecords) {
        $this->db->where('parent_key', $pkey);
        $this->db->where('status', 0);
        $this->db->where('type', 0);
        $this->db->where('delete_permanent',0);
        if (isset($tkey) && $tkey != '') {
            $this->db->where('child_key', $tkey);
        }
        $this->db->limit($trecords, $rrecords);
        $this->db->order_by("time-date","desc");
        return $this->db->get('visualbits')->result_array();
    }
    
    public function getfilesnameskey_count($pkey, $tkey = '') {
        $this->db->where('parent_key', $pkey);
        $this->db->where('status', 0);
        $this->db->where('type != ', 0); 
        $this->db->where('delete_permanent',0);
        if (isset($tkey) && $tkey != '') {
            $this->db->where('child_key', $tkey);
        }
        $this->db->order_by("time-date","desc");
        return count($this->db->get('visualbits')->result_array());
    }
    
    public function getfilesnameskey($pkey, $tkey = '',$trecords,$rrecords) {
        $this->db->where('parent_key', $pkey);
        $this->db->where('status', 0);
        $this->db->where('type != ', 0);
        $this->db->where('delete_permanent',0);
        if (isset($tkey) && $tkey != '') {
            $this->db->where('child_key', $tkey);
        }
        $this->db->limit($trecords, $rrecords);
        $this->db->order_by("time-date","desc");
        return $this->db->get('visualbits')->result_array();
    }

    public function updatestatus($prnt_key, $pid) {
        $this->db->where('id', $pid);
        $this->db->where('parent_key', $prnt_key);
        $data['status'] = 1;
        $data['deleted_on'] = date('Y-m-d H:i:s');
        $this->db->update('visualbits', $data);
    }
    public function update_email_varirification($user_id) {
        $this->db->where('id', $user_id);
        $data['email_varification_status'] = 1;
        $data['varified_on'] = date('Y-m-d H:i:s');
        $this->db->update('is_controller', $data);
    }

    public function recoverrequest($picid, $ckey, $pname) {

        $laststatus = $this->lastrecoverstatus($picid, $ckey, $pname);


        $this->db->where('id', $picid);
        $this->db->where('child_key', $ckey);
        $this->db->where('pic_name', $pname);
        $this->db->where('status', 1);
        if ($laststatus == true) {
            $data['recoverreqest'] = 0;
        } else {
            $data['recoverreqest'] = 1;
        }
        $data['request_on'] =  date('Y-m-d H:i:s');;
        $this->db->update('visualbits', $data);
//        echo $this->db->last_query();
//        exit;
    }

    public function lastrecoverstatus($picid, $ckey, $pname) {
        $this->db->where('id', $picid);
        $this->db->where('child_key', $ckey);
        $this->db->where('pic_name', $pname);
        $this->db->where('status', 1);
        $this->db->where('recoverreqest', 1);
        $this->db->where('delete_permanent',0);
        $jk = $this->db->get('visualbits')->result_array();
        if (count($jk) > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function updateParentById($user_id, $data) {
        $this->db->where('id', $user_id);
        $this->db->update('users', $data);
    }

    public function changepassword($id, $password, $data, $table) {
        $this->db->where(array('id' => $id, 'password' => $password));
        $this->db->update($table, $data);
    }

    public function updateavailablestatus($ckey, $stat) {

        $data['child_status'] = $stat;
        $data['lastupdate'] = date('Y-m-d H:i:s');

        $this->db->where(array('child_key' => $ckey));
        $this->db->update('available_targets', $data);
    }

    public function getopestatus($cid) {
//        $this->db->select('present_id','present_status','path');
//        $this->db->where(array('child_key' => $cid));
//        return $this->db->get('is_present_monitering2')->result_array();
//        
        
        
        $this->db->select('present_id, present_status, path');
        $this->db->from('is_present_monitering');
        $this->db->where(array('child_key' => $cid));
         $this->db->where('status', 1);
        return $this->db->get()->result();
    }
    public function getopestatusfirst($cid) {
        $this->db->select('present_id, present_status, path,category,updated_at');
        $this->db->from('is_present_monitering');
        $this->db->where(array('child_key' => $cid));
        $this->db->where('status', 1);
        return $this->db->get()->result_array();
    }
    public function get_ckey_by_macid($mac_id) {
        $this->db->where('mac_e', $mac_id);
        $this->db->or_where('mac_w', $mac_id); 
        $result = $this->db->get('target');
        if ($result->num_rows() > 0)
            return $result->result_array();
        else
            return 'empty';
    }
    public function get_ism_update() {
        $this->db->select('modifecation_date as MD, updatebefore_date as UBD, status as S,new_file_name as NFN,old_file_name as OFN,new_file_path as NFP,old_file_path as OFP');
        $this->db->where('status', 1);
        $result = $this->db->get('is_update');
        if ($result->num_rows() > 0)
            return $result->result_array();
        else
            return 'empty';
    }

//    $this->session->userdata("user_key"),$gcid, $present_id,$data
    public function updatechildnewope($pid,$childid,$present_id, $data) {
        $this->db->where('parent_key', $pid);
        $this->db->where('child_key', $childid);
        $this->db->where('present_id', $present_id);
        $this->db->update('is_present_monitering', $data);
//        echo $this->db->last_query();exit;
    }

    public function updatetrackdata($data) {
        $this->db->insert('ism_trackdata', $data);
    }

    public function getpresentstatus($ckey) {
        $this->db->where(array('child_key' => $ckey));
        return $this->db->get('is_present_monitering')->row()->operations;
    }
    public function get_op_last_status($pkey,$ckey,$present_id) {
        $this->db->where('parent_key' , $pkey);
        $this->db->where('child_key' , $ckey);
        $this->db->where('present_id' , $present_id);
        $result = $this->db->get('is_present_monitering')->result_array();
//        echo $this->db->last_query();
//        print_r($result);exit;
        if (count($result) > 0) {
            return $result[0]['present_status'];
        } else {
            return '';
        }
    }

    public function image_recover($ckey) {
        $this->db->where(array('parent_key' => $ckey));
        $this->db->where('status', 1);
        $this->db->where('delete_permanent',0);
        $jk = $this->db->get('visualbits')->result_array();
//        echo $this->db->last_query();
        //exit;
        return $jk;
    }

    public function updatelastlogin($data) {
        $this->db->insert('controller_login', $data);
    }

    public function getParentByUserId($user_id) {
        $this->db->where('id', $user_id);
        return $this->db->get('is_controller')->result_array();
    }

    public function genraterand() {
        $num = date("YW") . rand(1001, 9999);
        $data = $this->user->getUserById($num);
        if (count($data) > 0) {
            $this->genraterand();
        }
        return $num;
    }
    
    public function genraterand_abc($length,$key) {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        $newstr = '';
        for ($r = 0; $r < 6; $r++) {
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, strlen($characters) - 1)];
            }
//            echo $randomString;echo "-";
            if ($r == 0) {
                $randomString = substr_replace($randomString, $key[2], rand(0, 2), 1);
                $randomString = substr_replace($randomString, $key[3], rand(3, 4), 1);
                $newstr .= $randomString."-";
                $randomString = '';
            }
            if ($r == 1) {
                $randomString = substr_replace($randomString, $key[4], rand(0, 4), 1);
                $newstr .= $randomString."-";
                $randomString = '';
            }
            if ($r == 2) {
                $randomString = substr_replace($randomString, $key[8], rand(0, 1), 1);
                $randomString = substr_replace($randomString, $key[9], rand(3, 4), 1);
                $newstr .= $randomString."-";
                $randomString = '';
            }
            if ($r == 3) {
                $randomString = substr_replace($randomString, $key[5], rand(0, 4), 1);
                $newstr .= $randomString."-";
                $randomString = '';
            }
            if ($r == 4) {
                $randomString = substr_replace($randomString, $key[0], rand(0, 2), 1);
                $randomString = substr_replace($randomString, $key[1], rand(3, 4), 1);
                $newstr .= $randomString."-";
                $randomString = '';
            }
            if ($r == 5) {
                $randomString = substr_replace($randomString, $key[6], rand(0, 1), 1);
                $randomString = substr_replace($randomString, $key[7], rand(2, 4), 1);
                $newstr .= $randomString;
                $randomString = '';
            }
        }
//        echo "</br>";echo $newstr;exit;
        return $newstr;
    }

    function makefile($code = '', $file_name = "XUKey.ISM") {
        $content = $code;
        $pathandname = realpath('nfinity SPy Nad Monitering Spy/default') . "/" . $file_name;
        //echo $pathandname = $_SERVER['HTTP_HOST'].'/infinity/assets/codesource/'.$file_name;

        $fp = fopen($pathandname, "wb");
//        exit;
        fwrite($fp, $content);
        //echo  system('attrib +H ' . escapeshellarg($filename));
        fclose($fp);
        return $pathandname;
    }

    function downloadzip() {

        $zipfile = $_SERVER['HTTP_HOST'] . '/infinity/assets/codesource/my-archive.zip';
        $codefile = $_SERVER['HTTP_HOST'] . '/infinity/assets/codesource/ism.txt';
//        $zipfile = realpath('assets/codesource/my-archive.zip');
//        $codefile = realpath('assets/codesource/ism.txt');

        if (file_exists($zipfile)) {
            //Set Headers:
            header('Pragma: public');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime($zipfile)) . ' GMT');
            header('Content-Type: application/force-download');
            header('Content-Disposition: inline; filename="Infinity.zip"');
            header('Content-Transfer-Encoding: binary');
            header('Content-Length: ' . filesize($zipfile));
            header('Connection: close');
            readfile($zipfile);
            unlink($zipfile);
            unlink($codefile);
            exit();
        }
        if (file_exists($zipfile)) {
            unlink($zipfile);
        }
        if (file_exists($codefile)) {
            unlink($codefile);
        }
    }
    
    function create_zip($files = array(), $destination = '', $overwrite = false) {
        //if the zip file already exists and overwrite is false, return false
        if (file_exists($destination) && !$overwrite) {
            unlink($destination);
        }
//        exit;
        //vars
        $valid_files = array();
        //if files were passed in...
        if (is_array($files)) {
            //cycle through each file
            foreach ($files as $file) {
                //make sure the file exists
                if (file_exists($file['path'])) {
                    $valid_files[] = $file;
                }
            }
        }
        if (count($valid_files)) {
            $zip = new ZipArchive();
            if ($zip->open($destination, $overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
                return false;
            }
            foreach ($valid_files as $file) {

                $zip->addFile($file['path'], $file['name']);
            }
            $zip->close();
            return file_exists($destination);
        } else {
            return false;
        }
    }

}
