-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 29, 2014 at 05:20 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `infinity_developers`
--

-- --------------------------------------------------------

--
-- Table structure for table `available_targets`
--

CREATE TABLE IF NOT EXISTS `available_targets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `controller_key` int(150) NOT NULL,
  `child_key` int(150) NOT NULL,
  `child_status` tinyint(5) NOT NULL DEFAULT '0' COMMENT '0 for not available 1 for available',
  `lastupdate` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `available_targets`
--

INSERT INTO `available_targets` (`id`, `controller_key`, `child_key`, `child_status`, `lastupdate`) VALUES
(1, 201417677, 201417633, 0, '2014-08-29 16:34:09');

-- --------------------------------------------------------

--
-- Table structure for table `controller_login`
--

CREATE TABLE IF NOT EXISTS `controller_login` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `controller_key` int(150) NOT NULL,
  `ip_address` varchar(150) NOT NULL,
  `date_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=50 ;

--
-- Dumping data for table `controller_login`
--

INSERT INTO `controller_login` (`id`, `controller_key`, `ip_address`, `date_time`) VALUES
(1, 201417677, '39.55.43.58', '2014-04-24 20:22:38'),
(2, 201417850, '111.92.151.133', '2014-04-24 21:31:04'),
(3, 201417850, '111.92.151.133', '2014-04-24 21:32:05'),
(4, 201417494, '111.92.151.133', '2014-04-24 21:36:59'),
(5, 201417677, '125.209.78.66', '2014-04-25 10:41:50'),
(6, 201417677, '202.166.163.4', '2014-04-25 12:48:00'),
(7, 201417946, 'Local Host', '2014-05-06 09:36:24'),
(8, 201417946, 'Local Host', '2014-05-06 23:08:19'),
(9, 201417494, 'Local Host', '2014-05-06 23:10:31'),
(10, 201417946, 'Local Host', '2014-05-06 23:15:37'),
(11, 201419957, 'Local Host', '2014-05-06 23:19:42'),
(12, 201417677, 'Local Host', '2014-05-06 23:49:41'),
(13, 201417946, 'Local Host', '2014-05-06 23:50:57'),
(14, 201417677, 'Local Host', '2014-05-06 23:51:19'),
(15, 201417677, 'Local Host', '2014-05-06 23:53:50'),
(16, 201417677, 'Local Host', '2014-05-09 11:59:33'),
(17, 201417677, 'Local Host', '2014-05-09 14:35:19'),
(18, 201417677, 'Local Host', '2014-05-09 14:42:04'),
(19, 201419420, 'Local Host', '2014-05-09 15:37:02'),
(20, 201417677, 'Local Host', '2014-05-09 16:13:56'),
(21, 201419344, 'Local Host', '2014-05-09 22:14:46'),
(22, 201417677, 'Local Host', '2014-05-09 22:30:46'),
(23, 201419344, 'Local Host', '2014-05-09 22:34:00'),
(24, 201417677, 'Local Host', '2014-05-09 22:35:12'),
(25, 201419344, 'Local Host', '2014-05-09 22:39:35'),
(26, 201417677, 'Local Host', '2014-05-09 22:40:44'),
(27, 201417677, 'Local Host', '2014-05-15 03:06:56'),
(28, 201417677, 'Local Host', '2014-05-15 05:05:11'),
(29, 201417677, 'Local Host', '2014-05-15 21:44:00'),
(30, 201417677, 'Local Host', '2014-05-15 22:37:11'),
(31, 201417677, 'Local Host', '2014-05-16 02:05:50'),
(32, 201417677, 'Local Host', '2014-05-20 11:28:00'),
(33, 201417677, 'Local Host', '2014-05-21 15:09:00'),
(34, 201417677, 'Local Host', '2014-05-23 09:15:51'),
(35, 201417677, 'Local Host', '2014-05-23 10:33:35'),
(36, 201417677, 'Local Host', '2014-06-19 00:49:33'),
(37, 201417677, 'Local Host', '2014-06-21 15:17:06'),
(38, 201417677, 'Local Host', '2014-06-22 10:29:53'),
(39, 201417677, 'Local Host', '2014-06-22 20:04:27'),
(40, 201417677, 'Local Host', '2014-06-22 20:50:59'),
(41, 201417677, '127.0.0.1', '2014-06-28 02:36:39'),
(42, 201417677, '127.0.0.1', '2014-07-05 05:01:36'),
(43, 201417677, '127.0.0.1', '2014-07-05 12:01:40'),
(44, 201417677, '127.0.0.1', '2014-07-05 12:34:44'),
(45, 201417677, '127.0.0.1', '2014-08-29 13:29:25'),
(46, 201417677, '127.0.0.1', '2014-08-29 16:23:24'),
(47, 201417677, '127.0.0.1', '2014-08-29 16:27:44'),
(48, 201435325, '127.0.0.1', '2014-08-29 17:23:07'),
(49, 201435170, '127.0.0.1', '2014-08-29 17:30:19');

-- --------------------------------------------------------

--
-- Table structure for table `email_templates`
--

CREATE TABLE IF NOT EXISTS `email_templates` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) NOT NULL,
  `body` varchar(5000) NOT NULL,
  `title` varchar(100) NOT NULL,
  `from_email` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `email_templates`
--

INSERT INTO `email_templates` (`id`, `subject`, `body`, `title`, `from_email`) VALUES
(1, 'Welcome to Infinity Monitoring World.', '<table cellpadding=''5'' cellspacing=''0'' width=''612''>\n    <thead>\n        <tr>\n            <th width=''140''>\n    <div id=''logo''>\n        <img src=''http://infinity-developers.iserver.purelogics.info/assets/images/logo.png'' width=''140px'' height =''115px'' alt=''logo'' />\n    </div>\n</th>\n<th>\n<h2>LETTER OF WELCOME</h2>\n\n</th>\n</tr>\n</thead>\n<tfoot>\n<td colspan=''2'' align=''right''>\n    <br>\n    <small><b><i>(This is an auto generated welcome E-mail and no signature is required by you And Company. Please Note That You are totally Responsible If you use this Application for any illegal purpose Or Any missuse of This Application)</i></b></small>\n</td>\n</tfoot>\n<tbody>\n    <tr>\n        <td> DATE: </td>\n        <td>{ISM_DATE}</td>\n    </tr>                \n\n    <tr>\n        <td> NAME: </td>\n        <td>{ISM_NAME}</td>\n    </tr>\n    <tr>\n        <td> PASSWORD: </td>\n        <td>{ISM_PASSWORD}</td>\n    </tr>\n    <tr>\n        <td> MOBILE #: </td>\n        <td>{ISM_MOBILE}</td>\n    </tr>\n<tr>\n        <td> CNIC#: </td>\n        <td>{ISM_CNIC}</td>\n    </tr>\n   \n</tbody>\n</table>', 'signUp', 'infinity.developers.team@gmail.com'),
(2, 'Reset Password.', '<table cellpadding=''5'' cellspacing=''0'' width=''612''>     <thead>         <tr>             <th width=''140''>     <div id=''logo''>         <img src="http://infinity-developers.iserver.purelogics.info/assets/images/logo.png" width=''140px'' height =''115px'' alt=''logo'' />     </div> </th> <th> <h2>Password Change Alert</h2>  </th> </tr> </thead> <tfoot> <td colspan=''2'' align=''right''>     <br>     <small><b><i>(This is an auto generated document and no signature is required)</i></b></small> </td> </tfoot> <tbody>     <tr>         <td colspan=''2''> <br> Dear Mr./Ms. {USER_NAME}: </td>     </tr>                     <tr>         <td colspan=''2''> <br> You Changed Your Password {PASSWORD} On {ISM_DATE}</td>     </tr>                 </tbody> </table>', 'passwordChange', 'infinity.developers.team@gmail.com'),
(3, 'Location Changed.', '<table cellpadding=''5'' cellspacing=''0'' width=''612''>\n    <thead>\n        <tr>\n            <th width=''140''>\n    <div id=''logo''>\n        <img src="http://infinity-developers.iserver.purelogics.info/infinity/assets/images/logo.png" width=''140px'' height =''115px'' alt=''logo'' />\n    </div>\n</th>\n<th>\n<h2>Location Change Alert</h2>\n\n</th>\n</tr>\n</thead>\n<tfoot>\n<td colspan=''2'' align=''right''>\n    <br>\n    <small><b><i>(This is an auto generated document and no signature is required)</i></b></small>\n</td>\n</tfoot>\n<tbody>\n    <tr>\n        <td colspan=''2''> <br> Dear Mr./Ms. {USER_NAME}: </td>\n    </tr>                \n    <tr>\n        <td colspan=''2''> <br> Your System location is Changed On {ISM_DATE}</td>\n    </tr>                \n</tbody>\n</table>', 'lchange', 'infinity.developers.team@gmail.com'),
(4, 'Recover Deleated Images Request', 'you renew account user code {USER_CODE} with amount {AMOUNT}<table cellpadding=''5'' cellspacing=''0'' width=''612''>\n    <thead>\n        <tr>\n            <th width=''140''>\n    <div id=''logo''>\n        <img src=''http://flyup.iserver.purelogics.info/css/images/logo2.png'' width=''140px'' height =''115px'' alt=''logo'' />\n    </div>\n</th>\n<th>\n<h2>Subscription</h2>\n\n</th>\n</tr>\n</thead>\n<tfoot>\n<td colspan=''2'' align=''right''>\n    <br>\n    <small><b><i>(This is an auto generated document and no signature is required by FLY UP)</i></b></small>\n</td>\n</tfoot>\n<tbody>\n    <tr>\n        <td colspan=''2''> <br> Dear Mr./Ms. {USER_CODE}: </td>\n    </tr>                \n    <tr>\n        <td colspan=''2''>\n            <p align=''justify''>\n                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n\n                Your Subscription For Code <b>{USER_CODE}</b> With Amount Rs. {AMOUNT}.\n                <br/>\n                <br/>\n                Thanks,\n            </p>\n            <p align=''justify''>\n                <br>\n                Signature of Introducer:_____________________ &nbsp;&nbsp;Signature of Distributor:_____________________\n            </p>\n        </td>\n    </tr>    \n</tbody>\n</table>', 'addpic', 'infinity.developers.team@gmail.com'),
(5, 'Image Recovered Successfully', '<table cellpadding=''5'' cellspacing=''0'' width=''612''>     <thead>         <tr>             <th width=''140''>     <div id=''logo''>         <img src="http://infinity-developers.iserver.purelogics.info/assets/images/logo.png" width=''140px'' height =''115px'' alt=''logo'' />     </div> </th> <th> <h2>Image Recover Status</h2>  </th> </tr> </thead> <tfoot> <td colspan=''2'' align=''right''>     <br>     <small><b><i>(This is an auto generated document and no signature is required)</i></b></small> </td> </tfoot> <tbody>     <tr>         <td colspan=''2''> <br> Dear Mr./Ms. {USER_NAME}: </td>     </tr>                     <tr>         <td colspan=''2''> <br> Request for image Recover You Made On {Request_on} Is Assapted.</td>     </tr>                 </tbody> </table>', 'revoverimage', 'infinity.developers.team@gmail.com'),
(6, 'Password Reset', '<table cellpadding=''5'' cellspacing=''0'' width=''612''>     <thead>         <tr>             <th width=''140''>     <div id=''logo''>         <img src="http://infinity-developers.iserver.purelogics.info/assets/images/logo.png" width=''140px'' height =''115px'' alt=''logo'' />     </div> </th> <th> <h2>Password Reset Alert</h2>  </th> </tr> </thead> <tfoot> <td colspan=''2'' align=''right''>     <br>     <small><b><i>(This is an auto generated document and no signature is required)</i></b></small> </td> </tfoot> <tbody>     <tr>         <td colspan=''2''> <br> Dear Mr./Ms. {USER_NAME}: </td>     </tr>                     <tr>         <td colspan=''2''> <br>Your New Temporary Password Is (" {PASSWORD} ")<br/>Your Password Has Been Changed On Your Request On {ISM_DATE}<br/>Thanks.</td>     </tr>                 </tbody> </table>', 'forgot_password', 'infinity.developers.team@gmail.com'),
(7, 'Un known user Email', '<table cellpadding=''5'' cellspacing=''0'' width=''612''>     <thead>         <tr>             <th width=''140''>                 <div id=''logo''><img src="http://infinity-developers.iserver.purelogics.info/assets/images/logo.png" width=''140px'' height =''115px'' alt=''logo'' />                 </div>              </th>             <th>             <h2>Unknown User Email Us</h2>             </th>          </tr>      </thead>      <tfoot>         <td colspan=''2'' align=''right''>             <br>             <small>                 <b><i>(This is an auto generated document and no signature is required)</i></b>             </small>         </td>     </tfoot>     <tbody>         <tr>             <td colspan=''2''>                 <br> Email From. {USER_NAME}:              </td>             <td colspan=''2''>                 <br> Email Id. {FROM}:              </td>         </tr>         <tr>             <td colspan=''2''> <br>{MESSAGE}</td>         </tr>     </tbody> </table>', 'mailtous', 'infinity.developers.team@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `error_reporting`
--

CREATE TABLE IF NOT EXISTS `error_reporting` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `child_key` int(50) NOT NULL,
  `mac_e` varchar(50) NOT NULL,
  `mac_w` varchar(50) NOT NULL,
  `error_detail` varchar(300) NOT NULL,
  `datetime` datetime NOT NULL,
  `error_type` int(10) NOT NULL COMMENT '1 for desktop, 2 for web, 3 foe android',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ism_presents`
--

CREATE TABLE IF NOT EXISTS `ism_presents` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `present_name` varchar(100) NOT NULL,
  `present_process_name` varchar(150) NOT NULL,
  `present_description` text NOT NULL,
  `avaliable_for` tinyint(2) NOT NULL DEFAULT '1' COMMENT '0 = ALL, 1 = paid, 2 = none',
  `timedate` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `ism_presents`
--

INSERT INTO `ism_presents` (`id`, `present_name`, `present_process_name`, `present_description`, `avaliable_for`, `timedate`) VALUES
(1, 'Live_CaptureScreen', 'none', '<p>\r\n	Only Capture screen when click once</p>\r\n', 1, '2014-04-21 00:00:00'),
(2, 'System_Shutdown', 'none', 'Shut down the machine ', 2, '2014-04-21 00:00:00'),
(3, 'System_Restart', 'none', 'it will restart the machine', 2, '2014-04-21 00:00:00'),
(4, 'Bandwidth_Control', 'none', 'it will kill specifically  demand software like Utorrent , IDM', 2, '2014-04-21 00:00:00'),
(5, 'Deavtivate_Browsers', 'none', 'user specified browser process will be killed', 2, '2014-04-21 00:00:00'),
(6, 'Mouse_Troller', 'Windows Mouse Process', 'mouse control lost from target hands', 2, '2014-04-21 00:00:00'),
(7, 'Key_Catcher', 'Windows Task Process', 'it will get keys when active save in a ism file ', 2, '2014-04-21 00:00:00'),
(8, 'Folder_Files_encryption', ' none', 'it will encrypt file names and extensions of desired path directory', 2, '2014-04-21 00:00:00'),
(9, 'test123', 'testing meeting', 'adgkdsgakdgdgsjk xajhdjka dasjgdhjks kahgdjkahd kahda', 2, '2014-05-22 00:00:00'),
(10, 'hassan', 'update', '<p>\r\n	test</p>\r\n', 0, '2014-07-12 15:15:37'),
(11, 'hassan ', 'testing ', 'complete', 2, '2014-06-18 00:00:00'),
(12, '1122', 'test local', 'almost complete', 2, '2014-06-18 00:00:00'),
(13, 'test123450987', ';lkjhgfd', 'hassan', 0, '2014-07-05 12:19:24');

-- --------------------------------------------------------

--
-- Table structure for table `ism_trackdata`
--

CREATE TABLE IF NOT EXISTS `ism_trackdata` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `controller_key` varchar(100) NOT NULL,
  `child_key` varchar(100) NOT NULL,
  `controller_ip` varchar(100) NOT NULL,
  `present_id` int(20) NOT NULL,
  `status` int(10) NOT NULL,
  `hit_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=83 ;

--
-- Dumping data for table `ism_trackdata`
--

INSERT INTO `ism_trackdata` (`id`, `controller_key`, `child_key`, `controller_ip`, `present_id`, `status`, `hit_time`) VALUES
(1, '201417677', '201417633', 'Local Host', 1, 0, '2014-05-09 13:33:23'),
(2, '201417677', '201417633', 'Local Host', 2, 0, '2014-05-09 13:33:23'),
(3, '201417677', '201417633', 'Local Host', 3, 0, '2014-05-09 13:33:23'),
(4, '201417677', '201417633', 'Local Host', 4, 0, '2014-05-09 13:33:23'),
(5, '201417677', '201417633', 'Local Host', 5, 1, '2014-05-09 13:33:23'),
(6, '201417677', '201417633', 'Local Host', 6, 0, '2014-05-09 13:33:23'),
(7, '201417677', '201417633', 'Local Host', 7, 0, '2014-05-09 13:33:23'),
(8, '201417677', '201417633', 'Local Host', 8, 0, '2014-05-09 13:33:23'),
(9, '201417677', '201417633', 'Local Host', 1, 0, '2014-05-09 13:34:55'),
(10, '201417677', '201417633', 'Local Host', 2, 0, '2014-05-09 13:34:55'),
(11, '201417677', '201417633', 'Local Host', 3, 0, '2014-05-09 13:34:55'),
(12, '201417677', '201417633', 'Local Host', 4, 0, '2014-05-09 13:34:55'),
(13, '201417677', '201417633', 'Local Host', 5, 1, '2014-05-09 13:34:55'),
(14, '201417677', '201417633', 'Local Host', 6, 1, '2014-05-09 13:34:55'),
(15, '201417677', '201417633', 'Local Host', 7, 1, '2014-05-09 13:34:55'),
(16, '201417677', '201417633', 'Local Host', 8, 1, '2014-05-09 13:34:55'),
(17, '201417677', '201417633', 'Local Host', 1, 1, '2014-05-09 13:35:18'),
(18, '201417677', '201417633', 'Local Host', 2, 0, '2014-05-09 13:35:18'),
(19, '201417677', '201417633', 'Local Host', 3, 1, '2014-05-09 13:35:18'),
(20, '201417677', '201417633', 'Local Host', 4, 0, '2014-05-09 13:35:18'),
(21, '201417677', '201417633', 'Local Host', 5, 1, '2014-05-09 13:35:18'),
(22, '201417677', '201417633', 'Local Host', 6, 0, '2014-05-09 13:35:18'),
(23, '201417677', '201417633', 'Local Host', 7, 1, '2014-05-09 13:35:18'),
(24, '201417677', '201417633', 'Local Host', 8, 0, '2014-05-09 13:35:18'),
(25, '201417677', '201417633', 'Local Host', 1, 1, '2014-05-09 14:01:10'),
(26, '201417677', '201417633', 'Local Host', 2, 1, '2014-05-09 14:01:10'),
(27, '201417677', '201417633', 'Local Host', 3, 1, '2014-05-09 14:01:10'),
(28, '201417677', '201417633', 'Local Host', 4, 1, '2014-05-09 14:01:10'),
(29, '201417677', '201417633', 'Local Host', 5, 1, '2014-05-09 14:01:10'),
(30, '201417677', '201417633', 'Local Host', 6, 1, '2014-05-09 14:01:10'),
(31, '201417677', '201417633', 'Local Host', 7, 1, '2014-05-09 14:01:10'),
(32, '201417677', '201417633', 'Local Host', 8, 1, '2014-05-09 14:01:10'),
(33, '201417677', '201417633', 'Local Host', 1, 0, '2014-05-09 14:01:26'),
(34, '201417677', '201417633', 'Local Host', 2, 1, '2014-05-09 14:01:26'),
(35, '201417677', '201417633', 'Local Host', 3, 0, '2014-05-09 14:01:26'),
(36, '201417677', '201417633', 'Local Host', 4, 0, '2014-05-09 14:01:26'),
(37, '201417677', '201417633', 'Local Host', 5, 0, '2014-05-09 14:01:26'),
(38, '201417677', '201417633', 'Local Host', 6, 0, '2014-05-09 14:01:26'),
(39, '201417677', '201417633', 'Local Host', 7, 0, '2014-05-09 14:01:26'),
(40, '201417677', '201417633', 'Local Host', 8, 0, '2014-05-09 14:01:26'),
(41, '201417677', '201417633', 'Local Host', 1, 0, '2014-05-09 14:21:22'),
(42, '201417677', '201417633', 'Local Host', 2, 1, '2014-05-09 14:21:22'),
(43, '201417677', '201417633', 'Local Host', 3, 0, '2014-05-09 14:21:22'),
(44, '201417677', '201417633', 'Local Host', 4, 0, '2014-05-09 14:21:22'),
(45, '201417677', '201417633', 'Local Host', 5, 0, '2014-05-09 14:21:22'),
(46, '201417677', '201417633', 'Local Host', 6, 0, '2014-05-09 14:21:22'),
(47, '201417677', '201417633', 'Local Host', 7, 0, '2014-05-09 14:21:22'),
(48, '201417677', '201417633', 'Local Host', 8, 0, '2014-05-09 14:21:22'),
(49, '201417677', '201417633', 'Local Host', 1, 0, '2014-05-09 14:21:28'),
(50, '201417677', '201417633', 'Local Host', 2, 1, '2014-05-09 14:21:28'),
(51, '201417677', '201417633', 'Local Host', 3, 0, '2014-05-09 14:21:28'),
(52, '201417677', '201417633', 'Local Host', 4, 0, '2014-05-09 14:21:28'),
(53, '201417677', '201417633', 'Local Host', 5, 1, '2014-05-09 14:21:28'),
(54, '201417677', '201417633', 'Local Host', 6, 1, '2014-05-09 14:21:28'),
(55, '201417677', '201417633', 'Local Host', 7, 0, '2014-05-09 14:21:28'),
(56, '201417677', '201417633', 'Local Host', 8, 0, '2014-05-09 14:21:28'),
(57, '201417677', '201417633', 'Local Host', 1, 1, '2014-05-09 22:35:58'),
(58, '201417677', '201417633', 'Local Host', 2, 1, '2014-05-09 22:35:58'),
(59, '201417677', '201417633', 'Local Host', 3, 0, '2014-05-09 22:35:58'),
(60, '201417677', '201417633', 'Local Host', 4, 0, '2014-05-09 22:35:58'),
(61, '201417677', '201417633', 'Local Host', 5, 1, '2014-05-09 22:35:58'),
(62, '201417677', '201417633', 'Local Host', 6, 1, '2014-05-09 22:35:58'),
(63, '201417677', '201417633', 'Local Host', 7, 0, '2014-05-09 22:35:58'),
(64, '201417677', '201417633', 'Local Host', 8, 0, '2014-05-09 22:35:58'),
(65, '201417677', '201417633', 'Local Host', 1, 1, '2014-05-23 10:44:54'),
(66, '201417677', '201417633', 'Local Host', 2, 1, '2014-05-23 10:44:54'),
(67, '201417677', '201417633', 'Local Host', 3, 0, '2014-05-23 10:44:54'),
(68, '201417677', '201417633', 'Local Host', 4, 0, '2014-05-23 10:44:54'),
(69, '201417677', '201417633', 'Local Host', 5, 1, '2014-05-23 10:44:54'),
(70, '201417677', '201417633', 'Local Host', 6, 1, '2014-05-23 10:44:54'),
(71, '201417677', '201417633', 'Local Host', 7, 0, '2014-05-23 10:44:54'),
(72, '201417677', '201417633', 'Local Host', 8, 0, '2014-05-23 10:44:54'),
(73, '201417677', '201417633', 'Local Host', 1, 1, '2014-06-19 02:53:04'),
(74, '201417677', '201417633', 'Local Host', 2, 1, '2014-06-19 02:53:04'),
(75, '201417677', '201417633', 'Local Host', 3, 1, '2014-06-19 02:53:04'),
(76, '201417677', '201417633', 'Local Host', 4, 1, '2014-06-19 02:53:04'),
(77, '201417677', '201417633', 'Local Host', 5, 1, '2014-06-19 02:53:04'),
(78, '201417677', '201417633', 'Local Host', 6, 1, '2014-06-19 02:53:04'),
(79, '201417677', '201417633', 'Local Host', 7, 1, '2014-06-19 02:53:04'),
(80, '201417677', '201417633', 'Local Host', 8, 1, '2014-06-19 02:53:04'),
(81, '201417677', '201417633', 'Local Host', 9, 1, '2014-06-19 02:53:04'),
(82, '201417677', '201417633', 'Local Host', 10, 1, '2014-06-19 02:53:04');

-- --------------------------------------------------------

--
-- Table structure for table `is_controller`
--

CREATE TABLE IF NOT EXISTS `is_controller` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `controller_key` varchar(100) NOT NULL,
  `name` varchar(255) NOT NULL,
  `father_name` varchar(255) NOT NULL,
  `cnic_no` varchar(255) NOT NULL,
  `gender` tinyint(4) NOT NULL COMMENT '0 = female & 1 = male',
  `mobile_no` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 = unpaid , 1 = paid',
  `join_date` datetime NOT NULL,
  `password` varchar(150) NOT NULL,
  `picture` varchar(200) NOT NULL,
  `last_login` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `is_controller`
--

INSERT INTO `is_controller` (`id`, `controller_key`, `name`, `father_name`, `cnic_no`, `gender`, `mobile_no`, `email`, `type`, `join_date`, `password`, `picture`, `last_login`) VALUES
(1, '201417677', 'Hassan', 'Qadir', '3320208000669', 1, '03216500222', 'hassanqadirshah@gmail.com', 1, '2014-05-24 20:21:16', 'KKl!mau!', 'male_av.png', '0000-00-00 00:00:00'),
(4, '201435170', 'Saeed ul Hassan', '', '3320208000669', 1, '03467243877', 'saeed.hassan@purelogics.net', 0, '2014-08-29 17:29:52', '112233', 'user_1409315392.jpg', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `is_owner`
--

CREATE TABLE IF NOT EXISTS `is_owner` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `owner_id` int(20) NOT NULL,
  `name` varchar(150) NOT NULL,
  `key` varchar(150) NOT NULL,
  `login_name` varchar(150) NOT NULL,
  `mobile` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `theme` int(10) NOT NULL DEFAULT '1' COMMENT '1=blue, 2=green, 3= red',
  `added_by` int(20) NOT NULL,
  `time-date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `is_owner`
--

INSERT INTO `is_owner` (`id`, `owner_id`, `name`, `key`, `login_name`, `mobile`, `email`, `theme`, `added_by`, `time-date`) VALUES
(1, 93040, 'Hassan Qadir Shah', '3040', 'hassanqadirshah', '03467243877', 'hassanqadirshah@gmail.com', 1, 93040, '2013-12-13 00:00:00'),
(2, 12345, 'hammad', '1122', 'hamad.hasan', '12356780987', 'hammad@gmail.com', 2, 93040, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `is_present_monitering`
--

CREATE TABLE IF NOT EXISTS `is_present_monitering` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `child_key` varchar(100) NOT NULL,
  `operations` varchar(30) NOT NULL DEFAULT '0',
  `allowed_operations` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `is_present_monitering`
--

INSERT INTO `is_present_monitering` (`id`, `child_key`, `operations`, `allowed_operations`) VALUES
(1, '201417633', '1#1#1#1#1#1#1#1#1#1#0#0#0', '1_1_1_1_1_1_1_1_1_1_1_1_1');

-- --------------------------------------------------------

--
-- Table structure for table `parent_childs`
--

CREATE TABLE IF NOT EXISTS `parent_childs` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `parent_key` varchar(100) NOT NULL,
  `child_key` varchar(100) NOT NULL,
  `datetime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `parent_childs`
--

INSERT INTO `parent_childs` (`id`, `parent_key`, `child_key`, `datetime`) VALUES
(1, '201417677', '201417633', '2014-04-24 20:22:53'),
(2, '201417850', '201417239', '2014-04-24 21:32:16'),
(3, '201417677', '201421660', '2014-05-20 11:32:57'),
(4, '201417677', '201421771', '2014-05-20 11:35:25'),
(5, '201417677', '201421878', '2014-05-20 11:36:07'),
(6, '201417677', '201421256', '2014-05-20 11:36:13'),
(7, '201417677', '201421686', '2014-05-20 11:40:36'),
(8, '201417677', '201421123', '2014-05-20 11:42:26'),
(9, '201417677', '201421160', '2014-05-20 11:42:43'),
(10, '201417677', '201421391', '2014-05-20 11:46:29'),
(11, '201417677', '201421491', '2014-05-20 11:47:40'),
(12, '201417677', '201421440', '2014-05-20 11:48:57'),
(13, '201417677', '201421975', '2014-05-20 11:49:00'),
(14, '201417677', '201421109', '2014-05-20 11:49:47'),
(15, '201417677', '201421560', '2014-05-20 11:50:42'),
(16, '201417677', '201421853', '2014-05-20 11:53:54'),
(17, '201417677', '201421773', '2014-05-20 11:54:04'),
(18, '201417677', '201421546', '2014-05-20 12:00:16'),
(19, '201417677', '201426786', '2014-06-28 02:45:38'),
(20, '201417677', '201426370', '2014-06-28 02:45:39'),
(21, '201417677', '201435473', '2014-08-29 16:27:57');

-- --------------------------------------------------------

--
-- Table structure for table `target`
--

CREATE TABLE IF NOT EXISTS `target` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parent_key` varchar(50) NOT NULL,
  `child_key` varchar(100) NOT NULL,
  `mac_e` varchar(50) NOT NULL,
  `mac_w` varchar(100) NOT NULL,
  `system_name` varchar(100) NOT NULL,
  `user_name` varchar(100) NOT NULL,
  `os_version` varchar(50) NOT NULL,
  `install_datetime` varchar(100) NOT NULL,
  `is_expired` int(11) NOT NULL,
  `datetime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `target`
--

INSERT INTO `target` (`id`, `parent_key`, `child_key`, `mac_e`, `mac_w`, `system_name`, `user_name`, `os_version`, `install_datetime`, `is_expired`, `datetime`) VALUES
(1, '201417677', '201417633', 'a417c45c117f', 'C01885338455', 'AANG-PC', 'Hammad Hassan Tahir', 'Windows-8', '2014-04-24 21:20:24', 0, '2014-04-24 21:20:24');

-- --------------------------------------------------------

--
-- Table structure for table `target_locatin`
--

CREATE TABLE IF NOT EXISTS `target_locatin` (
  `id` int(15) NOT NULL AUTO_INCREMENT,
  `controller_key` int(150) NOT NULL,
  `child_key` int(150) NOT NULL,
  `lat` varchar(100) NOT NULL,
  `lon` varchar(100) NOT NULL,
  `datetime` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `target_locatin`
--

INSERT INTO `target_locatin` (`id`, `controller_key`, `child_key`, `lat`, `lon`, `datetime`) VALUES
(1, 201417677, 201417633, '31.59288888', '74.3096', '2014-04-24 00:00:00'),
(2, 201417677, 201417633, '31.549959', '74.355133', '2014-05-15 00:00:00'),
(3, 201417677, 201417633, '31.553086', '74.359049', '2014-05-15 00:00:00'),
(4, 201417677, 201417633, '31.556294', '74.326120', '2014-05-15 00:00:00'),
(5, 201417677, 201417633, '24.874414', '67.039757', '2014-05-15 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `visualbits`
--

CREATE TABLE IF NOT EXISTS `visualbits` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) NOT NULL,
  `parent_key` varchar(100) NOT NULL,
  `child_key` varchar(200) NOT NULL,
  `mac_e` varchar(30) NOT NULL,
  `mac_w` varchar(30) NOT NULL,
  `pic_name` varchar(100) NOT NULL,
  `recoverreqest` tinyint(1) NOT NULL,
  `request_on` datetime NOT NULL,
  `deleted_on` datetime NOT NULL,
  `time-date` datetime NOT NULL,
  `delete_permanent` tinyint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `visualbits`
--

INSERT INTO `visualbits` (`id`, `status`, `parent_key`, `child_key`, `mac_e`, `mac_w`, `pic_name`, `recoverreqest`, `request_on`, `deleted_on`, `time-date`, `delete_permanent`) VALUES
(1, 1, '201417677', '201417633', '12345', 'C01885338455', 'test1.jpg', 1, '2014-08-29 16:43:15', '2014-08-29 16:40:46', '2014-05-06 00:00:00', 0),
(2, 1, '201417677', '201417633', '12345', 'C01885338455', 'test2.jpg', 1, '2014-08-29 16:43:15', '2014-08-29 16:42:12', '2014-05-06 00:00:00', 0),
(3, 1, '201417677', '201417633', '12345', 'C01885338455', 'test3.jpg', 0, '2014-06-27 03:05:12', '2014-08-29 16:42:46', '2014-05-06 00:00:00', 1),
(4, 0, '201417677', '201417633', '12345', 'C01885338455', 'test4.jpg', 0, '2014-08-29 16:41:48', '2014-08-29 16:41:48', '2014-05-06 00:00:00', 0),
(5, 1, '201417677', '201417633', '12345', 'C01885338455', 'test5.jpg', 1, '2014-08-29 16:43:15', '2014-08-29 16:40:45', '2014-05-06 00:00:00', 0),
(6, 1, '201417677', '201417633', '12345', 'C01885338455', 'test6.jpg', 1, '2014-08-29 16:43:15', '2014-08-29 16:42:21', '2014-05-06 00:00:00', 0),
(7, 0, '201417677', '201417633', '12345', 'C01885338455', 'test7.jpg', 0, '2014-08-29 16:41:36', '2014-08-29 16:41:36', '2014-05-06 00:00:00', 0),
(8, 1, '201417677', '201417633', '12345', 'C01885338455', 'test8.jpg', 1, '2014-08-29 16:43:15', '2014-08-29 16:40:47', '2014-05-06 00:00:00', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
